
#ifndef GUI__H
#define GUI__H

#include <stdargs.h>
#include "common.h"
#include "gfx.h"
#include "resources.h"
#include "typedefs.h"
#include "maths.h"


#define MAX_PRINT_LEN		48

#define GUI_GAMESTATE_BG_WIDTH			100
#define GUI_GAMESTATE_PADDING			2


#define GUI_GAMESTATE_TIMER_OFFSET_X	4
#define GUI_GAMESTATE_TIMER_OFFSET_Y	7
#define GUI_GAMESTATE_TIMER_X			(GUI_GAMESTATE_SCORE_OFFSET_X + GUI_GAMESTATE_SCORE_WIDTH + (GUI_GAMESTATE_TIMER_WIDTH / 2) + GUI_GAMESTATE_TIMER_OFFSET_X)
#define GUI_GAMESTATE_TIMER_Y			(SCREEN_H-GUI_GAMESTATE_TIMER_OFFSET_Y)
#define GUI_GAMESTATE_TIMER_WIDTH		30
#define GUI_GAMESTATE_TIMER_HEIGHT		4
#define GUI_GAMESTATE_TIMER_COLOR		ATI_565RGB(0xFF,0xFF,0xC0)

#define GUI_GAMESTATE_TARGET_WIDTH		100

#define GUI_GAMESTATE_SCORE_OFFSET_X	3
#define GUI_GAMESTATE_SCORE_X			((GUI_GAMESTATE_SCORE_WIDTH / 2) + GUI_GAMESTATE_SCORE_OFFSET_X)
#define GUI_GAMESTATE_SCORE_Y			GUI_GAMESTATE_TIMER_Y
#define GUI_GAMESTATE_SCORE_WIDTH		40
#define GUI_GAMESTATE_SCORE_HEIGHT		9
#define GUI_GAMESTATE_SCORE_BG_COLOR	ATI_565RGB(0x39,0x7D,0xF3)
#define GUI_GAMESTATE_SCORE_TEXT_COLOR	ATI_565RGB(0xFF,0xFF,0xFF)
#define GUI_GAMESTATE_SCORE_SPEED		51


#define GUI_GAMESTATE_PAYMENT_OFFSET_Y		3
#define GUI_GAMESTATE_PAYMENT_PATH_Y		40
#define GUI_GAMESTATE_PAYMENT_WIDTH			36
#define GUI_GAMESTATE_PAYMENT_HEIGHT		9
#define GUI_GAMESTATE_PAYMENT_TEXT_COLOR	ATI_565RGB(0xFF,0xFF,0xFF)
#define GUI_GAMESTATE_PAYMENT_SHADOW_COLOR	ATI_565RGB(0x22,0x22,0x22)
#define GUI_GAMESTATE_PAYMENT_SPEED			TOFIXED(10.0)


BOOL guiInit();
BOOL guiTerminate();
BOOL guiInitState();


BOOL guiInitCharsBuf(u8 fontID);

BOOL guiFontEnableBg(BOOL enable);
BOOL guiFontSetColor(u32 color);
BOOL guiFontSet(s32 fontID);
BOOL guiPrintf(s32 x, s32 y, const char *fmt, ...);
BOOL guiPrintfCenter(s32 x, s32 y, const char *fmt, ...);
BOOL guiPrintfCenterScreen(s32 line, const char *fmt, ...);
BOOL guiPrint(s32 x, s32 y, char *str);

BOOL guiCallbackHealthBar(TSprite *spr, s32 x, s32 y);
BOOL guiCallbackScore(TSprite *spr, s32 x, s32 y);
BOOL guiCallbackPayment(TSprite *spr, s32 x, s32 y);
BOOL guiDrawGameState();
BOOL guiDrawBar(AHIRECT_T *outerRect, s32 color, FIXED percent);

BOOL guiConstructRect(AHIRECT_T *rect, s32 x, s32 y, s32 w, s32 h);

#endif
