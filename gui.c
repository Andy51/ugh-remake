
#include "gui.h"


static AHICHAR_T			sCharsBuf[MAX_PRINT_LEN];
static BOOL					sFontBg;
static s32					sFontID;
static u32					sFontColor;
static AHISURFACE_T			sGuiSurf;		// Target surface to draw GUI into
static AHISURFACE_T			sSavedSurf;		// Previously set dst surf to restore after gui draw done
static u32					sAnimatedScore;

extern TGameState			gGameState;
extern u32					gLastPayment;


BOOL guiInit()
{
	BOOL				result;
	
	sSavedSurf = 0;
	sGuiSurf = gDrawSurf;

	resLoadFont("font_small.fnt", 0);
	result = guiInitCharsBuf(0);

	resLoadFont("font_big.fnt", 1);
	result = guiInitCharsBuf(1);

	guiFontSet(1);

	return result;
}

BOOL guiTerminate()
{
	return TRUE;
}


BOOL guiInitState()
{
	sAnimatedScore = gGameState.score;

	return TRUE;
}

BOOL guiInitCharsBuf(u8 fontID)
{
	u32					i;
	TFont				*font = &gFonts[fontID];

	font->buffer = (AHICHAR_T*)suAllocMem(sizeof(AHICHAR_T) * MAX_PRINT_LEN, NULL);

	for(i=0; i<MAX_PRINT_LEN; i++)
	{
		font->buffer[i].stridex = font->w;
		font->buffer[i].stridey = 0;
		font->buffer[i].w = font->w;
		font->buffer[i].h = font->h;
	}

	return TRUE;
}

BOOL guiFontEnableBg(BOOL enable)
{
	sFontBg = enable;

	return TRUE;
}

BOOL guiFontSetColor(u32 color)
{
	sFontColor = color;

	return TRUE;
}

BOOL guiFontSet(s32 fontID)
{
	sFontID = fontID;

	return TRUE;
}


/* 
   Set the target surface to draw GUI into
*/
BOOL guiSetSurf(AHISURFACE_T surf)
{
	sGuiSurf = surf;

	return TRUE;
}

BOOL guiSwitchSurf()
{
	if(sSavedSurf == 0)
	{
		AhiDrawSurfDstSet(gCtx, sGuiSurf, 0);
	
		sSavedSurf = sGuiSurf;
	}
	else
	{
		AhiDrawSurfDstSet(gCtx, sSavedSurf, 0);

		sSavedSurf = 0;
	}

	return TRUE;
}

BOOL guiPrintf(s32 x, s32 y, const char *fmt, ...)
{
	va_list				vars;
	char				buf[MAX_PRINT_LEN];
	BOOL				result;

	va_start( vars, fmt );
	
	vsprintf(buf, fmt, vars);
	//vsnprintf(buf, MAX_PRINT_LEN, fmt, vars);

	result = guiPrint(x, y, buf);
		
	va_end( vars );
	
	return result;
}

BOOL guiPrintfCenter(s32 x, s32 y, const char *fmt, ...)
{
	va_list				vars;
	char				buf[MAX_PRINT_LEN];
	BOOL				result, eos;
	s32					start, end;
	TFont				*font = &gFonts[sFontID];
	s32					len;


	va_start( vars, fmt );
	
	vsprintf(buf, fmt, vars);

	va_end( vars );

	// For each line of text
	start = end = 0;
	eos = FALSE;
	do
	{
		while(buf[end] != '\r' && buf[end] != 0)
			end++;

		if(buf[end] == 0)
			eos = TRUE;

		buf[end] = 0;

		len = (end - start) * font->w;
		guiPrint(x - len/2 + 1, y - font->h/2, &buf[start]);
		y += font->h;

		start = ++end;

	} while(eos == FALSE);
	
	return result;
}


BOOL guiPrintfCenterScreen(s32 line, const char *fmt, ...)
{
	va_list				vars;
	char				buf[MAX_PRINT_LEN];
	BOOL				result, eos;
	s32					start, end;
	TFont				*font = &gFonts[sFontID];
	s32					len;
	s32					x, y;

	x = SCREEN_W/2;
	y = (line ) * font->h;


	va_start( vars, fmt );
	
	vsprintf(buf, fmt, vars);

	va_end( vars );

	// For each line of text
	start = end = 0;
	eos = FALSE;
	do
	{
		while(buf[end] != '\r' && buf[end] != 0)
			end++;

		if(buf[end] == 0)
			eos = TRUE;

		buf[end] = 0;

		len = (end - start) * font->w;
		guiPrint(x - len/2 + 1, y, &buf[start]);
		y += font->h;

		start = ++end;

	} while(eos == FALSE);
	
	return result;
}

#ifndef WIN32
int __wrap_vsprintf( char * buffer, const char * format, va_list arglist )
{
	return __real_vsprintf( buffer, format, &arglist );
}


int __wrap_vsnprintf( char * buffer, size_t maxlen, const char * format, va_list arglist )
{
	return __real_vsnprintf( buffer, maxlen, format, &arglist );
}
#endif

BOOL guiPrint(s32 x, s32 y, char *str)
{
	AHIPOINT_T			pt;
	AHIRECT_T			rect;
	TFont				*font = &gFonts[sFontID];
	s32					len;
	
	len = 0;
	while( str[len] != '\0' )
	{

		font->buffer[len].image = (void*)&font->bits[font->map[((u8)(str[len])-0x20)] * font->stride];
		
		len++;
	}


	if(sFontBg)
	{

		AhiDrawRopSet( gCtx, AHIROP3(AHIROP_PATCOPY) );
		AhiDrawBrushFgColorSet(gCtx, ATI_565RGB(0xFF,0xFF,0xFF));
	
		rect.x1 = x - 1;
		rect.y1 = y - 1;
		rect.x2 = x + len * font->w + 1;
		rect.y2 = y + font->h + 1;
	
		AhiDrawSpans(gCtx, &rect, 1, 0);
		AhiDrawRopSet( gCtx, AHIROP3(AHIROP_SRCCOPY) );
	}

	AhiDrawFgColorSet( gCtx, sFontColor );

	pt.x = x - font->w;
	pt.y = y + 1;
	
	AhiDrawChar( gCtx, &pt, font->buffer, len, 0 );
	
	return TRUE;
}

BOOL guiCallbackHealthBar(TSprite *spr, s32 x, s32 y)
{
	AHIRECT_T			rect;
	s32					w, h;

	w = resGetSpriteWidth(spr);
	h = resGetSpriteHeight(spr);

	x -= w/2;
	y -= h/2;

	guiConstructRect( &rect, x, y, w, h );

	h = gGameState.playerHealth;

	guiDrawBar( &rect, GUI_GAMESTATE_TIMER_COLOR, h );

	return TRUE;
}

BOOL guiCallbackScore(TSprite *spr, s32 x, s32 y)
{
	AHIRECT_T			rect;
	s32					w, h;

	w = resGetSpriteWidth(spr);
	h = resGetSpriteHeight(spr);

	guiConstructRect( &rect, x - w/2, y - h/2, w, h );

	guiDrawBar( &rect, GUI_GAMESTATE_SCORE_BG_COLOR, TOFIXED(1.0f) );

	if(sAnimatedScore < gGameState.score)
	{
		sAnimatedScore += GUI_GAMESTATE_SCORE_SPEED;
	}

	// In case we need reinitialization (new game) or counter exceeded target value
	if(sAnimatedScore > gGameState.score)
	{
		sAnimatedScore = gGameState.score;
	}

	guiFontSet(0);
	guiFontSetColor(GUI_GAMESTATE_SCORE_TEXT_COLOR);
	guiPrintfCenter(x, y, "%d", sAnimatedScore);


	return TRUE;
}

BOOL guiCallbackPayment(TSprite *spr, s32 x, s32 y)
{
	AHIRECT_T			rect;
//	static s32			curScore = 0;
	s32					w, h;

	w = resGetSpriteWidth(spr);
	h = resGetSpriteHeight(spr);

/*	guiConstructRect( &rect, x - w/2, y - h/2, w, h );

	guiDrawBar( &rect, GUI_GAMESTATE_SCORE_BG_COLOR, TOFIXED(1.0f) );

	if(curScore < gGameState.score)
	{
		curScore += GUI_GAMESTATE_SCORE_SPEED;
		if(curScore > gGameState.score)
		{
			curScore = gGameState.score;
		}
	}
*/

	guiFontSet(0);
	guiFontSetColor(GUI_GAMESTATE_PAYMENT_SHADOW_COLOR);
	guiPrintfCenter(x+1, y+1, "%d", gLastPayment);

	guiFontSetColor(GUI_GAMESTATE_PAYMENT_TEXT_COLOR);
	guiPrintfCenter(x, y, "%d", gLastPayment);

	return TRUE;
}


BOOL guiConstructRect(AHIRECT_T *rect, s32 x, s32 y, s32 w, s32 h)
{
	if(rect == NULL)
		return FALSE;

	rect->x1 = x;
	rect->y1 = y;
	rect->x2 = rect->x1 + w;
	rect->y2 = rect->y1 + h;
	
	return TRUE;
}

BOOL guiReduceRect(AHIRECT_T *rect, s32 x, s32 y)
{
	if(rect == NULL)
		return FALSE;

	rect->x1 += x;
	rect->y1 += y;
	rect->x2 -= x;
	rect->y2 -= y;
	
	return TRUE;
}

BOOL guiDrawBar(AHIRECT_T *outerRect, s32 color, FIXED percent)
{
	AHIRECT_T			rect = *outerRect;

	AhiDrawRopSet( gCtx, AHIROP3(AHIROP_PATCOPY) );

	AhiDrawBrushFgColorSet(gCtx, ATI_565RGB(0x00,0x00,0x00));
	AhiDrawSpans(gCtx, outerRect, 1, 0);

	guiReduceRect(&rect, 1, 1);

	rect.x2 = rect.x1 + fpmul(rect.x2-rect.x1, percent);

	AhiDrawBrushFgColorSet(gCtx, color);
	AhiDrawSpans(gCtx, &rect, 1, 0);

	AhiDrawRopSet( gCtx, AHIROP3(AHIROP_SRCCOPY) );

	return TRUE;
}


BOOL guiDrawGameState()
{
	AHIPOINT_T			pt;
	AHIRECT_T			bgRect;
	AHIRECT_T			barRect;
	s32					len;
	

	//guiSwitchSurf();

	/*
		Background
	*/
/*
	guiConstructRect( &bgRect,
					  (SCREEN_W - GUI_GAMESTATE_BG_WIDTH) / 2,
					  SCREEN_H - GUI_GAMESTATE_BG_HEIGHT,
					  GUI_GAMESTATE_BG_WIDTH,
					  GUI_GAMESTATE_BG_HEIGHT );

	guiDrawBar( &bgRect, ATI_565RGB(0xFF,0xFF,0xFF), TOFIXED(1.0) );

	guiReduceRect( &bgRect, GUI_GAMESTATE_PADDING, GUI_GAMESTATE_PADDING);
*/

	/*
		Healthbar
	*/
/*
	barRect = bgRect;
	barRect.x2 = barRect.x1 + GUI_GAMESTATE_TIMER_WIDTH;

	guiDrawBar( &barRect, ATI_565RGB(0xFF,0xFF,0x0), TOFIXED(0.5) );
*/
	
	/*
		Current destination
	*/
/*
	guiPrint(barRect.x2 + GUI_GAMESTATE_PADDING, barRect.y1, "1");

	guiSwitchSurf();
*/
	return TRUE;
}

BOOL guiDrawScreenBackground(u32 color)
{
	AHIRECT_T			rect;

	AhiDrawRopSet( gCtx, AHIROP3(AHIROP_PATCOPY) );
	AhiDrawBrushFgColorSet(gCtx, color);
	
	rect.x1 = rect.y1 = 0;
	rect.x2 = SCREEN_W;
	rect.y2 = SCREEN_H;
	
	AhiDrawSpans(gCtx, &rect, 1, 0);
	
	AhiDrawRopSet( gCtx, AHIROP3(AHIROP_SRCCOPY) );

	return TRUE;
}

