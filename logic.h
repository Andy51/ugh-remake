#ifndef LOGIC__H
#define LOGIC__H

#include "common.h"
#include "resources.h"
#include "dbg.h"
#include "maths.h"
#include "gui.h"

#define FLYOUT_VELOCITY_Y					TOFIXED(-80.0)
#define FLYOUT_VELOCITY_X					TOFIXED(10.0)
#define EJECT_VELOCITY						TOFIXED(70.0)

#define FRUITS_CALORIES						TOFIXED(0.4)
#define BIRD_DAMAGE							TOFIXED(-0.4)
#define TRIOP_DAMAGE						TOFIXED(-0.4)
#define IMPULSE_KOEF						TOFIXED(0.9)

#define WIND_FORCE							TOFIXED(100)

#define STM_DEFAULT							0
#define STM_CURRENT_ROUTE_DELAY				1
#define STM_CURRENT_OBJECT_DELAY			2
#define STM_CURRENT_OBJECT					3
#define STM_CURRENT_PLATFORM				4
#define STM_CHOPPER_OBJECT					5

#define PLATFORMS_MAX						6
#define PLATFORM_WATER						(-1)
#define PLATFORM_NONE						(-2)

#define SCORE_MIN							500
// Reduction per second
#define SCORE_REDUCTION						100
#define SCORE_CAVEMAN						1100
#define SCORE_CAVEWOMAN						1600
#define SCORE_CAVEOLDMAN					2600

#define SCORE_BONUS_LIFE					50000



typedef enum
{
	EVENT_NULL = 0,
	EVENT_TIMER,
	EVENT_ANIMATION,
	EVENT_ARRIVAL,
	EVENT_LANDING,
	EVENT_TAKEOFF,
	EVENT_COLLISION,
	EVENT_EJECT,
	EVENT_SWIM,
	EVENT_OFFSCREEN,
	EVENT_GETON

} TEventType;


enum
{
	SPEECH_NULL = 0,
	SPEECH_PLATFORM = 1,
	/* Platforms enums here */
	SPEECH_PLATFORM_MAX = 10,

	SPEECH_QUESTION
};

typedef enum
{
	ACTION_CAVEMAN_WALK = 0,
	ACTION_CAVEMAN_WALK_IN,
	ACTION_CAVEMAN_WALK_OUT,
	ACTION_CAVEMAN_WAIT,
	ACTION_CAVEMAN_FALL,
	ACTION_CAVEMAN_SWIM

} TActionCaveman;

typedef struct
{
	TObject			*obj1;
	TObject			*obj2;

} TCollisionEvData;


extern TObject					*gObjectsList;
extern TObject					*gSpeechBubble;
extern u32						gObjectsCount;
extern u32						gTimestamp;

extern BOOL						gPlatformOccupied[PLATFORMS_MAX];

BOOL lgcInit();
BOOL lgcTerminate();
BOOL lgcReset();
BOOL lgcRegisterEvent(TEventType type, void* data);
TEvent* lgcCheckEvent(TObject *obj, TEventType type, void* data);
BOOL lgcHandleEvent(TObject *obj, TEvent *ev, void *data);
BOOL lgcInitObjectState(TObject *obj, const TEvent *state);
BOOL lgcInitObject(TObject *obj, s8 state);
BOOL lgcInitState();
BOOL lgcSetObjectState(TObject *obj, s32 state);

TObject* lgcUtilGetCollider(TCollisionEvData *collision, TObject *obj);
TObject* lgcUtilGetObject(s16 index);
s16 lgcUtilFindObjectIndex(TObject *obj);


/*
	Externals
*/
TObject* mexCreateObject(u8 type, void* data);
BOOL mexDeleteObject(TObject *obj);
BOOL mexAddPlayerHealth(FIXED add);

#endif
