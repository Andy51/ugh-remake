Long done:

	+ Collision detection between nonphysical objects
	+ Healthbar
	+ Cavemen swimming target moves
	+ Fruit eating
	+ Fruit rotting
	+ Passengers death

	+ Fix multiple cavemen on a same platform
	+ Fix chopper waiting on water
	+ Fix offscreen landscape collisions
	+ Fix Caveman's swimming sprite

New tasks:

	+ Dino
	+ Triopterus
	+ Fix chopper waiting for the caveman on the platform
	+ Stone should have some initial velocity when thrown
	+ States saving
	+ Vertical movement should be limited
	+ Pilot flies too when crash occures
	+ Lives
	+ Cash for taxi
	+ Menu
	+ Resources cleanup
	+ Common logic for various cavemen
	? Menu cleanup
	+ Scores display
	+ Animated score
	+ Payments display
	+ Ending Screen
	+ Game font
	- Screensaver handling
	+ Bonus lives for score
	? L7 support
	+ New bitmap format
	+ [High]scores
	- Savegames caching
	- Cutscenes:
		- Intro #0 (Cavegirl)
		- Intro #1 (Characters intro)
		- Intro #2 (Apple)
		- Outro


	+ Duplicate the movement keys on the digit keys
	+ Add logic's platform occupation info to savegames
	+ Fix multiple cavemen in water
	+ Birds should collide with the player even when he is landed
	+ Fix cavemen should swim when reached by flooding water
	+ Fix initial choper placement (e.g. on lvl 4)
	+ Fix water levels
	