
#ifndef MAP__H
#define MAP__H

#include "common.h"
#include "resources.h"
#include "gfx.h"




#define SOLID_TILE		((u8)117)


BOOL mapInit();
BOOL mapTerminate();
BOOL mapReset();
BOOL mapRender();
BOOL mapRenderWater();

extern FIXED				gWaterLevel;

#endif

