
#ifndef MECHANICS__H
#define MECHANICS__H

#include "common.h"
#include "maths.h"
#include "resources.h"
#include "gfx.h"
#include "map.h"
#include "logic.h"
#include "gui.h"



#define GRAVITY_ACCEL				TOFIXED(120)

#define TRACTION_X					TOFIXED(90)
#define TRACTION_Y					TOFIXED(100)

#define MAX_VELOCITY				TOFIXED(70)

#define FRICTION_CUTOFF				TOFIXED(2)
#define FRICTION_KOEF				TOFIXED(-0.5)

// Fa + mg = 0
// Fa = pg(V/2) = -mg
// pgV = -mg*2
#define ARCHIMEDE_FORCE_CHOPPER		TOFIXED(-33*2)
//#define ARCHIMEDE_FORCE_DEFAULT		TOFIXED(-40)
#define WATER_DEMP_FORCE			TOFIXED(-35*2)
#define ARCHIMEDE_FORCE_DEFAULT		TOFIXED(-100*2)

#define PLAYER_MASS					TOFIXED(0.3)
#define DEFAULT_MASS				TOFIXED(1)
//#define STONE_MASS					PLAYER_MASS
#define STONE_MASS					TOFIXED(2)
#define LANDING_SPEED				TOFIXED(40)
#define SWIM_ACCELERATION			TOFIXED(10)
#define SWIM_VELOCITY				TOFIXED(20)

#define DAMAGE_CUTOFF				TOFIXED(80.0)
#define DAMAGE_KOEF					TOFIXED(0.002)

/*
#define GRAVITY_ACCEL				TOFIXED(30)

#define TRACTION_X					TOFIXED(70)
#define TRACTION_Y					TOFIXED(80)

#define MAX_VELOCITY				TOFIXED(70)

#define FRICTION_CUTOFF				TOFIXED(2)
#define FRICTION_KOEF				TOFIXED(-0.0)

#define PLAYER_MASS					TOFIXED(0.5) 
*/ 

#define BOUNCE_KOEF					TOFIXED(-0.4)

#define DEADLY_DEPTH				TOFIXED(SCREEN_H*1.5)

#define GAMESTATE_CRASH_TIMER 		3000
#define GAMESTATE_LIVES_COUNT		5

#define SAVEGAME_FILENAME			"game.sav"
#define HIGHSCORE_FILENAME			"score.hi"

#define CURRENT_HIGHSCORE_VER		1


typedef struct TSavedObject_tag
{
	u8					type;

	s8					curPlatform;
    u16					route;

	u16					flags;

	TVector2D			pos;
	TVector2D			vel;
	TVector2D			accel;
	TVector2D			appliedForce;

	FIXED				mass;
	FIXED				archimedeForce;

	u32					timestamp;

	union
	{
		FIXED				pos;
		s32 				obj;
	} target;

	s16					aux;

	u16					sprite;

	u16					state;
	s16					stateData[STATE_EVENTS_MAX];

	s16					passenger;

} TSavedObject;


typedef struct TSavedObjectV7_tag
{
	u8					type;

	s8					curPlatform;
    u8					route;

	u16					flags;

	TVector2D			pos;
	TVector2D			vel;
	TVector2D			accel;
	TVector2D			appliedForce;

	FIXED				mass;
	FIXED				archimedeForce;

	u32					timestamp;

	union
	{
		FIXED				pos;
		s32 				obj;
	} target;

	s16					aux;

	u16					sprite;

	u16					state;
	s16					stateData[STATE_EVENTS_MAX];

	s16					passenger;

} TSavedObjectV7;

typedef struct
{
	u8				version;
	u16				level;
	TGameState		gameState;
	FIXED			waterLevel;
	u32				objCount;
	u32				timestamp;
	BOOL			platformOccupied[PLATFORMS_MAX];

} TSavedGameInfo;

typedef struct
{
	u16				ver;
	s16				level;
	u32				score;

} THighscoreInfo;


BOOL mexInit();
BOOL mexTerminate();
BOOL mexReset();
BOOL mexInitState(u32 level);
BOOL mexTermState();
STATE_RESULT_T mexUpdate();
BOOL mexPrepareLevel();
void mexHandleKeys(u32 keysPressed, u32 keysReleased, u32 keysHold);
BOOL mexRemoveObject(TObject *obj);
BOOL mexClearObjects();
BOOL mexCreateStatic(TObject *obj, u8 spriteID);
BOOL mexCreateCaveman(TObject *obj);
BOOL mexPhysicalMovement(TObject *obj);
TObject* mexCreateObject(u8 type, void* data);

BOOL mexClearSaveGame();
BOOL mexCheckSaveGame();
BOOL mexSaveGame();
BOOL mexLoadGame( u32 *level );

BOOL mexLoadHighscore(u32 *score, s32 *level);
BOOL mexSaveHighscore(u32 score, s32 level);

BOOL mexLoadStateObjects();



FIXED lgcUtilPlaceOver(TObject *obj, s16 level);

#endif
