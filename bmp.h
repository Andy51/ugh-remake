#ifndef _BMP_H_
#define _BMP_H_


#include "bmp_types.h"
#include <ati.h>


#define SWAP_BYTES_IN_WORD(w) (		((w & 0x00FF) << 8) | \
									((w & 0xFF00) >> 8) )


#define SWAP_BYTES_IN_DWORD(d) (	((d & 0x000000FF) << 24) | \
									((d & 0x0000FF00) <<  8) | \
									((d & 0x00FF0000) >>  8) | \
									((d & 0xFF000000) >> 24) )


#define E16(w)				(UINT16)(		((w & 0x00FF) << 8) | \
									((w & 0xFF00) >> 8) )


#define E32(d)					(UINT32)(	((d & 0x000000FF) << 24) | \
									((d & 0x0000FF00) <<  8) | \
									((d & 0x00FF0000) >>  8) | \
									((d & 0xFF000000) >> 24) )

#ifdef WIN32
	#undef SWAP_BYTES_IN_WORD
	#undef SWAP_BYTES_IN_DWORD
	#define SWAP_BYTES_IN_WORD(w)	w
	#define SWAP_BYTES_IN_DWORD(d)	d
#else
	#undef E16
	#undef E32
	#define E16(w)	w
	#define E32(d)	d
#endif





#ifdef __cplusplus
extern "C" {
#endif


RETURN_STATUS_T BMP_LoadFromFile( const WCHAR * file, AHIBITMAP_T * ahibmp );
RETURN_STATUS_T BMP_SaveToFile( const WCHAR * file, AHIBITMAP_T * ahibmp );


#ifdef __cplusplus
} /* extern "C" */
#endif


#endif // _BMP_H_
