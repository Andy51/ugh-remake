
#include "main.h"

//#define DEBUG_CONSTANT_TICK

u32					gFPS;
FIXED				gTick;

static u32			sPrevKeyboard;
static UINT64		sLastTime;
static s32			sChainCount;

SEMAPHORE_HANDLE_T	gSemaphore;

#ifdef DEBUG_SINGLE_FRAME
static BOOL			sFrameRequest;
#endif

void * __wrap_memcpy(void *dst, const void *src, size_t sz )
{
    return __rt_memcpy( dst, src, sz );
}

typedef struct
{
	const char			*moduleName;
	TInitFn				initializer;
	TInitFn				terminator;

} TInitChainEntry;

static TInitChainEntry sInitChain[] = 
{
	{ "Task",	mainTaskInit,	mainTaskTerminate },
	{ "GFX",	gfxInit,		gfxTerminate },
	{ "RES",	resInit,		resTerminate },
	{ "MAP",	mapInit,		mapTerminate },
	{ "MEX",	mexInit,		mexTerminate },
	{ "GUI",	guiInit,		guiTerminate },
	{ "STS",	stsInit,		stsTerminate }
};

#define MAIN_CHAIN_SIZE		(sizeof(sInitChain)/sizeof(TInitChainEntry))

BOOL mainChainInit()
{
	BOOL				result = TRUE;
	s32					i;

	for(i=0; i < MAIN_CHAIN_SIZE && result != FALSE; i++)
	{
		if(sInitChain[i].initializer != NULL)
			result = sInitChain[i].initializer();
	}

	i--;

	if(result == FALSE)
		dbgf("* ERROR: Module %s failed initialization!", sInitChain[i--].moduleName); // i-- So we should not terminate failed module

	sChainCount = i; 

	return result;
}

BOOL mainChainTerminate()
{
	BOOL				result = TRUE;
	s32					i;

	for(i=sChainCount; i >= 0 /* && result != FALSE */; i--)
	{
		if(sInitChain[i].terminator != NULL)
			result = sInitChain[i].terminator();
	}

	return result;
}



void mainTaskLoop()
{
	s32					error;
	
	while( (gGlobalState != GLOBAL_STATE_EXIT ) && (gGlobalState != GLOBAL_STATE_SUSPEND) )
    {
		mainUpdateTick();

		/*guiFontEnableBg(TRUE);
		guiFontSetColor(ATI_565RGB(0x0,0x0,0x0));
		guiFontSet(0);
		guiPrintf(0, 0, "%d", gFPS);
		guiFontEnableBg(FALSE);
		*/
		stsUpdate();

		gfxFlushDoublebuffer();

		#ifdef WIN32
			suSleep(20, &error);
		#else
			suSleep(1, &error);
		#endif

		mainHandleInput();
    }

}

void mainTask(void *arg)
{
	s32					successCnt = 0;
	BOOL				result = 0;

	result = mainChainInit();

	if(result)
	{
		mainTaskLoop();
	}

	mainChainTerminate();

}


BOOL mainTaskInit()
{
	INT32		err;
	dbg("creating semaphore..");
	gSemaphore = suCreateBSem(SEMAPHORE_LOCKED, &err);
	dbgf("gSemaphore = 0x%X, err = %d", gSemaphore, err);

	if(strcmp(ldrGetPlatformName(), "LTE") == 0)
		gPhonePlatform = PHONE_PLATFORM_LTE;
	else
		gPhonePlatform = PHONE_PLATFORM_LTE2;

	DL_KeyKjavaGetKeyState();
	sPrevKeyboard = DL_KeyKjavaGetKeyState();
	dbgf("Initial keystate: 0x%X", sPrevKeyboard);
	sLastTime = suPalTicksToMsec( suPalReadTime() );

	randomize();

	return TRUE;
}

BOOL mainTaskTerminate()
{
	BOOL			result = TRUE;
	dbg("mainTaskTerminate");

	if(gGlobalState == GLOBAL_STATE_EXIT)
		result = appTerminate();

	suReleaseSem(gSemaphore, NULL);

	dbg("mainTaskTerminate: release!");
	
	suDeleteSem(gSemaphore, NULL);

	return result;
}

void mainTaskCreate()
{
	suCreateTask( mainTask, TASK_STACK_SIZE, TASK_PRIORITY );
}


u32 mainUpdateTick()
{
	UINT64				currentTime;
	u32					dt;
	
	currentTime = suPalTicksToMsec( suPalReadTime() );

	dt = (u32)(currentTime - sLastTime);
	
	sLastTime = currentTime;

	//gTick = dt;
	gTick = (gTick + dt) / 2;

	if(gTick != 0)
		gFPS = 1000 / gTick;

	#ifdef DEBUG_CONSTANT_TICK
		gTick = 20;
	#endif

		
	return gTick;
}


void mainHandleInput()
{
	u32					keys;
	//u32 				key = 0x00080000;

	u32					keysPressed = 0;
	u32					keysReleased = 0;
	u32					keysHold = 0;
	
	keys = DL_KeyKjavaGetKeyState();

	keysHold = keys & sPrevKeyboard;
	keysPressed = keys & (~sPrevKeyboard);
	keysReleased = (~keys) & sPrevKeyboard;

	stsHandleKeys(keysPressed, keysReleased, keysHold);

	sPrevKeyboard = keys;
}

