#ifndef MATHS__H
#define MATHS__H

#include "common.h"

static __inline FIXED fpmul(FIXED x, FIXED y)
{
	return (x*y)>>FP_BASE;
}

static __inline FIXED fpdiv(FIXED x, FIXED y)
{
	return ((x<<FP_BASE)/y);
}

// Increased accuracy (base+4) functions, for 1/x calculations
static __inline FIXED fpmul_acc(FIXED x, FIXED y)
{
	return (x*y)>>(FP_BASE+4);
}

static __inline FIXED fpmul2(FIXED x, FIXED y)
{
	return (x*y)>>(FP_BASE*2);
}

static __inline FIXED fpdiv_acc(FIXED x, FIXED y)
{
	return ((x<<(FP_BASE+4))/y);
}

FIXED fpsin(INT32 angle);
FIXED fpcos(INT32 angle);


#endif
