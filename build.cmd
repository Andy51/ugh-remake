@echo off

set OPT=-DEP2 -DFTR_E398 -DSETCANVASCONTROL -DSINGLEBUFFER -DLTE -DGFXLTE

del task_test.elf
arm-eabi-gcc -c -mbig-endian -mthumb -I..\SDK -fshort-wchar -nostdlib -mthumb-interwork %OPT% app.c main.c gfx.c resources.c bmp.c map.c mechanix.c maths.c gui.c logic.c states.c menu.c
arm-eabi-ld -pie -EB -O -nostdlib app.o main.o gfx.o resources.o bmp.o map.o mechanix.o maths.o gui.o logic.o states.o menu.o std.sa rt.lo -wrap memcpy -wrap vsprintf -wrap vsnprintf -o task_test.elf
del /Q *.o
del /Q telf3.elf
postlink task_test.elf -o telf3.elf
pause