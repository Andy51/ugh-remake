#include <mem.h>
#include <filesystem.h>
#include "bmp.h"
#include "dbg.h"


/*
**
**
**
*/
RETURN_STATUS_T BMP_LoadFromFile( const WCHAR * file, AHIBITMAP_T * ahibmp )
{
	FILE_HANDLE_T		fh;
	UINT32				result, count, delta;
	BITMAP_FILEHEADER	bmp_fh;
	BITMAP_HEADER_V3	bmp_hdr;
	UINT16				*bmp_data, *top, *bottom, px;
	UINT32				stride;
	UINT32				i, flip=1;

	
	dbg("BMP_LoadFromFile: enter");
	
	if ( !file || !ahibmp ) return RESULT_FAIL;
	
	dbg("BMP_LoadFromFile: opening file");
	
	fh = DL_FsOpenFile( file, FILE_READ_MODE, 0 );
	if ( fh == FILE_HANDLE_INVALID ) return RESULT_FAIL;
	
	dbg("BMP_LoadFromFile: file opened");
	
	result = DL_FsReadFile( &bmp_fh, BITMAP_FILEHEADER_SIZE, 1, fh, &count );
	if ( result != RESULT_OK )
	{
		DL_FsCloseFile( fh );
		return RESULT_FAIL;
	}
	
	dbg("BMP_LoadFromFile: bmp file header readed");
	
	if ( bmp_fh.Signature != BITMAP_SIGNATURE ) return RESULT_FAIL;
	dbg("BMP_LoadFromFile: it's bmp file");
	
	result = DL_FsReadFile(  &bmp_hdr, sizeof(BITMAP_HEADER_V3), 1, fh, &count );
	if ( result != RESULT_OK )
	{
		DL_FsCloseFile( fh );
		return RESULT_FAIL;
	}
	
	dbg("BMP_LoadFromFile: bmp info header readed");
	
	// convert LE->BE
	bmp_fh.BitsOffset		= SWAP_BYTES_IN_DWORD( bmp_fh.BitsOffset );
	bmp_hdr.HeaderSize		= SWAP_BYTES_IN_DWORD( bmp_hdr.HeaderSize );
	bmp_hdr.Width			= SWAP_BYTES_IN_DWORD( bmp_hdr.Width );
	bmp_hdr.Height			= SWAP_BYTES_IN_DWORD( bmp_hdr.Height );
	bmp_hdr.BitCount		= SWAP_BYTES_IN_WORD ( bmp_hdr.BitCount );
	bmp_hdr.Compression		= SWAP_BYTES_IN_DWORD( bmp_hdr.Compression );
	bmp_hdr.SizeImage		= SWAP_BYTES_IN_DWORD( bmp_hdr.SizeImage );
	bmp_hdr.ClrUsed			= SWAP_BYTES_IN_DWORD( bmp_hdr.ClrUsed );
	
	stride = (( bmp_hdr.Width * bmp_hdr.BitCount / 8 + 3 ) & 0xFFFFFFC) / 2;
	
	// after PhotoShop if save bmp with flip rows
	if ( bmp_hdr.Height < 0) 
	{
		flip = 0;
		bmp_hdr.Height = -bmp_hdr.Height;
	}
	
	if ( bmp_hdr.SizeImage == 0 ) bmp_hdr.SizeImage = stride * 2 * bmp_hdr.Height;
	
	if(		//bmp_hdr.HeaderSize != BITMAP_HEADER_V3_SIZE || 
			bmp_hdr.BitCount != 16 || // RGB565
			!(bmp_hdr.Compression == BI_RGB || 
				bmp_hdr.Compression == BI_BITFIELDS) || // ��� ����������
			bmp_hdr.ClrUsed != 0 ) // ������� ���� �� ��������������
	{
		DL_FsCloseFile( fh );
		return RESULT_FAIL;
	}
	
	dbg("BMP_LoadFromFile: bmp at good format");
	dbgf("BMP_LoadFromFile: bmp_hdr: HeaderSize=%d, Width=%d, Height=%d", 
			bmp_hdr.HeaderSize, bmp_hdr.Width, bmp_hdr.Height);
	dbgf("BMP_LoadFromFile: bmp_hdr: BitCount=%d, Compression=%d, SizeImage=%d", 
			bmp_hdr.BitCount, bmp_hdr.Compression, bmp_hdr.SizeImage);
	
	
	result = DL_FsFSeekFile( fh, bmp_fh.BitsOffset, SEEK_WHENCE_SET );
	if ( result != RESULT_OK )
	{
		DL_FsCloseFile( fh );
		return RESULT_FAIL;
	}
	
	dbg("BMP_LoadFromFile: seek successful");
	
	bmp_data = (void*)suAllocMem( bmp_hdr.SizeImage, (INT32*)&count );
	if ( !bmp_data )
	{
		DL_FsCloseFile( fh );
		return RESULT_FAIL;
	}
	
	dbg("BMP_LoadFromFile: mem allocated");
	


	// BITMAP_DATA
//#ifndef WIN32
	if(bmp_hdr.Width != stride)
	{
		delta = (stride - bmp_hdr.Width)*2;
		top = bmp_data;

		for(i=0; i<bmp_hdr.Height; i++)
		{
			DL_FsReadFile( top, bmp_hdr.Width*2, 1, fh, &count );
			DL_FsFSeekFile( fh, delta, SEEK_WHENCE_CUR );

			top += bmp_hdr.Width;
		}

		stride = bmp_hdr.Width;
	}
	else
//#endif
	{
		result = DL_FsReadFile( bmp_data, bmp_hdr.SizeImage, 1, fh, &count );
		if ( result != RESULT_OK )
		{
			suFreeMem( bmp_data );
			DL_FsCloseFile( fh );
			return RESULT_FAIL;
		}
	}
		
	dbg("BMP_LoadFromFile: bmp data read");
	
	result = DL_FsCloseFile( fh );
	

			// convert LE->BE and flip if necessary
		if ( bmp_hdr.BitCount == 16 )
		{
			top = bmp_data;
			bottom = bmp_data + stride * ( bmp_hdr.Height - 1 );

			while ( top < bottom )
			{
				for ( i = 0; i < stride; i++, top++, bottom++ )
				{
					if ( flip )
					{
						px = SWAP_BYTES_IN_WORD( *top );
						*top = SWAP_BYTES_IN_WORD( *bottom );
						*bottom = px;
					}
					else
					{
						*(UINT16*)top = SWAP_BYTES_IN_WORD( *(UINT16*)top );
						*(UINT16*)bottom = SWAP_BYTES_IN_WORD( *(UINT16*)bottom );
					}
				}
				bottom -= stride*2;
			}
			
			if ( top == bottom ) // middle line
			{
				for ( i = 0; i < stride; i++, top++ )
					*top = SWAP_BYTES_IN_WORD( *top );
			}
			
		}
	


	ahibmp->width		=	bmp_hdr.Width;
	ahibmp->height		=	bmp_hdr.Height;
	ahibmp->image		=	bmp_data;
	ahibmp->stride		=	stride*2;
	ahibmp->format		=	AHIFMT_16BPP_565;
	
	dbg("BMP_LoadFromFile: exit");
	
	return RESULT_OK;
}


/* EOF */
