
#include "app.h"


UINT32 appStart( EVENT_STACK_T *ev_st,  REG_ID_T reg_id,  UINT32 param2 );
UINT32 appExit( EVENT_STACK_T *ev_st,  APPLICATION_T *app );

/*#ifndef WIN32

	#undef dbgf
	#define dbgf( format, ... ) 
	#undef dbg
	#define dbg( format ) 
	
#endif*/

const char			app_name[APP_NAME_LEN] = "UGH!"; 

UINT32				evcode_base;

ldrElf				elf;

UINT32				gGlobalState;
APPLICATION_T		*gApplication;
extern SEMAPHORE_HANDLE_T		gSemaphore;


EVENT_HANDLER_ENTRY_T any_state_handlers[] =
{
	{ EV_LOSE_FOCUS,				HandleLoseFocus },
	{ STATE_HANDLERS_END,			NULL },
};

EVENT_HANDLER_ENTRY_T init_state_handlers[] =
{
	{ EV_GAIN_FOCUS,				HandleGainFocus },
	{ EV_DISPLAY_ACTIVE,			HandleGainFocus },
	{ EV_GRANT_TOKEN,				HandleUITokenGranted },
	{ STATE_HANDLERS_END,			NULL }
};

EVENT_HANDLER_ENTRY_T main_state_handlers[] =
{
	{ EV_DISPLAY_NO_ACTIVE,			HandleLoseFocus },
	{ STATE_HANDLERS_END,			NULL }
};

const STATE_HANDLERS_ENTRY_T state_handling_table[] =
{
	{
		APP_STATE_ANY,
		NULL,
		NULL,
		any_state_handlers
	},
	
	{
		APP_STATE_INIT,
		NULL,
		NULL,
		init_state_handlers
	},

	{
		APP_STATE_MAIN,
		MainStateEnter,
		MainStateExit,
		main_state_handlers
	}

};


ldrElf* _start( WCHAR *uri, WCHAR *params )
{
	UINT32					status = RESULT_OK;
	
	if ( ldrIsLoaded( (char*)app_name ) )
	{
		cprint("Dummy: Already loaded!\n");
		return NULL;
	}
	
	evcode_base = ldrRequestEventBase( );
	evcode_base = ldrInitEventHandlersTbl( any_state_handlers, evcode_base );
	evcode_base = ldrInitEventHandlersTbl( init_state_handlers, evcode_base );
	evcode_base = ldrInitEventHandlersTbl( main_state_handlers, evcode_base );
	
	gGlobalState = GLOBAL_STATE_INIT;

	resInitHomeDir(uri);

	status = APP_Register(  &evcode_base, 1,
							state_handling_table,
							APP_STATE_MAX,
							(void*)appStart );

	ldrSendEvent( evcode_base );

	elf.name = (char*)app_name;

	return &elf;
}
/*
void My_HandleEvent(	EVENT_STACK_T           *ev_st,
						APPLICATION_T           *app,
						APP_ID_T                app_id,
						REG_ID_T                reg_id )
{
	dbgf("My_HandleEvent: 0x%X", AFW_GetEvCode(ev_st));
	APP_HandleEvent( ev_st,	app, app_id, reg_id );
}*/

UINT32 appStart( EVENT_STACK_T *ev_st,  REG_ID_T reg_id,  UINT32 param2 )
{
	UINT32					status = RESULT_OK;
	
	gApplication = APP_InitAppData( (void *)APP_HandleEvent,
									sizeof(APPLICATION_T),
									reg_id,
									0, 0,
									1,
									AFW_APP_CENTRICITY_PRIMARY,
									AFW_FOCUS,
									AFW_POSITION_TOP );

	status = APP_Start( ev_st,
						gApplication,
						APP_STATE_INIT,
						state_handling_table,
						appExit,
						app_name, 0 );

	elf.app = gApplication;

	return status;
}


UINT32 appExit( EVENT_STACK_T *ev_st,  APPLICATION_T *app )
{
	UINT32					status;

	cprint("Dummy: Exit\n");
	
	status = APP_ExitStateAndApp( ev_st, app, 0 );
		
	ldrUnloadElf(); 

	return status;
}


UINT32 MainStateEnter( EVENT_STACK_T *ev_st,  APPLICATION_T *app,  ENTER_STATE_TYPE_T type )
{
	UIS_DIALOG_T			dialog;
	SU_PORT_T				port = app->port;
	DRAWING_BUFFER_T		bufd = {NULL, 176, 220};

	dbgf("MainStateEnter type = %d", type);

	if ( type != ENTER_STATE_ENTER )
		return RESULT_OK;

/*	#ifdef SETCANVASCONTROL
		setCanvasControl__13StatusManagerFScPUs( &theStatusManager, TRUE, NULL );
	#endif


	dialog = UIS_CreateNullDialog( &port );*/
	dialog = UIS_CreateColorCanvas ( &port, &bufd, TRUE );
	
	if ( dialog == NULL ) 
		return RESULT_FAIL;
	
	app->dialog = dialog;
	
	gGlobalState = GLOBAL_STATE_MAIN;
	
	mainTaskCreate();
	
	return RESULT_OK;
}


UINT32 MainStateExit( EVENT_STACK_T *ev_st,  APPLICATION_T *app,  EXIT_STATE_TYPE_T type )
{

	dbgf("MainStateExit type = %d", type);
	if (type != EXIT_STATE_EXIT)
		return RESULT_OK;

	if(gGlobalState == GLOBAL_STATE_MAIN)
	{
		gGlobalState = GLOBAL_STATE_SUSPEND;
	
		dbg("MainStateExit: acquiring semaphore!");
		
		suAcquireSem(gSemaphore, SEMAPHORE_WAIT_FOREVER, NULL);
	
		dbg("MainStateExit: passed acquire!");
	}
		
	if ( app->dialog != NULL ) 
		APP_UtilUISDialogDelete( &app->dialog );

	return RESULT_OK;
}


UINT32 HandleUITokenGranted( EVENT_STACK_T * ev_st,  APPLICATION_T * app )
{
	UINT32					status;
	dbg("HandleUITokenGranted!");
	status = APP_HandleUITokenGranted( ev_st, app );
	
	if( (status == RESULT_OK) && (app->token_status == 2) ) 
	{
		dbg("HandleUITokenGranted: our");
		status = APP_UtilChangeState( APP_STATE_MAIN, ev_st, app );
	}

	return status;
}

UINT32 HandleUITokenRevoked( EVENT_STACK_T * ev_st,  APPLICATION_T * app )
{
	UINT32					status;
	dbg("HandleUITokenRevoked!");
	status = APP_HandleUITokenRevoked( ev_st, app );
	
	if( (status == RESULT_OK) && (app->token_status == 3) )
	{
		dbg("HandleUITokenRevoked: our");
		//status = APP_UtilChangeState( APP_STATE_INIT, ev_st, app );
	}

	return status;
}

UINT32 HandleLoseFocus( EVENT_STACK_T * ev_st,  APPLICATION_T * app )
{
	UINT32					status;

	if(gGlobalState != GLOBAL_STATE_MAIN)
		return RESULT_OK;

	gGlobalState = GLOBAL_STATE_SUSPEND;

	dbg("HandleLoseFocus: acquiring semaphore!");
	
	suAcquireSem(gSemaphore, SEMAPHORE_WAIT_FOREVER, NULL);

	dbg("HandleLoseFocus: passed acquire!");

	status = APP_UtilChangeState( APP_STATE_INIT, ev_st, app );

	return status;
}

UINT32 HandleGainFocus( EVENT_STACK_T * ev_st,  APPLICATION_T * app )
{
	UINT32					status = RESULT_OK;

	dbg("HandleGainFocus!");

	if(gGlobalState == GLOBAL_STATE_SUSPEND)
		status = APP_UtilChangeState( APP_STATE_MAIN, ev_st, app );

	return status;
}


BOOL appTerminate()
{
	dbg("appTerminate!");
	gApplication->exit_status = TRUE;
	
	return TRUE;
}
