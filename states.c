
#include "states.h"

static TStateEnum		sState;
static u32				sSubState;
static u32 				sStateKeysPressed;
static u32				sStateParam;
u32						gCurrentLevelNumber;

extern u32				gHighscore;
extern s32				gHighLevel;

// Total singleplayer levels is 69
#define	STARTING_LEVEL	0
#define	LEVEL_LAST		68



enum
{
	SUBSTATE_NORMAL = 0,
	SUBSTATE_PROCEED,
	SUBSTATE_CANCEL
};

STATE_RESULT_T stsGameUpdate();

BOOL stsResultInit(u32 init);
BOOL stsResultTerminate(void);
STATE_RESULT_T stsResultUpdate();
void stsResultHandleKeys(u32 keysPressed, u32 keysReleased, u32 keysHold);

BOOL stsLoadingInit(u32 data);
BOOL stsLoadingTerminate();
STATE_RESULT_T stsLoadingUpdate();
void stsLoadingHandleKeys(u32 keysPressed, u32 keysReleased, u32 keysHold);
STATE_RESULT_T stsMenuUpdate();

BOOL stsConfirmInit(u32 init);
BOOL stsConfirmTerminate(void);
STATE_RESULT_T stsConfirmUpdate();
void stsConfirmHandleKeys(u32 keysPressed, u32 keysReleased, u32 keysHold);



TStateEntry sStateHandlers[STATE_MAX] = 
{
	NULL, NULL, NULL, NULL,																	// STATE_NULL
	NULL, NULL, NULL, NULL,																	// STATE_INIT
	stsMenuUpdate,		menuHandleKeys,			menuInitState,		menuTerminateState,		// STATE_MENU
	stsLoadingUpdate,	stsLoadingHandleKeys,	stsLoadingInit,		stsLoadingTerminate,	// STATE_LOADING
	stsGameUpdate,		mexHandleKeys,			mexInitState,		mexTermState,			// STATE_GAME
	stsResultUpdate,	stsResultHandleKeys,	stsResultInit,		stsResultTerminate,		// STATE_RESULT
	stsConfirmUpdate,	stsConfirmHandleKeys,	stsConfirmInit,		stsConfirmTerminate,	// STATE_CONFIRM
};

BOOL stsInit()
{
	resLoadMap("levels");
	resLoadTiles("tiles.img");
	resLoadSprites("sprites.spr");

	gCurrentLevelNumber = STARTING_LEVEL;
	stsChangeState(STATE_MENU, gCurrentLevelNumber);
	return TRUE;
}

BOOL stsTerminate()
{
	stsChangeState(STATE_NULL, 0);
	return FALSE;
}

BOOL stsChangeState(TStateEnum newState, u32 data)
{
	if(sStateHandlers[sState].terminator != NULL)
		sStateHandlers[sState].terminator();

	sSubState = SUBSTATE_NORMAL;
	sStateParam = data;

	if(sStateHandlers[newState].initializer != NULL)
		sStateHandlers[newState].initializer(data);

	sState = newState;	

	return TRUE;
}

BOOL stsUpdate()
{
	if(sStateHandlers[sState].update != NULL)
		sStateHandlers[sState].update();

	return TRUE;
}

void stsHandleKeys(u32 keysPressed, u32 keysReleased, u32 keysHold)
{
	if(sStateHandlers[sState].keyHandler != NULL)
		sStateHandlers[sState].keyHandler(keysPressed, keysReleased, keysHold);
}



/**********************************************************\
 
						MENU STATE

\**********************************************************/

STATE_RESULT_T stsMenuUpdate()
{
	u32			result;

	result = menuUpdate();

	switch(result)
	{
		case 0:		// New Game
			gCurrentLevelNumber = STARTING_LEVEL;

			mexReset();

			if(mexCheckSaveGame() == TRUE)
				stsChangeState(STATE_CONFIRM, 0);
			else if(gHighLevel >= 1)
				stsChangeState(STATE_CONFIRM, 1);
			else
				stsChangeState(STATE_LOADING, gCurrentLevelNumber);
		break;

		case 1: // Continue
			stsChangeState(STATE_LOADING, gCurrentLevelNumber);
		break;

		case 2: // Exit
			gGlobalState = GLOBAL_STATE_EXIT;
		break;

		default: // Do nothing
		break;

	}

	return TRUE;
}


/**********************************************************\
 
						LOADING STATE

\**********************************************************/


BOOL stsLoadingInit(u32 level)
{
	/* Draw the loading screen */

	guiDrawScreenBackground(ATI_565RGB(0x0,0x0,0x0));

	guiFontSetColor(ATI_565RGB(0xFF,0xFF,0xFF));

	/* Do the loading */

	mapReset();

	resCleanSpriteProtos();

	mexLoadGame( &level );

	guiInitState();
	guiFontSet(0);
	guiPrintfCenterScreen(1, "Score: %06d", gGameState.score);
	guiPrintfCenterScreen(3, "Lives: %d", gGameState.livesCount);

	mapSetLevel(level);


	guiFontSet(1);
	guiPrintfCenterScreen(7, "Level %d:", gCurrentLevelNumber + 1);
	guiPrintfCenterScreen(9, &gStrings[gCurrentLevel->nameTblOff]);

	mexPrepareLevel(level);


	/* Ready message */

	guiFontSet(0);
	guiPrintfCenterScreen(18, "Press any key");

	sStateKeysPressed = 0;

	return TRUE;
}

BOOL stsLoadingTerminate()
{
	return TRUE;
}

STATE_RESULT_T stsLoadingUpdate()
{
	if(sSubState == SUBSTATE_PROCEED)
	{
		stsChangeState(STATE_GAME, gCurrentLevelNumber);
	}
	
	return STATE_RESULT_NONE;
}

void stsLoadingHandleKeys(u32 keysPressed, u32 keysReleased, u32 keysHold)
{
	u32 				key = 0x00080000;

	// Any key
	if(keysReleased && sStateKeysPressed)
	{
		sSubState = SUBSTATE_PROCEED;
	}

	if(sStateKeysPressed == 0)
		sStateKeysPressed = keysPressed;
}



/**********************************************************\
 
						GAME STATE

\**********************************************************/

STATE_RESULT_T stsGameUpdate()
{
	u32			result;

	result = mexUpdate();

	switch(result)
	{
		case STATE_RESULT_PROCEED:
			gCurrentLevelNumber++;

			if(gCurrentLevelNumber <= LEVEL_LAST)
				stsChangeState(STATE_RESULT, STATE_PARAM_LVLCOMPLETE);
			else
				stsChangeState(STATE_RESULT, STATE_PARAM_WIN);
		break;

		case STATE_RESULT_RETRY:
			stsChangeState(STATE_RESULT, STATE_PARAM_TRYAGAIN);
		break;

		case STATE_RESULT_FAIL:
			stsChangeState(STATE_RESULT, STATE_PARAM_LOSE);
		break;

		case STATE_RESULT_SUSPEND:
			mexSaveGame();
			stsChangeState(STATE_MENU, 0);
		break;

	}

	return TRUE;
}


/**********************************************************\
 
						RESULT STATE

\**********************************************************/


static const char *sResultState_Messages[] = 
{
	"Level complete!",
	"You lost. Try Again!",
	"Game Over",
	"Congratulations!"
};

BOOL stsResultInit(u32 init)
{
	sSubState = SUBSTATE_NORMAL;

    /*
		Show a result message
    */

	guiFontSet(1);
	guiDrawScreenBackground(ATI_565RGB(0x0,0x0,0x0));

	guiFontSetColor(ATI_565RGB(0xFF,0xFF,0xFF));
	
	guiPrintfCenter(SCREEN_W/2, SCREEN_H/2, sResultState_Messages[init]);


	/* 
	    Show the score
	*/

	guiFontSet(0);
	guiPrintfCenterScreen(1, "Score: %06d", gGameState.score);
	guiPrintfCenterScreen(3, "Lives: %d", gGameState.livesCount);
	guiPrintfCenterScreen(26, "Highscore: %06d", gHighscore);

	/* 
	    Show optional message depending on result type
	*/

	switch(init)
	{
		case STATE_PARAM_WIN:
			guiPrintfCenter(SCREEN_W/2, SCREEN_H/2 + 12, "You have finished all levels!");
		break;
	}

	if( init == STATE_PARAM_LOSE || init == STATE_PARAM_WIN )
	{
		if(gGameState.score > gHighscore)
		{
			guiPrintfCenterScreen(20, "New Highscore!");
			gHighscore = gGameState.score;
			gHighLevel = gCurrentLevelNumber;
			mexSaveHighscore(gHighscore, gCurrentLevelNumber);
		}
	}

	sStateKeysPressed = 0;
	
	return TRUE;
}

BOOL stsResultTerminate()
{
	return TRUE;
}

STATE_RESULT_T stsResultUpdate()
{
	if(sSubState == SUBSTATE_PROCEED)
	{
		stsChangeState(STATE_LOADING, gCurrentLevelNumber);
	}
	else if(sSubState == SUBSTATE_CANCEL)
	{
		mexClearSaveGame();
		stsChangeState(STATE_MENU, 0);
	}

	return STATE_RESULT_NONE;
}

void stsResultHandleKeys(u32 keysPressed, u32 keysReleased, u32 keysHold)
{
	// Any key
	if(keysReleased && sStateKeysPressed && sSubState == SUBSTATE_NORMAL)
	{
		if( sStateParam == STATE_PARAM_WIN ||
			sStateParam == STATE_PARAM_LOSE )
			sSubState = SUBSTATE_CANCEL;
		else
			sSubState = SUBSTATE_PROCEED;
	}

	// TODO: Remove this crap! 
	if(keysReleased & MULTIKEY_STAR)
	{
		gGlobalState = GLOBAL_STATE_EXIT;
	}

	if(sStateKeysPressed == 0)
		sStateKeysPressed = keysPressed;
}

/**********************************************************\
 
						CONFIRM STATE

\**********************************************************/

#ifdef LANDSCAPE_RIGHT

#define CONFIRM_VARIANT_OFF_X			190
#define CONFIRM_VARIANT_OFF_Y			5

#else

#define CONFIRM_VARIANT_OFF_X			5
#define CONFIRM_VARIANT_OFF_Y			5

#endif



BOOL stsConfirmInit(u32 init)
{
    /*
		Show a confirmation message
    */

	guiDrawScreenBackground(ATI_565RGB(0x0,0x0,0x0));

	guiFontSetColor(ATI_565RGB(0xFF,0xFF,0xFF));
	
	guiFontSet(0);


	if(init == 0) // Lose savegame confirmation
	{
		guiPrintfCenterScreen(10, "You will lose your saved game");
		guiPrintfCenterScreen(12, "Are you sure you want to proceed?");
	}
	else // Start from the highest level available confirmation
	{
		guiPrintfCenterScreen(10, "Do you want to proceed");
		guiPrintfCenterScreen(12, "from the last level?");
	}


	guiFontSet(1);

	guiPrintf(CONFIRM_VARIANT_OFF_X, CONFIRM_VARIANT_OFF_Y, "Yes");
	guiPrintf(CONFIRM_VARIANT_OFF_X, SCREEN_H - CONFIRM_VARIANT_OFF_Y*2, "No");

	sSubState = SUBSTATE_NORMAL;
	sStateKeysPressed = 0;
	
	return TRUE;
}

BOOL stsConfirmTerminate()
{
	return TRUE;
}

STATE_RESULT_T stsConfirmUpdate()
{
	// We did agree - clear the savegame and reset the level
	if(sSubState == SUBSTATE_PROCEED)
	{
		if(sStateParam == 0)
		{
			gCurrentLevelNumber = STARTING_LEVEL;
		}
		else
		{
			gCurrentLevelNumber = gHighLevel;
		}

		mexClearSaveGame();

		stsChangeState(STATE_LOADING, gCurrentLevelNumber);
	}

	// We did not agree - leave everething as it is
	if(sSubState == SUBSTATE_CANCEL)
	{
		if(sStateParam == 0)
		{
			stsChangeState(STATE_MENU, 0);
		}
		else
		{
			gCurrentLevelNumber = STARTING_LEVEL;
			stsChangeState(STATE_LOADING, gCurrentLevelNumber);
		}
	}

	return STATE_RESULT_NONE;
}

void stsConfirmHandleKeys(u32 keysPressed, u32 keysReleased, u32 keysHold)
{

	// "Yes"
	#ifdef LANDSCAPE_RIGHT
	if(sStateKeysPressed & MULTIKEY_SOFT_RIGHT)
	#else
	if(sStateKeysPressed & MULTIKEY_SOFT_LEFT)
	#endif
	{
		sSubState = SUBSTATE_PROCEED;
	}

	// "No"
	#ifdef LANDSCAPE_RIGHT
	if(sStateKeysPressed & MULTIKEY_SOFT_LEFT)
	#else
	if(sStateKeysPressed & MULTIKEY_SOFT_RIGHT)
	#endif
	{
		sSubState = SUBSTATE_CANCEL;
	}

	if(sStateKeysPressed == 0)
		sStateKeysPressed = keysPressed;
}
