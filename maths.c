
#include "maths.h"

static const FIXED sineLUT[] =
{
	   0,   18,   36,   54,   71,
	  89,  107,  125,  143,  160,
	 178,  195,  213,  230,  248,
	 265,  282,  299,  316,  333,
	 350,  367,  384,  400,  416,
	 433,  449,  465,  481,  496,
	 512,  527,  543,  558,  573,
	 587,  602,  616,  630,  644,
	 658,  672,  685,  698,  711,
	 724,  737,  749,  761,  773,
	 784,  796,  807,  818,  828,
	 839,  849,  859,  868,  878,
	 887,  896,  904,  912,  920,
	 928,  935,  943,  949,  956,
	 962,  968,  974,  979,  984,
	 989,  994,  998, 1002, 1005,
	1008, 1011, 1014, 1016, 1018,
	1020, 1022, 1023, 1023, 1024,
	1024
};

FIXED fpsin(INT32 angle) // angle - degrees
{
	BOOL	negative = FALSE;	
	FIXED	result;

	while(angle > 360)
		angle -= 360;
		
	while(angle < 0)
		angle += 360;
	
	if(angle>270)
	{
		angle = 360 - angle;
		negative = TRUE;
	}

	if(angle > 180) 
	{
		angle -= 180;
		negative = TRUE;
	}

	if(angle > 90)
		angle = 180 - angle;

	result = (sineLUT[angle]<<0);

	if(negative)
		result = -result;
		
	return result;
}

FIXED fpcos(INT32 angle)
{
	return fpsin(90-angle);
}
