

EVRESULT_T lgcCavemanWalkOut(TObject *obj, void *data)
{
	TPlatform				*platform;

	obj->curPlatform = obj->route->platform;

	// Should we wait until the platform is free?
	if(gPlatformOccupied[obj->curPlatform])
		return EVRESULT_TOSTATE + 1;

	// If not, occupy the platform
	gPlatformOccupied[obj->curPlatform] = TRUE;

	platform = &gLevelPlatforms[obj->curPlatform];

	lgcUtilCavemanChangeSprite(obj, ACTION_CAVEMAN_WALK_OUT);

	obj->pos.x = TOFIXED(platform->cave);
	obj->pos.y = lgcUtilPlaceOver(obj, platform->level);

	lgcUtilShowObject(obj, TRUE);

	// Actually, we can proceed to the next route stop right now

	obj->route++;

	return EVRESULT_PROCEED;
}


EVRESULT_T lgcCavemanWalkToSpot(TObject *obj, void *data)
{
	TPlatform				*platform;
	EVRESULT_T				result;

	platform = &gLevelPlatforms[obj->curPlatform];

	lgcUtilCavemanChangeSprite(obj, ACTION_CAVEMAN_WALK);

	// Go to the platform stop
	lgcUtilSetTargetPosition(obj, platform->stop, 8*FP_ONE);

	// We could be saying something
	lgcUtilShowObject(gSpeechBubble, FALSE);

	// The chopper could already be waiting at the platform
	if((gPlayer->curPlatform == obj->curPlatform) && (gPlayer->flags & OBJFLAG_PHYSIC) == 0)
	{
		result = lgcCavemanTellDestination(obj, NULL);

		if(result == EVRESULT_PROCEED)
			return EVRESULT_TOSTATE + 5;
	}

	return EVRESULT_PROCEED;
}

EVRESULT_T lgcCavemanSwim(TObject *obj, void *data)
{
	EVRESULT_T				result;

    obj->curPlatform = PLATFORM_WATER;

	// The chopper could already be waiting at the platform
	if(gPlayer->flags & OBJFLAG_SWIM)
	{
		result = lgcCavemanTellDestination(obj, NULL);

		if(result == EVRESULT_PROCEED)
			return EVRESULT_TOSTATE + 13;
	}

	return EVRESULT_PROCEED;
}

EVRESULT_T lgcCavemanWait(TObject *obj, void *data)
{
	obj->flags &= ~OBJFLAG_TARGET;

	lgcUtilCavemanChangeSprite(obj, ACTION_CAVEMAN_WAIT);

	return EVRESULT_PROCEED;
}

EVRESULT_T lgcCavemanTellDestination(TObject *obj, void *data)
{
	// We should check if the chopper is free, but something like
	// if(gPlayer->passenger != NULL) will not work, since we dont have
	// event priorities and the chopper could have a walking out passenger right now

	if( (gPlayer->passenger != NULL) &&
		(gPlayer->passenger->curPlatform != obj->curPlatform) )
	{
		return EVRESULT_CANCEL;
	}

	//obj->flags &= ~OBJFLAG_PHYSIC;

	lgcUtilCavemanSay(obj, SPEECH_PLATFORM + obj->route->platform);

	return EVRESULT_PROCEED;
}

EVRESULT_T lgcCavemanWTF(TObject *obj, void *data)
{
	lgcUtilCavemanSay(obj, SPEECH_QUESTION);

	return EVRESULT_PROCEED;
}


EVRESULT_T lgcCavemanWalkToChopper(TObject *obj, void *data)
{
	// We were saying something, hide it now
	lgcUtilShowObject(gSpeechBubble, FALSE);

	if(obj->flags & OBJFLAG_SWIM)
	{
		lgcUtilCavemanChangeSprite(obj, ACTION_CAVEMAN_SWIM);
        lgcUtilSetTargetObject(obj, gPlayer, 8*FP_ONE);
	}
	else
	{
		lgcUtilCavemanChangeSprite(obj, ACTION_CAVEMAN_WALK);
        lgcUtilSetTargetPosition(obj, gPlayer->pos.x>>FP_BASE, 8*FP_ONE);
	}

	return EVRESULT_PROCEED;
}

EVRESULT_T lgcCavemanGetIn(TObject *obj, void *data)
{
	// Free the platform we were at
	gPlatformOccupied[obj->curPlatform] = FALSE;

	// Set the curPlatform to the next route platform
	obj->curPlatform = obj->route->platform;

	// Register us as a passenger
	gPlayer->passenger = obj;

	// Remember the time when we got in
	obj->timestamp = gTimestamp;

	lgcUtilClearTargets(obj);
	lgcUtilShowObject(obj, FALSE);

    obj->flags &= ~OBJFLAG_PHYSIC;
    obj->flags &= ~OBJFLAG_SWIM;

	// Tell everyone that we got to the chopper first
	lgcRegisterEvent(EVENT_GETON, (void*)obj);

	return EVRESULT_PROCEED;
}

EVRESULT_T lgcCavemanWalkToCave(TObject *obj, void *data)
{
	TPlatform				*platform;

	lgcUtilCavemanChangeSprite(obj, ACTION_CAVEMAN_WALK);

	platform = &gLevelPlatforms[obj->curPlatform];

	obj->pos.x = gPlayer->pos.x;
	obj->pos.y = lgcUtilPlaceOver(obj, platform->level);

	lgcUtilShowObject(obj, TRUE);

	lgcUtilSetTargetPosition(obj, platform->cave, 8*FP_ONE);

	// Deregister us as passenger
	gPlayer->passenger = NULL;

	// Increase the score
	lgcUtilCavemanPayment(obj);

	return EVRESULT_PROCEED;
}

EVRESULT_T lgcCavemanWalkIn(TObject *obj, void *data)
{
	TPlatform				*platform;

	platform = &gLevelPlatforms[obj->curPlatform];

	obj->pos.x = TOFIXED(platform->cave);
	obj->pos.y = lgcUtilPlaceOver(obj, platform->level);

	lgcUtilCavemanChangeSprite(obj, ACTION_CAVEMAN_WALK_IN);

	obj->flags &= ~OBJFLAG_TARGET;

	return EVRESULT_PROCEED;
}

EVRESULT_T lgcCavemanWaitInCave(TObject *obj, void *data)
{
	// Is it the last route stop?
	if(obj->route->delay < 0)
	{
		gGameState.completeRoutes++;
	}

	lgcUtilShowObject(obj, FALSE);

	return EVRESULT_PROCEED;
}

EVRESULT_T lgcCavemanFall(TObject *obj, void *data)
{
	obj->vel.y = FP_ONE*5;
	obj->flags |= OBJFLAG_PHYSIC;
    lgcUtilClearTargets(obj);

	// Free the platform we were at
	gPlatformOccupied[obj->curPlatform] = FALSE;

	lgcUtilCavemanChangeSprite(obj, ACTION_CAVEMAN_FALL);

	// We were saying something, hide it now
	lgcUtilShowObject(gSpeechBubble, FALSE);
	
	return EVRESULT_PROCEED;
}

EVRESULT_T lgcCavemanDrown(TObject *obj, void *data)
{
    // We could be saying something
	lgcUtilShowObject(gSpeechBubble, FALSE);

	obj->mass += FP_ONE;

	return EVRESULT_PROCEED;
}

const TEvent lgcCaveman[17][STATE_EVENTS_MAX] = 
{
	{ // 0: Doing stuff in the cave
		{ EVENT_TIMER, STM_CURRENT_ROUTE_DELAY, lgcCavemanWalkOut, 2 },
	},

	{ // 1: Waiting the platform to free
		{ EVENT_TAKEOFF, STM_DEFAULT, lgcCavemanWalkOut, 2 },
	},

	{ // 2: Walking out

		{ EVENT_ANIMATION, 1, lgcCavemanWalkToSpot, 3 },
	},

	{ // 3: Walking to the bus stop

		{ EVENT_ARRIVAL, STM_DEFAULT, lgcCavemanWait, 4 },
		{ EVENT_LANDING, STM_CHOPPER_OBJECT, lgcCavemanTellDestination, 5 },
		{ EVENT_COLLISION, OBJTYPE_CHOPPER, lgcCavemanFall, 11 },
		{ EVENT_SWIM, STM_CURRENT_OBJECT, lgcCavemanFall, 11 },
	},

	{ // 4: Waiting at the bus stop

		{ EVENT_LANDING, STM_CHOPPER_OBJECT, lgcCavemanTellDestination, 5 },
		{ EVENT_COLLISION, OBJTYPE_CHOPPER, lgcCavemanFall, 11 },
		{ EVENT_SWIM, STM_CURRENT_OBJECT, lgcCavemanFall, 11 },
	},

	{ // 5: Speaking to the player - destination

		{ EVENT_TIMER, 2000, lgcCavemanWalkToChopper, 7 },
		{ EVENT_TAKEOFF, STM_DEFAULT, lgcCavemanWTF, 6 },
	},

	{ // 6: Speaking to the player - WTF?! Where are u goin?

		{ EVENT_TIMER, 2000, lgcCavemanWalkToSpot, 3 },
		{ EVENT_LANDING, STM_CHOPPER_OBJECT, lgcCavemanTellDestination, 5 },
		{ EVENT_COLLISION, OBJTYPE_CHOPPER, lgcCavemanFall, 11 },
	},

	{ // 7: Walking to the chopper

		{ EVENT_ARRIVAL, STM_DEFAULT, lgcCavemanGetIn, 8 },
		{ EVENT_TAKEOFF, STM_DEFAULT, lgcCavemanWTF, 6 },
	},

	{ // 8: Flying

		{ EVENT_LANDING, STM_CHOPPER_OBJECT, lgcCavemanWalkToCave, 9 },
	},

	
	{ // 9: Walking to the cave

		{ EVENT_ARRIVAL, STM_DEFAULT, lgcCavemanWalkIn, 10 },
	},

	{ // 10: Waiting in the cave

		{ EVENT_ANIMATION, STM_DEFAULT, lgcCavemanWaitInCave, 0 },
	},

	{ // 11: Pushed off the platform - just flying down into the water

		{ EVENT_SWIM, STM_CURRENT_OBJECT, lgcCavemanSwim, 12 },
	},

	{ // 12: Swimming in the water

		{ EVENT_TIMER, 10000, lgcCavemanDrown, 16 },
		{ EVENT_SWIM, STM_CHOPPER_OBJECT, lgcCavemanTellDestination, 13 },
	},

	{ // 13: Speaking to the player - destination, while swimming

		{ EVENT_TIMER, 2000, lgcCavemanWalkToChopper, 14 },
		{ EVENT_TAKEOFF, STM_DEFAULT, lgcCavemanWTF, 15 },
	},

	{ // 14: Swimming to the chopper

		{ EVENT_ARRIVAL, STM_DEFAULT, lgcCavemanGetIn, 8 },
		{ EVENT_TAKEOFF, STM_DEFAULT, lgcCavemanWTF, 15 },
		{ EVENT_GETON, STM_CURRENT_OBJECT, lgcCavemanWTF, 15 },
	},

	{ // 15: WTFing the player while drowning :)

		{ EVENT_TIMER, 2000, lgcCavemanDrown, 16 },
	},

	{ // X: Do nothing
		{ EVENT_NULL }
	},

};

