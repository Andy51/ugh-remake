
EVRESULT_T lgcTreeGetBashed(TObject *obj, void *data)
{
	TObject				*collider;
	TObject				*fruit;

	collider = lgcUtilGetCollider((TCollisionEvData*)data, obj);

	collider->vel.y = -collider->vel.y / 2;

	obj->sprite = resChangeSprite(obj->sprite, SPRITE_TREE_BASHED);

	// Generate some fruit

	dbg("tree bashed");

	fruit = mexCreateObject(OBJTYPE_FRUIT, NULL);

	lgcUtilRandomFlyout(fruit, &obj->pos);
	
	return EVRESULT_PROCEED;
}

EVRESULT_T lgcTreeStayStill(TObject *obj, void *data)
{
	obj->sprite = resChangeSprite(obj->sprite, SPRITE_TREE);
	
	return EVRESULT_PROCEED;
}

const TEvent lgcTree[2][STATE_EVENTS_MAX] = 
{
	{ // 0: Just standing still :)
		{ EVENT_COLLISION, OBJTYPE_STONE, lgcTreeGetBashed, 1 },
	},

	{ // 1: Stay bashed

		{ EVENT_TIMER, 4000, lgcTreeStayStill, 0 },
	},
};


