
EVRESULT_T lgcStoneGetIn(TObject *obj, void *data)
{
	// See lgcCavemanTellDestination for comments
	if( (gPlayer->passenger != NULL)  || // If the chopper is not empty
		(gPlayer->curPlatform != PLATFORM_NONE) ) // OR waiting on the platform
	{
		return EVRESULT_CANCEL;
	}

	// To make the previous "if" work, set stones's destination to null
	obj->curPlatform = PLATFORM_NONE;

	// Register us as a passenger
	gPlayer->passenger = obj;

	lgcUtilShowObject(obj, FALSE);

	return EVRESULT_PROCEED;
}

EVRESULT_T lgcStoneEject(TObject *obj, void *data)
{
	// Deregister us as a passenger
	gPlayer->passenger = NULL;

	obj->sprite = resChangeSprite(obj->sprite, SPRITE_STONE_FALL);
	lgcUtilShowObject(obj, TRUE);

	// Set flight parameters
	obj->pos.x = gPlayer->pos.x;
	obj->pos.y = gPlayer->pos.y;
	obj->vel.x = gPlayer->vel.x;
	obj->vel.y = gPlayer->vel.y + EJECT_VELOCITY;

	obj->flags |= OBJFLAG_PHYSIC;

	return EVRESULT_PROCEED;
}

EVRESULT_T lgcStoneLanding(TObject *obj, void *data)
{
	obj->sprite = resChangeSprite(obj->sprite, SPRITE_STONE);
	dbg("stone landed");

	return EVRESULT_PROCEED;
}

const TEvent lgcStone[3][STATE_EVENTS_MAX] = 
{
	{ // 0: Just lying still :)
		{ EVENT_COLLISION, OBJTYPE_CHOPPER, lgcStoneGetIn, 1 },
	},

	{ // 1: Flying onboard

		{ EVENT_EJECT, 1, lgcStoneEject, 2 },
	},

	{ // 2: Free flight

		{ EVENT_LANDING, STM_CURRENT_OBJECT, lgcStoneLanding, 0 },
		// For debugging
		{ EVENT_SWIM, STM_CURRENT_OBJECT, lgcStoneLanding, 0 }
	},


};


