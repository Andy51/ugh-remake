

EVRESULT_T lgcDinoInhale(TObject *obj, void *data)
{
	// Dino's "passenger" is his wind :D
	obj->passenger->sprite->direction = 1;
	
	return EVRESULT_PROCEED;
}

EVRESULT_T lgcDinoExhale(TObject *obj, void *data)
{
	// Dino's "passenger" is his wind :D
	obj->passenger->sprite->direction = 0;

	return EVRESULT_PROCEED;
}


EVRESULT_T lgcDinoGetBashed(TObject *obj, void *data)
{
	TObject				*collider;

	collider = lgcUtilGetCollider((TCollisionEvData*)data, obj);
	collider->vel.y = -collider->vel.y / 2;

	obj->sprite->curFrame = 0;
	mexStopAnimation(obj->sprite);

	// We "hide" already invisible object to prevent it from colliding
	lgcUtilShowObject(obj->passenger, FALSE);
	

	return EVRESULT_PROCEED;
}

EVRESULT_T lgcDinoBeginBreathing(TObject *obj, void *data)
{
	mexStartAnimation(obj->sprite);
	lgcUtilShowObject(obj->passenger, TRUE);

	return EVRESULT_PROCEED;
}


EVRESULT_T lgcWindApply(TObject *obj, void *data)
{
	TObject				*collider;

	collider = lgcUtilGetCollider((TCollisionEvData*)data, obj);

	if(obj->sprite->direction == 0)
		collider->appliedForce.x -= WIND_FORCE;
	else
		collider->appliedForce.x += WIND_FORCE;

	return EVRESULT_PROCEED;
}

const TEvent lgcDino[3][STATE_EVENTS_MAX] = 
{
	{ // 0: Inhaling
		{ EVENT_TIMER, 1000, lgcDinoExhale, 1 },
		{ EVENT_COLLISION, OBJTYPE_STONE, lgcDinoGetBashed, 2 },
	},

	{ // 1: Exhaling
		{ EVENT_TIMER, 1000, lgcDinoInhale, 0 },
		{ EVENT_COLLISION, OBJTYPE_STONE, lgcDinoGetBashed, 2 },
	},

	{ // 2: Staying bashed
		{ EVENT_TIMER, 5000, lgcDinoBeginBreathing, 0 },
	}

};


const TEvent lgcWind[1][STATE_EVENTS_MAX] = 
{
	{ // 0: All we ever do is blow..
		{ EVENT_COLLISION, OBJTYPE_CHOPPER, lgcWindApply, 0 },
	}
};
