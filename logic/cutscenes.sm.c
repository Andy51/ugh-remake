

EVRESULT_T lgcC0CavemanThrow(TObject *obj, void *data)
{
	obj->sprite = resChangeSprite(obj->sprite, SPRITE_CS0_CAVEMAN_THROW);

	lgcUtilClearTargets(obj);

	return EVRESULT_PROCEED;
}

EVRESULT_T lgcC0CavemanWait(TObject *obj, void *data)
{
	obj->sprite = resChangeSprite(obj->sprite, SPRITE_CS0_CAVEMAN_SMILE);

	return EVRESULT_PROCEED;
}


EVRESULT_T lgcC0CavemanWalkAway(TObject *obj, void *data)
{
	obj->sprite = resChangeSprite(obj->sprite, SPRITE_CAVEMAN_WALK);

	lgcUtilSetTargetPosition(obj, ((s32)obj->aux->csCavemanCarry.tx) * TILE_W, TOFIXED(8));

	return EVRESULT_PROCEED;
}

EVRESULT_T lgcC0CavemanStop(TObject *obj, void *data)
{
	lgcUtilClearTargets(obj);

	return EVRESULT_PROCEED;
}

EVRESULT_T lgcC0LogoWait(TObject *obj, void *data)
{
	TVector2D		impulse;

	lgcUtilClearTargets(obj);

	return EVRESULT_PROCEED;
}

EVRESULT_T lgcC0LogoBeThrown(TObject *obj, void *data)
{
	TVector2D		impulse;

	impulse.x = TOFIXED(-80.0);
	impulse.y = TOFIXED(-110.0);

	lgcUtilApplyImpulse(obj, &impulse);

	return EVRESULT_PROCEED;
}

EVRESULT_T lgcC0LogoCollision(TObject *obj, void *data)
{
	obj->vel.x = obj->vel.y = 0;
	obj->flags &= ~OBJFLAG_PHYSIC;

	return EVRESULT_PROCEED;
}



const TEvent lgcCutsceneCavemanCarry[5][STATE_EVENTS_MAX] = 
{
	{ // 0: Walking into the screen, [carrying an object]
		{ EVENT_ARRIVAL, STM_DEFAULT, lgcC0CavemanThrow, 1 },
	},

	{ // 1: Throwing an object
		{ EVENT_ANIMATION, STM_DEFAULT, lgcC0CavemanWait, 2 },
	},

	{ // 2: Waiting a bit
		{ EVENT_TIMER, 3000, lgcC0CavemanWalkAway, 3 },
	},

	{ // 3: Walking out of the screen

		{ EVENT_ARRIVAL, STM_DEFAULT, lgcC0CavemanStop, 4 },
	},

	{ // X: Do nothing
		{ EVENT_NULL }
	},

};

const TEvent lgcCutsceneLogo[4][STATE_EVENTS_MAX] = 
{
	{ // 0: Being 'carried' by a Caveman
		{ EVENT_ARRIVAL, STM_DEFAULT, lgcC0LogoWait, 1 },
	},

	{ // 1: Being thrown
		{ EVENT_TIMER, 450, lgcC0LogoBeThrown, 2 },
	},

	{ // 2: Flying
		{ EVENT_COLLISION, OBJTYPE_COLLIDER, lgcC0LogoCollision, 3 },
	},

	{ // X: Do nothing
		{ EVENT_NULL }
	},

};


EVRESULT_T lgcGuiPaymentHide(TObject *obj, void *data)
{
	lgcUtilClearTargets(obj);
	lgcUtilShowObject(obj, FALSE);

	return EVRESULT_PROCEED;
}

const TEvent lgcGuiPayment[4][STATE_EVENTS_MAX] = 
{
	{ // 0: Do nothing
		{ EVENT_NULL }
	},

	{ // 1: Moving up
		{ EVENT_TIMER, 1500, lgcGuiPaymentHide, 0 },
	},

};

