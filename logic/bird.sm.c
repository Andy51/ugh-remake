

EVRESULT_T lgcBirdFly(TObject *obj, void *data)
{
	s32				target;

	obj->sprite = resChangeSprite(obj->sprite, SPRITE_BIRD_FLY);

	// Determine the direction in which to fly
	if(obj->pos.x < 0)
		target = SCREEN_W+20;
	else
		target = -20;

	lgcUtilSetTargetPosition(obj, target, TOFIXED(obj->aux->bird.speed));

	// Since we are nuisace, we fly just where the player is now :)
	obj->pos.y = gPlayer->pos.y;

	lgcUtilShowObject(obj, TRUE);
	
	return EVRESULT_PROCEED;
}

EVRESULT_T lgcBirdWait(TObject *obj, void *data)
{
	lgcUtilShowObject(obj, FALSE);
	
	return EVRESULT_PROCEED;
}

EVRESULT_T lgcBirdCollidePlayer(TObject *obj, void *data)
{
	// Apply the damage
	mexAddPlayerHealth(BIRD_DAMAGE);

	// Push the chopper
	if((gPlayer->flags & OBJFLAG_PHYSIC) == 0)
		lgcUtilApplyImpulseLift(gPlayer, &obj->vel, TOFIXED(60.0));
	else
		lgcUtilApplyImpulse(gPlayer, &obj->vel);

	// Continue the flight
	return EVRESULT_PROCEED;
}

EVRESULT_T lgcBirdFall(TObject *obj, void *data)
{
	obj->flags |= OBJFLAG_PHYSIC;
	obj->flags &= ~OBJFLAG_TARGET;

	obj->vel.x /= 2;
	
	obj->sprite = resChangeSprite(obj->sprite, /*SPRITE_BIRD_FALL*/ SPRITE_BIRD_FLY);

	return EVRESULT_PROCEED;
}

EVRESULT_T lgcBirdOffscreeen(TObject *obj, void *data)
{
	obj->flags &= ~OBJFLAG_PHYSIC;

	obj->pos.x = TOFIXED(-20);

	return EVRESULT_PROCEED;
}


const TEvent lgcBird[5][STATE_EVENTS_MAX] = 
{
	{ // 0: Waiting for interval timeout
		{ EVENT_TIMER, STM_CURRENT_OBJECT_DELAY, lgcBirdFly, 1 },
	},

	{ // 1: Flying thru the screen

		{ EVENT_ARRIVAL, STM_DEFAULT, lgcBirdWait, 0 },
		{ EVENT_COLLISION, OBJTYPE_CHOPPER, lgcBirdCollidePlayer, 2 },
		{ EVENT_COLLISION, OBJTYPE_STONE, lgcBirdFall, 3 },
	},

	{ // 2: Flying thru the screen, after the collision, so that it can collide with player only once
		{ EVENT_ARRIVAL, STM_DEFAULT, lgcBirdWait, 0 },
		{ EVENT_COLLISION, OBJTYPE_STONE, lgcBirdFall, 3 },
	},

	{ // 3: Falling
		{ EVENT_OFFSCREEN, STM_DEFAULT, lgcBirdOffscreeen, 4 },
	},

	{ // 4: Cooldown :)
		{ EVENT_TIMER, 6000, lgcBirdFly, 1 },
	}

};
