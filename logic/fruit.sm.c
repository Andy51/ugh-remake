
EVRESULT_T lgcFruitGetEaten(TObject *obj, void *data)
{
	// Delete the fruit
	mexDeleteObject(obj);
	
	// Increase the health
	mexAddPlayerHealth(FRUITS_CALORIES);
	
	return EVRESULT_PROCEED;
}

EVRESULT_T lgcFruitBlink(TObject *obj, void *data)
{
	dbg("lgcFruitBlink");
	// Blinking
	if(obj->flags & OBJFLAG_HIDDEN)
		lgcUtilShowObject(obj, TRUE);
	else
		lgcUtilShowObject(obj, FALSE);
	
	// Cancel the transfer so that timers are not reset
	return EVRESULT_CANCEL;
}

EVRESULT_T lgcFruitRot(TObject *obj, void *data)
{
	dbg("lgcFruitRot");
	// Delete the fruit
	mexDeleteObject(obj);
	
	return EVRESULT_PROCEED;
}

const TEvent lgcFruit[2][STATE_EVENTS_MAX] = 
{
	{ // 0: Just lying still :)
		// No matter what target state is - we are being deleted anyway
		{ EVENT_COLLISION, OBJTYPE_CHOPPER, lgcFruitGetEaten, 0 },
		{ EVENT_TIMER, 4000, NULL, 1 },
	},

	{ // 1: Getting rotten
		{ EVENT_COLLISION, OBJTYPE_CHOPPER, lgcFruitGetEaten, 0 },
		{ EVENT_TIMER, 450, lgcFruitBlink, 1 },		
		{ EVENT_TIMER, 5000, lgcFruitRot, 1 },		
	}
};


