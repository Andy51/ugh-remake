

EVRESULT_T lgcTriopChangeDirection(TObject *obj, void *data)
{
	TPlatform				*platform;
	EVRESULT_T				result;
	s32						target;
	s32						w;
	

	w = resGetSpriteWidth(obj->sprite) / 2;

	platform = &gLevelPlatforms[obj->curPlatform];

	// Determine the other platform edge than we just walked to
	if( obj->sprite->direction == 0)
		target = platform->right - w;
	else
		target = platform->left + w;

	// Go to the platform's edge
	lgcUtilSetTargetPosition(obj, target, TOFIXED(obj->aux->triopterus.speed));

	return EVRESULT_PROCEED;
}

EVRESULT_T lgcTriopGetAngry(TObject *obj, void *data)
{
	TPlatform				*platform;
	TObject					*disturbance;
	EVRESULT_T				result;
	s32						target;
	s32						w;
	
	lgcUtilClearTargets(obj);

	// Get angry :<
	obj->sprite = resChangeSprite(obj->sprite, SPRITE_TRIOPTERUS_ANGRY);

	disturbance = (TObject*)data;

	// Turn to the disturbance
	obj->sprite->direction = ( (obj->pos.x - disturbance->pos.x) > 0 ? 0 : 1 );

	// Remeber it!
	obj->passenger = disturbance;
	
	return EVRESULT_PROCEED;
}

EVRESULT_T lgcTriopDash(TObject *obj, void *data)
{
	TPlatform				*platform;
	TObject					*disturbance;
	EVRESULT_T				result;
	s32						target;
	s32						w;
	
	dbg("dash!");
	obj->sprite = resChangeSprite(obj->sprite, SPRITE_TRIOPTERUS_RUN);

	lgcUtilSetTargetObject( obj, obj->passenger, TOFIXED(obj->aux->triopterus.speed * 2) );

	return EVRESULT_PROCEED;
}


EVRESULT_T lgcTriopCalmDown(TObject *obj, void *data)
{
	TPlatform				*platform;
	EVRESULT_T				result;
	s32						target;
	s32						w;
	
	lgcUtilClearTargets(obj);

	w = resGetSpriteWidth(obj->sprite) / 2;

	platform = &gLevelPlatforms[obj->curPlatform];

	obj->sprite = resChangeSprite(obj->sprite, SPRITE_TRIOPTERUS_WALK);

	// Determine the current platform edge than we just walked to
	if( obj->sprite->direction == 0)
		target = platform->left + w;
	else
		target = platform->right - w;

	// Go to the platform's edge
	lgcUtilSetTargetPosition(obj, target, TOFIXED(obj->aux->triopterus.speed));

	return EVRESULT_PROCEED;
}


EVRESULT_T lgcTriopWakeUp(TObject *obj, void *data)
{
	TPlatform				*platform;
	EVRESULT_T				result;
	s32						target;
	s32						w;

	// If player is there
	if(gPlayer->curPlatform == obj->curPlatform)
	{
		lgcTriopGetAngry(obj, gPlayer);
		return EVRESULT_TOSTATE + 1;
	}
	
	return lgcTriopCalmDown(obj, data);
}


EVRESULT_T lgcTriopGore(TObject *obj, void *data)
{
	TPlatform			*platform;
	TObject				*collider;
	EVRESULT_T			result;
	s32					target;
	s32					w;

	collider = lgcUtilGetCollider((TCollisionEvData*)data, obj);

	// Apply the damage
	mexAddPlayerHealth(TRIOP_DAMAGE);

	// Push the chopper
	lgcUtilApplyImpulseLift(/*collider*/ gPlayer, &obj->vel, TOFIXED(80.0));

	return lgcTriopCalmDown(obj, data);
}

EVRESULT_T lgcTriopGetBashed(TObject *obj, void *data)
{
	TObject				*collider;
	TObject				*fruit;

	lgcUtilClearTargets(obj);

	collider = lgcUtilGetCollider((TCollisionEvData*)data, obj);

	collider->vel.y = -collider->vel.y / 2;

	obj->sprite = resChangeSprite(obj->sprite, SPRITE_TRIOPTERUS_BASHED);

	return EVRESULT_PROCEED;
}


const TEvent lgcTriopterus[4][STATE_EVENTS_MAX] = 
{
	{ // 0: Walking on the platform

		{ EVENT_ARRIVAL, STM_DEFAULT, lgcTriopChangeDirection, 0 },
		{ EVENT_LANDING, STM_CHOPPER_OBJECT, lgcTriopGetAngry, 1 },
		{ EVENT_COLLISION, OBJTYPE_STONE, lgcTriopGetBashed, 3 },

	},

	{ // 1: Angry!

		{ EVENT_TIMER, 1500, lgcTriopDash, 2 },
		{ EVENT_TAKEOFF, STM_DEFAULT, lgcTriopCalmDown, 0 },
	},

	{ // 2: Dashing

		{ EVENT_COLLISION, OBJTYPE_CHOPPER, lgcTriopGore, 0 },
		{ EVENT_TAKEOFF, STM_DEFAULT, lgcTriopCalmDown, 0 },
	},

	{ // 3: Staying bashed

		{ EVENT_TIMER, 6000, lgcTriopWakeUp, 0 },
	}
};

