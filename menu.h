
#ifndef MENU__H
#define MENU__H

#include "common.h"
#include "mechanix.h"
#include "gui.h"


BOOL menuDrawMenu();

BOOL menuInitState(u32 init);
BOOL menuTerminateState();
u32 menuUpdate();
void menuHandleKeys(u32 keysPressed, u32 keysReleased, u32 keysHold);

static BOOL menuUpdateObjects();

#endif
