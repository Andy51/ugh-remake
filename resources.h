#ifndef RESOURCES__H
#define RESOURCES__H

#include <ATI.h>
#include <filesystem.h>
#include <utilities.h>
#include <mem.h>
#include "common.h"
#include "bmp.h"
#include "gfx.h"

#define FONTS_COUNT			2

#define TILE_W				11
#define TILE_H				11

#define	TILES_COUNT			174

#define MAX_SPRITES			10

#define FRAME_TIME			150

#define LEVEL_W				20
#define LEVEL_H				16
#define LEVEL_SIZE			(LEVEL_W*LEVEL_H)

#define RESOURCES_DIR		L"res/"
enum
{
	SPRITE_NULL = 0,
	SPRITE_CHOPPER,
	SPRITE_CHOPPER_EMPTY,
	SPRITE_PROPELLER,

	SPRITE_CAVEMAN_WALK,
	SPRITE_CAVEMAN_WALK_IN,
	SPRITE_CAVEMAN_WALK_OUT,
	SPRITE_CAVEMAN_WAIT,
	SPRITE_CAVEMAN_FALL,
	SPRITE_CAVEMAN_SWIM,

	SPRITE_STONE,
	SPRITE_STONE_FALL,

	SPRITE_TREE,
	SPRITE_TREE_BASHED,

	SPRITE_SPEECH_BUBBLE,

	/* Fruit bonuses */

	SPRITE_FRUIT_MIN,
	SPRITE_FRUIT_APPLE = SPRITE_FRUIT_MIN,
	SPRITE_FRUIT_BANANA,
	SPRITE_FRUIT_CHERRY,
	SPRITE_FRUIT_COCONUT,
	SPRITE_FRUIT_PEAR,
	SPRITE_FRUIT_ORANGE,
	SPRITE_FRUIT_KIWI,
	SPRITE_FRUIT_STRAWBERRY,
	SPRITE_FRUIT_MELON,

	SPRITE_FRUIT_MAX = SPRITE_FRUIT_MELON,

	SPRITE_BIRD_FLY,
	SPRITE_DINO,

	SPRITE_TRIOPTERUS_RUN,
	SPRITE_TRIOPTERUS_WALK,
	SPRITE_TRIOPTERUS_ANGRY,
	SPRITE_TRIOPTERUS_BASHED,

	SPRITE_OLDMAN_WALK,
	SPRITE_OLDMAN_WALK_IN,
	SPRITE_OLDMAN_WALK_OUT,
	SPRITE_OLDMAN_WAIT,
	SPRITE_OLDMAN_FALL,

	SPRITE_WOMAN_WALK,
	SPRITE_WOMAN_WALK_IN,
	SPRITE_WOMAN_WALK_OUT,
	SPRITE_WOMAN_WAIT,
	SPRITE_WOMAN_FALL,
	SPRITE_WOMAN_SWIM,

	SPRITE_CS0_CAVEMAN_CARRY,
	SPRITE_CS0_CAVEMAN_THROW,
	SPRITE_CS0_CAVEMAN_SMILE,

	SPRITE_LOGO

};

typedef struct
{
	u16				magic;
	u16				version;

	u32				levelsCount;
	u32				platformsOff;
	u32				platformsCount;
	u32				passengersOff;
	u32				passengersCount;
	u32				routesOff;
	u32				routesCount;
	u32				stringsOff;
	u32				stringsSize;

} TLevelsHeader;


typedef struct
{
	u16				nameTblOff;				// String table offset
	u16				platformsTblOff;		// TPlatform
	u16				passengersTblOff;		// TPassenger
	u16				objectsTblOff;			// Unused

	u16				routesToComplete;		// Number of completed routes to win

	s16				firstPlayerX;			// Start position
	s16				firstPlayerY;

	s16				waterLevel;
	s16				waterSpeed;

} TLevel;

typedef struct
{
	s16				left;		// X
	s16				right;		// X
	s16				level;		// Y
	s16				cave;		// X
	s16				stop;
	s16				object;
	s16				number;

} TPlatform;


typedef struct
{
	u32				completeRoutes;
	u32				passengerDeaths;
	FIXED			playerHealth;
	BOOL			playerCrash;
	s32				playerCrashTimer;
	s32				livesCount;
	s32				score;

} TGameState;


typedef struct
{
	u8				w;
	u8				h;
	u8				stride;

	u8				*map;
	u8				*bits;

	AHICHAR_T		*buffer;

} TFont;

typedef struct
{
	u8				w;
	u8				h;
	u8				stride;

	u8				pad;

} TFontHeader;

typedef struct
{
	char			magic[3];
	u8				ver;
	u16				width;
	u16				height;
	u16				bpp;

} TImageHeader;


extern FILE_HANDLE_T		gLevelsData;
extern AHIBITMAP_T			gTileArray;
//extern AHIBITMAP_T		*gTiles;
extern AHIPOINT_T			*gTiles;

extern TFont				gFonts[FONTS_COUNT];

extern u8					*gCurrentLevelMap;

extern TLevel				*gLevels;
extern TLevel				*gCurrentLevel;
extern TPlatform			*gPlatforms;
extern TPlatform			*gLevelPlatforms;
extern TRouteStop			*gRoutes;
extern TGameObject			*gPassengers;
extern char					*gStrings;

extern TGameState			gGameState;



BOOL resInit();
BOOL resTerminate();

void resInitHomeDir(WCHAR *elfPath);
WCHAR *resGetFullPathExt(char *filename, const char *extension);
WCHAR *resGetFullPath(char *filename);
void* resLoadFile(WCHAR *path);

BOOL resLoadMap(char *filename);
BOOL resLoadLevelData(u32 levelIdx);
BOOL resLoadTiles(char *filename);

BOOL resLoadSprites(char *filename);
BOOL resFreeSpriteProto(TSpriteProto *proto);
BOOL resCleanSpriteProtos();
TSprite* resCreateSprite(u32 ID, BOOL patched);
TSprite* resCreateEmptySprite(s32 w, s32 h);
TSprite* resChangeSprite(TSprite *prevSprite, u32 newID);
BOOL resFreeSprite(TSprite *spr);
s32 resGetSpriteWidth(TSprite *spr);
s32 resGetSpriteHeight(TSprite *spr);

BOOL resLoadFont(char *filename, u8 fontID);

#endif

