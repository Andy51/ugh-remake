#ifndef MAIN__H
#define MAIN__H

#include <loader.h>
#include <mem.h>
#include <tasks.h>
#include <ATI.h>

#include "common.h"
#include "resources.h"
#include "mechanix.h"
#include "map.h"
#include "gfx.h"
#include "gui.h"
#include "dbg.h"
#include "states.h"


#define TASK_STACK_SIZE		0x5000
#define	TASK_PRIORITY		0x18


extern BOOL appTerminate();

void mainTask(void *arg);
BOOL mainTaskInit();
BOOL mainTaskTerminate();
void mainHandleInput();
u32 mainUpdateTick();


// PUBLIC

void mainTaskCreate();


#endif
