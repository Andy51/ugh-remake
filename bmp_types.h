#ifndef _BMP_TYPES_H_
#define _BMP_TYPES_H_


#ifndef WIN32
	#define BITMAP_SIGNATURE 0x424D //'BM' (BE-addressing)
#else
	#define BITMAP_SIGNATURE 0x4D42 //'BM' (BE-addressing)
#endif

#ifdef PACK1
	#undef PACK1
#endif
#ifdef PACK2
	#undef PACK2
#endif


#ifdef __arm
	#define PACK1 __packed
	#define PACK2 
#elif defined(__GNUC__)
	#define PACK1 
	#define PACK2 __attribute__ ((packed))
#else
	#define PACK1 
	#define PACK2 
	#pragma pack(push,1)
#endif

typedef  struct {
	unsigned short int Signature;
	PACK1 unsigned int Size PACK2;
	PACK1 unsigned int Reserved PACK2;
	PACK1 unsigned int BitsOffset PACK2;
} BITMAP_FILEHEADER;

#define BITMAP_FILEHEADER_SIZE 14

//#ifndef __GNUC__
#ifndef __arm
//#pragma pack()
#endif

typedef struct {
	unsigned int HeaderSize;
	int Width;
	int Height;
	unsigned short int Planes;
	unsigned short int BitCount;
} BITMAP_COREHEADER;

#define BITMAP_COREHEADER_SIZE 16


// Windows V3	BITMAPINFOHEADER
typedef struct {
	unsigned int HeaderSize;
	int Width;
	int Height;
	unsigned short int Planes;
	unsigned short int BitCount;
	unsigned int Compression;
	unsigned int SizeImage;
	int PelsPerMeterX;
	int PelsPerMeterY;
	unsigned int ClrUsed;
	unsigned int ClrImportant;
} BITMAP_HEADER_V3;

#define BITMAP_HEADER_V3_SIZE 40


typedef struct {
	unsigned int RedMask;
	unsigned int GreenMask;
	unsigned int BlueMask;
} BITMAP_HEADER_BITFIELDS;

#define BITMAP_HEADER_BITFIELDS_SIZE 12

typedef struct {
	unsigned int HeaderSize;
	int Width;
	int Height;
	unsigned short int Planes;
	unsigned short int BitCount;
	unsigned int Compression;
	unsigned int SizeImage;
	int PelsPerMeterX;
	int PelsPerMeterY;
	unsigned int ClrUsed;
	unsigned int ClrImportant;
	unsigned int RedMask;
	unsigned int GreenMask;
	unsigned int BlueMask;
	unsigned int AlphaMask;
	unsigned int CsType;
	unsigned int Endpoints[3]; // see http://msdn2.microsoft.com/en-us/library/ms536569.aspx
	unsigned int GammaRed;
	unsigned int GammaGreen;
	unsigned int GammaBlue;
} BITMAP_HEADER_V4;

#define BITMAP_HEADER_V4_SIZE 84


typedef enum {
	BI_RGB = 0,		//	none		Most common
	BI_RLE8,		//	RLE			8-bit/pixel	Can be used only with 8-bit/pixel bitmaps
	BI_RLE4,		//	RLE			4-bit/pixel	Can be used only with 4-bit/pixel bitmaps
	BI_BITFIELDS,	//	Bit field	Can be used only with 16 and 32-bit/pixel bitmaps.
	BI_JPEG,		//	JPEG		The bitmap contains a JPEG image
	BI_PNG			//	PNG			The bitmap contains a PNG image
} BITMAP_COMPRESSION_METHOD;



typedef unsigned short U_BGR565;


//#define BITMAP_HEADER_V1_SIZE 12
//#define BITMAP_HEADER_V2_SIZE 64
#if !defined(__arm) && !defined(__GNUC__)
	#pragma pack(pop)
#endif

#endif // _BMP_TYPES_H_
