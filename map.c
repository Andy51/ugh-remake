
#include "map.h"

#define WATER_ANIMATION_TIMEOUT		100
#define WATER_STEP					2
#define WATER_STEP_MASK				(~(WATER_STEP-1))

#define ROUND_UP(_x_,_b_)			(((_x_)+(_b_)-1) & (~((_b_)-1)))

FIXED				gWaterLevel;
FIXED				gWaterLevelSpeed;

static s32			sWaterPrevLevel;
static s32			sWaterWaveTime;
static s32			sWaterAnimationTimeout;

BOOL mapInit()
{
	mapReset();
	return TRUE;
}

BOOL mapTerminate()
{
	return TRUE;
}

BOOL mapReset()
{
	gWaterLevel = -1;
	return TRUE;
}

BOOL mapSetLevel(u32 newlevel)
{
	resLoadLevelData(newlevel);

	//gWaterLevel = 60<<FP_BASE;
	if(gWaterLevel < 0)
		gWaterLevel = (gCurrentLevel->waterLevel) << FP_BASE;

	gWaterLevelSpeed = gCurrentLevel->waterSpeed;

	// Reset water state
	sWaterPrevLevel = SCREEN_H;
	sWaterWaveTime = 0;
	sWaterAnimationTimeout = WATER_ANIMATION_TIMEOUT;

	return TRUE;
}

BOOL mapRender()
{
	AHIRECT_T		dstRect = {0, 0, TILE_W, TILE_H};
	AHIPOINT_T		srcPt = {0, 0};
	u32				x, y;
	u32				i, j;


	/*AhiDrawBitmapBlt(gCtx, &dstRect, &srcPt, &gTiles[0], NULL, 0);
	dstRect.y1 += TILE_H;
	dstRect.y2 += TILE_H;
	AhiDrawBitmapBlt(gCtx, &dstRect, &srcPt, &gTileArray, NULL, 0);*/



	for(j=0; j < LEVEL_H; j++)
	{
		for(i=0; i < LEVEL_W; i++)
		{
			//AhiDrawBitmapBlt(gCtx, &dstRect, &srcPt, &gTiles[level[i+j*LEVEL_W]], NULL, 0);
			AhiDrawBitmapBlt(gCtx, &dstRect, &gTiles[gCurrentLevelMap[i+j*LEVEL_W]], &gTileArray, NULL, 0);

			dstRect.x1 += TILE_W;
			dstRect.x2 += TILE_W;
		}

		dstRect.x1 = 0;
		dstRect.x2 = TILE_W;
		dstRect.y1 += TILE_H;
		dstRect.y2 += TILE_H;
	}
		
	return TRUE;
}

//#define WATER_OFFS_SZ	8	
int waterOffs[] = { -2, -1, 0, 1, 2, 1, 0, -1 };


int getWaterOff(int idx)
{
	return waterOffs[idx & 7];
}





BOOL updateWaterLevel(int y)
{

	AHIRECT_T			dstRect;
	AHIPOINT_T			srcPt;
	int					t = sWaterWaveTime;

	/*
		Alpha-blend the water region
	*/

	AhiDrawRopSet( gCtx, AHIROP3(AHIROP_PATCOPY) );
	AhiDrawBrushFgColorSet(gCtx, ATI_565RGB(0x66, 0x66, 0xBB));

	AhiDrawAlphaSet(gCtx, AHIALPHA(0x7));

	srcPt.x = 0;
	srcPt.y = 0;
	dstRect.x1 = 0;
	dstRect.y1 = y;
	dstRect.x2 = SCREEN_W;
	dstRect.y2 = sWaterPrevLevel;

	//AhiDrawSpans(gCtx, &srcRect, 1, 0);
	AhiDrawAlphaBlt(gCtx, &dstRect, &srcPt, NULL, NULL, AHIFLAG_ALPHA_SOLID);

	AhiDrawRopSet( gCtx, AHIROP3(AHIROP_SRCCOPY) );


	/*
		Make initial wave positioning
	*/

	AhiDrawSurfSrcSet(gCtx, gDrawSurf, 0);

	for( ; y < sWaterPrevLevel; y += WATER_STEP)
	{
		dstRect.x1 = getWaterOff(y/2 + t);
		dstRect.x2 = dstRect.x1 + SCREEN_W;
		dstRect.y1 = y;
		dstRect.y2 = dstRect.y1 + WATER_STEP;
			
		srcPt.x = 0;
		srcPt.y = y;

		AhiDrawBitBlt(gCtx, &dstRect, &srcPt);
	}

	return TRUE;

}

BOOL mapRenderWater()
{
	int t;

	AHIRECT_T			dstRect;
	AHIPOINT_T			srcPt;
	int					x, y;
	int					prevY;

	sWaterAnimationTimeout -= gTick;

	
	gWaterLevel += gWaterLevelSpeed * gTick;
	
	y = (gWaterLevel>>FP_BASE);
	y = ROUND_UP(y,WATER_STEP);

	AhiDrawClipDstSet( gCtx, NULL );
	
	if(sWaterAnimationTimeout <= 0)
	{
		AhiDrawSurfSrcSet(gCtx, gDrawSurf, 0);

		sWaterAnimationTimeout = WATER_ANIMATION_TIMEOUT;

		if(sWaterPrevLevel != y)
		{
			updateWaterLevel(y);
			t = sWaterPrevLevel;
			sWaterPrevLevel = y;
			y = t;
		}

		t = sWaterWaveTime++;

//		y = (((gWaterLevel>>FP_BASE)+WATER_STEP-1) & WATER_STEP_MASK );

		for( ; y < SCREEN_H; y += WATER_STEP)
		{
			dstRect.x1 = getWaterOff(y/2 + t);
			dstRect.x2 = dstRect.x1 + SCREEN_W;
			dstRect.y1 = y;
			dstRect.y2 = dstRect.y1 + WATER_STEP;
				
			srcPt.x = getWaterOff(y/2 + (t - 1));
			srcPt.y = y;

			AhiDrawBitBlt(gCtx, &dstRect, &srcPt);
		}

		t++;
		
	}

	AhiDrawClipDstSet( gCtx, &gScreenRect );

	return TRUE;
}

