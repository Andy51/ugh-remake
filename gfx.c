
#include "gfx.h"

AHIDEVCONTEXT_T				gCtx;

AHISURFACE_T				gDrawSurf;
AHISURFACE_T				gDispSurf;
static AHISURFACE_T			sDALDrawSurf;

u32							gCacheSize;

AHIRECT_T					gScreenRect = { 0, 0, SCREEN_W, SCREEN_H };

u32							sFlushCount;

extern u8					*Class_dal;

u32							sMemCfgFlags;
u32							sSurfBlockOffset;

//TGfxMemoryCfg				sMemoryCfg;

s32 resGetSpriteWidth(TSprite *spr);
BOOL gfxSetDispMode(u32 landscape);

#define AHIFLAG_EXTERNAL_MEMORY           0x00000020
#define AHIFLAG_INTERNAL_MEMORY           0x00000040

BOOL gfxInit()
{
	AHIDEVICE_T			device;
	u32					size, align;
	u32					status;

	/*
		Create new Context
	*/

	device = ldrGetAhiDevice();

	status = AhiDevOpen( &gCtx, device, "Dummy", 0 );

	if( status != 0 )
		return FALSE;

	dbgf("gCtx = 0x%08X", gCtx);



	/*
		Memory tests
	*/

	sMemCfgFlags = gfxDetectMemoryCfg();

	if(sMemCfgFlags == 0)
	{
		dbg(" *ERROR: Failed to detect memory configuration!");
		AhiDevClose(gCtx);
		return FALSE;
	}

	if(sMemCfgFlags & GFX_CFG_FREE_CACHE)
	{
		dbg("Deallocating DAL cache");

		DAL_FreeCache();

		dbg("Free wo/cache:");
		gfxOutputVideomemInfo();
	}

	/*
		Surfaces init
	*/

	#ifdef LANDSCAPE_RIGHT
	if( gfxSetDispMode(1) == FALSE )
	#else
	if( gfxSetDispMode(3) == FALSE )
	#endif
		return FALSE;

	
	/*
		Drawing settings init
	*/

	AhiDrawRopSet( gCtx, AHIROP3(AHIROP_SRCCOPY));

	AhiDrawSurfDstSet( gCtx, gDrawSurf, 0 );
	
	AhiDrawClipDstSet( gCtx, &gScreenRect );
	AhiDrawClipSrcSet( gCtx, NULL );

	
	return TRUE;
}

BOOL gfxDumpDAL(u32 offset, u32 	size)
{
	s32				i;
	u32				*val;

	val = (u32*)(Class_dal + offset);

	dbg("DAL Class dump start");

	for(i = size-4; i >= 0; i -= 4)
	{
		dbgf(" 0x%.2X: %.8X", offset, *val);

		val++;
		offset += 4;
	}

	dbg("DAL Class dump end");

	return TRUE;
}

#define GFX_DAL_SEARCH_REGION			0x100

u32 gfxFindSurfBlock(AHISURFACE_T dispSurf, AHISURFACE_T drawSurf)
{
	u32				offset = 0;
	u32				*dal;

	dal = (u32*)(Class_dal);

	while(offset < GFX_DAL_SEARCH_REGION)
	{
		/* 
		    Find the block of
		    o-4: ...
		    o  : curDrawSurf
		    o+4: curDispSurf
		    o+8: ...
		*/
		 
		if(*dal == (u32)dispSurf)
		{
			if(*(dal+1) == (u32)drawSurf)
			{
				break;
			}
		}

		dal++;
		offset += 4;
	}

	// Not found?
	if(offset >= GFX_DAL_SEARCH_REGION)
		offset = 0;

	return offset;
}

u32 gfxFindCacheBlock(AHISURFACE_T cacheSurf, AHIPOINT_T *cacheSize)
{
	u32				offset = 0;
	u32				*dal;

	dal = (u32*)(Class_dal);

	while(offset < GFX_DAL_SEARCH_REGION)
	{
		/* 
		    Find the block of
		    o-04: 0
		    o+00: [allocated cacheSurf]
		    o+04: cacheSize.x
		    o+08: cacheSize.y
		    o+0C: reused cacheSurf
		*/
		 
		if(*dal == (u32)cacheSurf)
		{
			if( (*(dal-2) == (u32)cacheSize->x) &&
				(*(dal-1) == (u32)cacheSize->y)/* &&
				(*(dal-4) == 0) */ )
			{
				offset -= 12;
				break;
			}
		}

		dal++;
		offset += 4;
	}

	// Not found?
	if(offset >= GFX_DAL_SEARCH_REGION)
		offset = 0;

	return offset;
}

u32 gfxDetectMemoryCfg()
{
	AHIPOINT_T			cacheSize;
	AHISURFACE_T		cacheSurf;
	AHISURFACE_T		dalDrawSurf, ahiDispSurf;
	u32					intsz, extsz, align;
	u32					cacheBlockOffset, flags;
	s32					delta;

	/* 
	    Autodetection based on initial free intmem
	*/

	flags = 0;
	
	intsz = extsz = 0;

	dbg("Trying to detect memory configuration...");

	AhiSurfGetLargestFreeBlockSize( gCtx, AHIFMT_8BPP, &intsz, &align, AHIFLAG_INTERNAL_MEMORY);
	dbgf("Initial intmem free: %d", intsz);

	AhiSurfGetLargestFreeBlockSize( gCtx, AHIFMT_8BPP, &extsz, &align, AHIFLAG_EXTERNAL_MEMORY);
	dbgf("Initial extmem free: %d", extsz);

	cacheSize = DAL_GetCacheSize();

	//gCacheSize = cacheSize.x * cacheSize.y;

	dbgf("DAL Cache size: %d x %d", cacheSize.x, cacheSize.y);

	cacheSurf = DAL_GetCachedSurface(cacheSize);

	dbgf("DAL Cached surface: 0x%X", cacheSurf);

	dalDrawSurf = DAL_GetDrawingSurface(DISPLAY_MAIN);
	dbgf("DAL_GetDrawingSurface returned: 0x%X", dalDrawSurf);

	AhiDispSurfGet(gCtx, &ahiDispSurf);
	dbgf("Current display surface: 0x%X", ahiDispSurf);

	sSurfBlockOffset = gfxFindSurfBlock(ahiDispSurf, dalDrawSurf);

	if(sSurfBlockOffset == 0)
	{
		dbg(" *ERROR: Surfs block offset not found!");
		return 0;
	}

	dbgf("Found the surfs block at offset 0x%X", sSurfBlockOffset);

	cacheBlockOffset = gfxFindCacheBlock(cacheSurf, &cacheSize);

	if(cacheBlockOffset == 0)
	{
		dbg(" *ERROR: Cache block offset not found!");
		return 0;
	}

	dbgf("Found the cache block at offset 0x%X", cacheBlockOffset);

	if(cacheBlockOffset == 0)
	{
		dbg(" *ERROR: Cache block offset not found!");
		return 0;
	}

	delta = cacheBlockOffset - sSurfBlockOffset;

	if(delta < 8)
	{
		dbgf(" *ERROR: Delta is invalid: %d", delta);
		return 0;
	}

	dbgf("Delta is 0x%X", delta);

	gfxDumpDAL(sSurfBlockOffset - 0x10, delta + 0x40);

	
	if(delta > 12) // LTE2 type with preallocated landscape surfaces
	{
		dbg("The configuration seem to have preallocated surfaces");

		flags |= GFX_CFG_HAS_LANDSCAPE_SURFS;
	}
	else
	{
		dbg("The configuration requires surfaces reallocation");

		flags |= GFX_CFG_SURFS_REALLOC;

		if((intsz < GFX_MEMORY_SMALLINTMEM) && (extsz == 0))
		{
			dbg("Cache will be cleared");
			flags |= GFX_CFG_FREE_CACHE;
		}
	}

	
	return flags;
}

BOOL gfxTerminate()
{
	u32					size, align;
	// Surfaces should be on their initial places
	if( (sFlushCount & 1) == 1 )
		gfxFlushDoublebuffer();

	gfxSetDispMode(0);

	AhiSurfGetLargestFreeBlockSize( gCtx, AHIFMT_8BPP, &size, &align, AHIFLAG_INTERNAL_MEMORY);
	dbgf("VRAM free in reset mode: %d", size);

	AhiDevClose( gCtx );

	return TRUE;
}



BOOL gfxChangeDispMode(u32 mode)
{
	AHIPOINT_T			landscapeSize = {SCREEN_W, SCREEN_H};
	AHIPOINT_T			portraitSize = {SCREEN_H, SCREEN_W};
	AHIDEVCONTEXT_T		dalCtx;
	u32					*dal_dispSurf, *dal_drawSurf;
	u32					size, align;
	u32					status;

	dalCtx = DAL_GetDeviceContext(0);
	dbgf("DAL context: 0x%X", dalCtx);

	if(mode & 1) // Landscape modes: 1, 3
	{
		if(sMemCfgFlags & GFX_CFG_HAS_LANDSCAPE_SURFS)
		{
			// Get the preallocated landscape surfs
			dal_drawSurf = (u32*)(Class_dal + sSurfBlockOffset + 0x08);
			dal_dispSurf = (u32*)(Class_dal + sSurfBlockOffset + 0x10);

			status = AhiSurfAlloc(gCtx, &gDispSurf, &landscapeSize, AHIFMT_16BPP_565, AHIFLAG_INTMEMORY);

			if(status != 0)
			{
				dbgf(" *ERROR: failed allocating display surface, status = %d", status);
				return FALSE;
			}

			//gDispSurf = (AHISURFACE_T)(*dal_dispSurf);
			gDrawSurf = (AHISURFACE_T)(*dal_dispSurf);

			dbgf("gDispSurf = 0x%08X", gDispSurf);
			dbgf("gDrawSurf = 0x%08X", gDrawSurf);
		}
		else
		{
			dal_dispSurf = (u32*)(Class_dal + sSurfBlockOffset + 0x00);
			dal_drawSurf = (u32*)(Class_dal + sSurfBlockOffset + 0x04);

			AhiSurfFree(dalCtx, (AHISURFACE_T)(*dal_dispSurf));
			AhiSurfFree(dalCtx, (AHISURFACE_T)(*dal_drawSurf));
	
			dbg("Empty mode:");
			gfxOutputVideomemInfo();

			status = AhiSurfAlloc(gCtx, &gDispSurf, &landscapeSize, AHIFMT_16BPP_565, AHIFLAG_INTMEMORY);

			if(status != 0)
			{
				dbgf(" *ERROR: failed allocating display surface, status = %d", status);
				return FALSE;
			}

			dbgf("gDispSurf = 0x%08X, status = %d", gDispSurf, status);

	
			status = AhiSurfAlloc(gCtx, &gDrawSurf, &landscapeSize, AHIFMT_16BPP_565, 0);

			if(status != 0)
			{
				AhiSurfFree(dalCtx, gDispSurf);
				gDispSurf = NULL;

				dbgf(" *ERROR: failed allocating drawing surface, status = %d", status);
				return FALSE;
			}

			dbgf("gDrawSurf = 0x%08X, status = %d", gDrawSurf, status);
		}

		dbg("Landscape mode:");
		gfxOutputVideomemInfo();
	}
	else // Portrait modes 0 and 2
	{

		if(sMemCfgFlags & GFX_CFG_HAS_LANDSCAPE_SURFS)
		{
			// Get the initial surfs
			dal_drawSurf = (u32*)(Class_dal + sSurfBlockOffset + 0x04);
			dal_dispSurf = (u32*)(Class_dal + sSurfBlockOffset + 0x00);

			dbgf("gDispSurf = 0x%08X, gDrawSurf = 0x%08X", gDispSurf, gDrawSurf);

			if(gDispSurf != NULL)
				AhiSurfFree(gCtx, gDispSurf);

			gDispSurf = (AHISURFACE_T)(*dal_dispSurf);
			gDrawSurf = (AHISURFACE_T)(*dal_dispSurf);

			dbgf("gDispSurf = 0x%08X", gDispSurf);
			dbgf("gDrawSurf = 0x%08X", gDrawSurf);
		}
		else
		{
			dal_dispSurf = (u32*)(Class_dal + sSurfBlockOffset + 0x00);
			dal_drawSurf = (u32*)(Class_dal + sSurfBlockOffset + 0x04);

			dbgf("gDispSurf = 0x%08X, gDrawSurf = 0x%08X", gDispSurf, gDrawSurf);
	
			if(gDispSurf != NULL)
				AhiSurfFree(gCtx, gDispSurf);
	
			if(gDrawSurf != NULL)
				AhiSurfFree(gCtx, gDrawSurf);
	
			dbg("Empty mode:");
			gfxOutputVideomemInfo();
	
			status = AhiSurfAlloc(dalCtx, &gDispSurf, &portraitSize, AHIFMT_16BPP_565, AHIFLAG_INTMEMORY);
	
			if(status != 0)
			{
				dbgf(" *ERROR: failed allocating display surface, status = %d", status);
				return FALSE;
			}
	
			dbgf("gDispSurf = 0x%08X, status = %d", gDispSurf, status);
	
			status = AhiSurfAlloc(dalCtx, &gDrawSurf, &portraitSize, AHIFMT_16BPP_565, 0);
	
			if(status != 0)
			{
				AhiSurfFree(dalCtx, gDispSurf);
				gDispSurf = NULL;

				dbgf(" *ERROR: failed allocating drawing surface, status = %d", status);
				return FALSE;
			}
	
			dbgf("gDrawSurf = 0x%08X, status = %d", gDrawSurf, status);
	
			*dal_dispSurf = (u32)gDispSurf;
			*dal_drawSurf = (u32)gDrawSurf;
		}

		dbg("Portrait mode:");
		gfxOutputVideomemInfo();
	}

	return TRUE;
}


BOOL gfxSetDispMode(u32 mode)
{
	AHIDISPMODE_T		dispMode;
	BOOL				result;
	
	u32					status;
	u32					size, align;


	dbgf("Switching to display mode %d", mode);

	result = gfxChangeDispMode(mode);

	if( result != TRUE )
	{
		dbgf(" *ERROR: Failed to switch display mode!", mode);

		// Failed changing, try to restore the default mode
		if(gfxChangeDispMode(0) != TRUE)
		{
			// Failed restoring, nothing we can do from now on...
			dbgf(" *EPIC FAIL: Failed to recover the original display mode!", mode);

			return FALSE;
		}
	}


	// Set the new display mode
	status = AhiDispModeGet(gCtx, &dispMode);

	AhiDispState( gCtx, 0, 0);

	if(result == TRUE)
	{
		dispMode.rotation = mode;
		AhiDispModeSet( gCtx, &dispMode, 0 );
	}

	AhiDispSurfSet(gCtx, gDispSurf, 0);

	AhiDispState( gCtx, 1, 0);


	dbgf("Done switching to display mode %d", mode);

	return result;
}

void gfxFlushDoublebuffer()
{
	/*AHISURFACE_T			tmpSurf;

	AhiDispSurfSet(gCtx, gDrawSurf, 0);

	tmpSurf = gDrawSurf;
	gDrawSurf = gDispSurf;
	gDispSurf = tmpSurf;

	AhiDrawSurfDstSet(gCtx, gDrawSurf, 0);

	sFlushCount++;*/

	AHIPOINT_T		srcPt;
	AHIRECT_T		dstRect;
	
	dstRect.x1	= 0;
	dstRect.y1	= 0;
	dstRect.x2	= SCREEN_W;
	dstRect.y2	= SCREEN_H;
	
	srcPt.x = 0;
	srcPt.y = 0;
	
	AhiDrawSurfSrcSet( gCtx, gDrawSurf, 0 );
	AhiDrawSurfDstSet( gCtx, gDispSurf, 0 );

	AhiDrawRopSet( gCtx, AHIROP3(AHIROP_SRCCOPY) );
	
	AhiDrawBitBlt( gCtx, &dstRect, &srcPt );

	AhiDrawSurfDstSet( gCtx, gDrawSurf, 0 );
}

BOOL gfxOutputVideomemInfo()
{
	u32					intSize, extSize, align;
	u32					status;
	
	intSize = extSize = 0;
	
	AhiSurfGetLargestFreeBlockSize( gCtx, AHIFMT_8BPP, &intSize, &align, AHIFLAG_INTERNAL_MEMORY);
	AhiSurfGetLargestFreeBlockSize( gCtx, AHIFMT_8BPP, &extSize, &align, AHIFLAG_EXTERNAL_MEMORY);

	dbgf("VMEM I %d E %d", intSize, extSize);

	return TRUE;
}

BOOL gfxDebugDrawSurf(AHISURFACE_T surf, s32 w, s32 h)
{
	AHIRECT_T			dstRect;
	AHIPOINT_T			srcPt = {0, 0};

	AhiDrawSurfSrcSet( gCtx, surf, 0 );

	dstRect.x1 = 0;
	dstRect.y1 = 30;
	dstRect.x2 = dstRect.x1 + w;
	dstRect.y2 = dstRect.y1 + h;

	AhiDrawBitBlt(gCtx, &dstRect, &srcPt);

	return TRUE;

}

BOOL gfxDrawSpritePatch(TSprite *sprite, TVector2D pos)
{
	AHIRECT_T			dstRect;
	AHIPOINT_T			srcPt = {0, 0};
	TSprite				*spr;

	if(sprite->flags & SPRITE_FLAG_NOPATCH)
		return TRUE;

	/*
		Apply patch
	*/

	if(sprite->prevSprite != NULL)
		spr = sprite->prevSprite;
	else
		spr = sprite;

	if((spr->prevPos.x < SCREEN_W) && (spr->prevPos.y < SCREEN_H))
	{
		AhiDrawSurfSrcSet( gCtx, spr->patch, 0 );

		dstRect.x1 = spr->prevPos.x;
		dstRect.y1 = spr->prevPos.y;
		dstRect.x2 = dstRect.x1 + spr->proto->patchSize.x;
		dstRect.y2 = dstRect.y1 + spr->proto->patchSize.y;

		/*dstRect.x1 = 0;
		dstRect.y1 = 0;
		dstRect.x2 = spr->proto->patchSize.x;
		dstRect.y2 = spr->proto->patchSize.y;*/

		AhiDrawBitBlt(gCtx, &dstRect, &srcPt);
	}

	if(sprite->prevSprite != NULL)
	{
		resFreeSprite(sprite->prevSprite);
		sprite->prevSprite = NULL;
	}		

	return TRUE;
}


BOOL gfxUpdateSpritePatch(TSprite *spr, TVector2D pos)
{
	AHIRECT_T			dstRect;
	AHIPOINT_T			srcPt = {0, 0};
	s32					w, h, x, y;

	if(spr->flags & SPRITE_FLAG_NOPATCH)
		return TRUE;

	/*
		Update patch
	*/

	AhiDrawSurfSrcSet( gCtx, gDrawSurf, 0 );
	AhiDrawSurfDstSet( gCtx, spr->patch, 0 );

	dstRect.x1 = 0;
	dstRect.y1 = 0;
	dstRect.x2 = spr->proto->patchSize.x;
	dstRect.y2 = spr->proto->patchSize.y;

	x = pos.x >> FP_BASE;
	y = pos.y >> FP_BASE;

	w = resGetSpriteWidth(spr);
	h = resGetSpriteHeight(spr);

	srcPt.x = x-w/2;
	srcPt.y = y-h/2;

	AhiDrawBitBlt(gCtx, &dstRect, &srcPt);

	AhiDrawSurfDstSet( gCtx, gDrawSurf, 0 );

	return TRUE;
}


BOOL gfxUpdateSpriteFrame(TSprite *spr)
{
	AHIRECT_T			dstRect;
	AHIPOINT_T			srcPt = {0, 0};
	s32					w, h, x, y;

	if( (spr->proto->framesCount == 0) ||
		(spr->flags & SPRITE_FLAG_NOPATCH) )
			return TRUE;

	w = resGetSpriteWidth(spr);
	h = resGetSpriteHeight(spr);
	
	/*
		Draw a sprite
	*/

	dstRect.x1 = spr->proto->patchSize.x;
	dstRect.y1 = 0;
	dstRect.x2 = dstRect.x1 + w;
	dstRect.y2 = dstRect.y1 + h;

	srcPt.x = spr->proto->frames[spr->curFrame];
	srcPt.y = 0;

	AhiDrawSurfDstSet( gCtx, spr->patch, 0 );

	AhiDrawBitmapBlt(gCtx, &dstRect, &srcPt, &spr->proto->bitmap, NULL, 0);

	AhiDrawSurfDstSet( gCtx, gDrawSurf, 0 );
}



BOOL gfxDrawSprite(TSprite *spr, TVector2D pos)
{
	AHIRECT_T			dstRect;
	AHIPOINT_T			srcPt = {0, 0};
	AHICOLOROP_T		srcCop;
	AHIMIRROR_T			mirror;
	s32					w, h, x, y;

	w = resGetSpriteWidth(spr);
	h = resGetSpriteHeight(spr);
	
	/*
		Draw a sprite
	*/


	x = pos.x >> FP_BASE;
	y = pos.y >> FP_BASE;

	dstRect.x1 = x - w/2;
	dstRect.y1 = y - h/2;
	dstRect.x2 = dstRect.x1 + w;
	dstRect.y2 = dstRect.y1 + h;


/*	x = (pos.x + (w<<(FP_BASE/2))) >> FP_BASE;
	y = pos.y >> FP_BASE;

	dstRect.x1 = x;
	dstRect.y1 = y - h/2;
	dstRect.x2 = dstRect.x1 + w;
	dstRect.y2 = dstRect.y1 + h;
*/

	if(spr->flags & SPRITE_FLAG_NOPATCH)
	{
		static counter = 0;
		// Unpatched sprites (does not support mirroring/rotation)

		srcPt.x = 0;
		srcPt.y = 0;

		AhiDrawBitmapBlt(gCtx, &dstRect, &srcPt, &spr->proto->bitmap, NULL, 0);
	
	}
	else if((spr->flags & SPRITE_FLAG_NOBITMAP) == 0)
	{
		// Patched sprites

		srcPt.x = spr->proto->patchSize.x;
		srcPt.y = 0;
	
		srcCop.key	= 0x0000;
		srcCop.mask	= 0xFFFF;
		srcCop.cmp	= AHICMP_NOTEQUAL;
	
		if(spr->direction == 1)
			mirror = AHIMIRR_HORIZONTAL;
		else
			mirror = AHIMIRR_NO;
	
		AhiDrawSurfSrcSet( gCtx, spr->patch, 0 );
		//AhiDrawSurfDstSet( gCtx, gDrawSurf, 0 );
	
		//AhiDrawBitmapBlt(gCtx, &dstRect, &srcPt, spr->bitmap, NULL, 0);
		//AhiDrawBitBlt(gCtx, &dstRect, &srcPt);
	
		//AhiDrawTransBlt(gCtx, &dstRect, &srcPt, NULL, &srcCop);
	
		AhiDrawSpriteBlt(gCtx, &dstRect, &srcPt, NULL, &srcCop, AHIROT_0, mirror, 0);
	}

	// Update previous positions

	spr->prevPos.x = dstRect.x1;
	spr->prevPos.y = dstRect.y1;

	if(spr->drawCallback != NULL)
		spr->drawCallback(spr, x, y);

	return TRUE;

}
