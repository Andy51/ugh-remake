
#include "logic.h"


extern TObject *gPlayer;
extern TObject *gPaymentDisplay;

BOOL			gPlatformOccupied[PLATFORMS_MAX];
u32				gLastPayment;


EVRESULT_T lgcCavemanTellDestination(TObject *obj, void *data);


u8 lgcUtilGetRandomPlatform(TObject *obj)
{
	s32					val;

	val = random(3);
	
	return (u8)val;

}

BOOL lgcUtilRandomFlyout(TObject *obj, TVector2D *from)
{
	s32					rnd;

	rnd = random(6) - 3;
	
	obj->vel.y = FLYOUT_VELOCITY_Y;
	obj->vel.x = FLYOUT_VELOCITY_X * rnd;

	if(from != NULL)
	{
		obj->pos.x = from->x;
		obj->pos.y = from->y;
	}

	obj->flags |= OBJFLAG_PHYSIC;

	return TRUE;
}


BOOL lgcUtilApplyImpulse(TObject *obj, TVector2D *impulse)
{
	obj->vel.x += fpmul(impulse->x, IMPULSE_KOEF);
	obj->vel.y += fpmul(impulse->y, IMPULSE_KOEF);

	obj->flags |= OBJFLAG_PHYSIC;

	return TRUE;
}

BOOL lgcUtilApplyImpulseLift(TObject *obj, TVector2D *impulse, FIXED lift)
{
	TVector2D				impulseLift;

	impulseLift.x = impulse->x;
	impulseLift.y = impulse->y - lift;

	lgcUtilApplyImpulse(obj, &impulseLift);

	if(obj->type == OBJTYPE_CHOPPER)
		lgcRegisterEvent(EVENT_TAKEOFF, (void*)(u32)obj->curPlatform);

	return TRUE;
}

BOOL lgcUtilSetTargetPosition(TObject *obj, s32 target, FIXED speed)
{
	FIXED				delta;

	target = target << FP_BASE;

	obj->target.pos = target;

	delta = target - obj->pos.x;

	if(delta >= 0)
	{
		obj->vel.x = speed;
		obj->sprite->direction = 1;
	}
	else
	{
		obj->vel.x = -speed;
		obj->sprite->direction = 0;
	}
	
	obj->vel.y = 0;

	obj->flags |= OBJFLAG_TARGET;

	return TRUE;
}


BOOL lgcUtilSetTargetObject(TObject *obj, TObject *target, FIXED speed)
{
	FIXED				delta;

	obj->target.obj = lgcUtilFindObjectIndex(target);

	delta = target->pos.x - obj->pos.x;

	if(delta >= 0)
	{
		obj->vel.x = speed;
		obj->sprite->direction = 1;
	}
	else
	{
		obj->vel.x = -speed;
		obj->sprite->direction = 0;
	}
	
	obj->vel.y = 0;

	obj->flags |= OBJFLAG_TARGET_OBJECT;

	return TRUE;
}


BOOL lgcUtilClearTargets(TObject *obj)
{
	FIXED				delta;

	obj->flags &= ~OBJFLAG_MASK_TARGETS;

	return TRUE;
}



BOOL lgcUtilShowObject(TObject *obj, BOOL show)
{
	if(show)
		obj->flags &= ~OBJFLAG_HIDDEN;
	else
	{
		if((obj->flags & OBJFLAG_HIDDEN) == 0)
		{
			obj->flags |= OBJFLAG_HIDING;
		}
	}

	return TRUE;
}

BOOL lgcUtilPlaceObject(TObject *obj, s32 x, s32 y)
{
	obj->pos.x = x << FP_BASE;
	obj->pos.y = y << FP_BASE;

	return lgcUtilShowObject(obj, TRUE);
}

FIXED lgcUtilPlaceOver(TObject *obj, s16 level)
{
	return TOFIXED(level - (resGetSpriteHeight(obj->sprite)+1) / 2 ); // rounded up
}

void lgcUtilGetTilePos(TObject *obj, TVector2D *tpos)
{
	tpos->x = (obj->pos.x >> FP_BASE) / TILE_W;
	tpos->y = (obj->pos.y >> FP_BASE) / TILE_H;
}

// Handles sprite changes depending on caveman type
BOOL lgcUtilCavemanChangeSprite(TObject *obj, TActionCaveman action)
{
	u32					baseSprite;

	switch(obj->type)
	{
		case OBJTYPE_CAVEMAN:
			baseSprite = SPRITE_CAVEMAN_WALK;
		break;

		case OBJTYPE_CAVEWOMAN:
			baseSprite = SPRITE_WOMAN_WALK;
		break;

		case OBJTYPE_CAVEOLDMAN:
			baseSprite = SPRITE_OLDMAN_WALK;
		break;
	}

	obj->sprite = resChangeSprite(obj->sprite, baseSprite + action);

	return (obj->sprite != NULL);
}

// See speech bubble callback mexCallbackSpeechBubble
BOOL lgcUtilCavemanSay(TObject *obj, u32 speechID)
{
	s32					x, y;

	/* The Caveman should stop to speak */

	lgcUtilClearTargets(obj);

	if(obj->flags & OBJFLAG_SWIM)
	{
		lgcUtilCavemanChangeSprite(obj, ACTION_CAVEMAN_FALL);
	}
	else
	{
		lgcUtilCavemanChangeSprite(obj, ACTION_CAVEMAN_WAIT);
	}


	x = (obj->pos.x >> FP_BASE) + resGetSpriteWidth(obj->sprite);
	y = (obj->pos.y >> FP_BASE) + 3 - resGetSpriteHeight(gSpeechBubble->sprite);

	// Set the data for callback

	gSpeechBubble->sprite->auxData = speechID;

	return lgcUtilPlaceObject(gSpeechBubble, x, y);
}

BOOL lgcUtilCavemanShowPayment(TObject *obj)
{
	s32				w,h;
	FIXED			x,y;

	w = ( resGetSpriteWidth(obj->sprite) + resGetSpriteWidth(gPaymentDisplay->sprite) ) / 2;
	h = ( resGetSpriteHeight(obj->sprite) + resGetSpriteHeight(gPaymentDisplay->sprite) ) / 2;

	gPaymentDisplay->pos.x = obj->pos.x;// + TOFIXED(w) + TOFIXED(GUI_GAMESTATE_PAYMENT_OFFSET_X);
	gPaymentDisplay->pos.y = obj->pos.y - TOFIXED(h) - TOFIXED(GUI_GAMESTATE_PAYMENT_OFFSET_Y);
	
	// Start the object's timer and movement
	lgcSetObjectState(gPaymentDisplay, 1);
	gPaymentDisplay->flags |= OBJFLAG_TARGET;
	gPaymentDisplay->vel.y = -GUI_GAMESTATE_PAYMENT_SPEED;

	lgcUtilShowObject(gPaymentDisplay, TRUE);

	return TRUE;
}

BOOL lgcUtilCavemanPayment(TObject *obj)
{
	s32				score, delta;

	// Base score
	switch(obj->type)
	{
		case OBJTYPE_CAVEMAN:
			score = SCORE_CAVEMAN;
		break;

		case OBJTYPE_CAVEWOMAN:
			score = SCORE_CAVEWOMAN;
		break;

		case OBJTYPE_CAVEOLDMAN:
			score = SCORE_CAVEOLDMAN;
		break;
	}

	delta = gTimestamp - obj->timestamp;

	// 1024 base should approx 1000 ms just fine, so operate like with FIXED falues
	score -= (SCORE_REDUCTION * delta) >> 10;

	// Minimum score for delivery
	if(score < SCORE_MIN)
		score = SCORE_MIN;

	dbgf("Time delta: %d, score: %d", delta, score);

	if( (gGameState.score / SCORE_BONUS_LIFE) != ((gGameState.score + score) / SCORE_BONUS_LIFE) )
	{
		dbg("Bonus life!");
		gGameState.livesCount++;
	}

	gGameState.score += score;

	gLastPayment = score;

	lgcUtilCavemanShowPayment(obj);

	return TRUE;
}


s16 lgcUtilFindObjectIndex(TObject *obj)
{
	TObject			*curObj = gObjectsList;
	s16				index = 0;

	if(obj != NULL)
	{
		while(curObj != obj)
		{
			curObj = curObj ->next;
			index++;
		}

		return index;
	}

	return -1;
}

TObject* lgcUtilGetObject(s16 index)
{
	TObject			*obj = gObjectsList;
	s32				i = 0;

	if(index == -1)
		return NULL;

	while( i < index  &&  obj != NULL )
	{
		obj = obj->next;
		i++;
	}

	return obj;
}

#include "logic/bird.sm.c"
#include "logic/dino.sm.c"
#include "logic/caveman.sm.c"
#include "logic/tree.sm.c"
#include "logic/stone.sm.c"
#include "logic/fruit.sm.c"
#include "logic/triopterus.sm.c"
#include "logic/cutscenes.sm.c"


BOOL lgcInit()
{
	lgcReset();
	return TRUE;
}

BOOL lgcReset()
{
	memset(gPlatformOccupied, 0, sizeof(gPlatformOccupied));

	return TRUE;
}

BOOL lgcInitState()
{
	memset(gPlatformOccupied, 0, sizeof(gPlatformOccupied));

	return TRUE;
}

BOOL lgcTerminate()
{
	return TRUE;
}

BOOL lgcRegisterEvent(TEventType type, void* data)
{
	TObject				*obj;
	TEvent				*ev;

	obj = gObjectsList;

	while(obj != NULL)
	{
		ev = lgcCheckEvent(obj, type, data);
		if(ev != NULL)
			lgcHandleEvent(obj, ev, data);

		obj = obj->next;
	}

	return TRUE;
}

/* 
    Returns the second participant of the collision
*/
TObject* lgcUtilGetCollider(TCollisionEvData *collision, TObject *obj)
{
	if(collision->obj1 == obj)
		return collision->obj2;

	if(collision->obj2 == obj)
		return collision->obj1;

	return NULL;
}

// If returned value is not NULL - event will be handled
TEvent* lgcCheckEvent(TObject *obj, TEventType type, void* data)
{
	TEvent				*ev;
	TEvent				*result;
	TCollisionEvData	*collision;
	TObject				*collider;
	TObject				*objData = (TObject*)data;
	s16					*evData;
	s32					evIndex = 0;
	s32					intData = (s32)data;
	

	ev = (TEvent*)obj->state;

	if(ev == NULL)
		return NULL;

	while( (ev->evID != EVENT_NULL) && (evIndex < STATE_EVENTS_MAX) )
	{
		if(ev->evID == type)
		{
			evData = &obj->stateData[evIndex];
			result = ev;
		
			switch(type)
			{
				case EVENT_TIMER:
					// Infinite timer is possible when a caveman reached the last stop in his route
					if(*evData < 0)
						result = NULL;
					else if(*evData >= intData)
					{
						*evData -= intData;
						result = NULL;
					}
					
					if(result != NULL)
					{
						// Event occured - restart the timer
						*evData = ev->evParam;
					}
				break;
		
				case EVENT_ANIMATION:
					if(intData != (u32)obj)
						result = NULL;
				break;
			
				case EVENT_ARRIVAL:
					if(intData != (u32)obj)
						result = NULL;
				break;
			
				case EVENT_LANDING:
					if((*evData != -1) && (lgcUtilGetObject(*evData) != objData))
						result = NULL;
					else if(obj->curPlatform != objData->curPlatform)
						result = NULL;
				break;
		
				case EVENT_TAKEOFF:
					if(obj->curPlatform != (s8)intData)
						result = NULL;
				break;
				
				case EVENT_COLLISION:
					// Check that current object is part of the collision
					// and that it is waiting for collision with specific object
		
					collision = (TCollisionEvData*)data;
					collider = lgcUtilGetCollider(collision, obj);
				
					if(collider == NULL)
						result = NULL;
					else if(*evData != collider->type)
						result = NULL;
				break;
		
				case EVENT_SWIM:
					if((*evData != -1) && (lgcUtilGetObject(*evData) != objData))
						result = NULL;
				break;

				case EVENT_GETON:
					// Ignore if we generated the event ourselves
					if(obj == objData)
						result = NULL;
				break;

				default:
				break;
			}

			if(result != NULL)
				return result;
		}

		evIndex++;
		ev++;
	}

	return NULL;
}

BOOL lgcHandleEvent(TObject *obj, TEvent *ev, void* data)
{
	EVRESULT_T				result = EVRESULT_PROCEED;
	s16						toState;

	if(ev->handler != NULL)
		result = ev->handler(obj, data);

	// Switch to the new state
	if(result != EVRESULT_CANCEL)
	{
		if(result == EVRESULT_PROCEED)
			toState = ev->toState;
		else // EVRESULT_TOSTATE
			toState = (s16)result;

		obj->state = obj->logic[toState];
		lgcInitObjectState(obj, obj->state);
	}

	return result;
}


/* 
	Initializes State's event fields
	Called on every state change
*/	 
BOOL lgcInitObjectState(TObject *obj, const TEvent *state)
{
	const TEvent		*ev;
	s32					evIndex = 0;

	ev = state;

	while( (ev->evID != EVENT_NULL) && (evIndex < STATE_EVENTS_MAX) )
	{
		switch(ev->evID)
		{
			case EVENT_TIMER:
				if(ev->evParam == STM_CURRENT_ROUTE_DELAY)
				{
					obj->stateData[evIndex] = obj->route->delay;
				}
				else if(ev->evParam == STM_CURRENT_OBJECT_DELAY)
				{
					obj->stateData[evIndex] = obj->aux->bird.interval;
				}
				else
				{
					obj->stateData[evIndex] = ev->evParam;
				}
			break;

			case EVENT_LANDING:
			case EVENT_SWIM:
				if(ev->evParam == STM_CURRENT_OBJECT)
					obj->stateData[evIndex] = lgcUtilFindObjectIndex(obj);
				else if(ev->evParam == STM_CHOPPER_OBJECT)
					obj->stateData[evIndex] = lgcUtilFindObjectIndex(gPlayer);
			break;

			default:
				obj->stateData[evIndex] = ev->evParam;
			break;
		}

		ev++;
		evIndex++;
	}

	return TRUE;
}

BOOL lgcSetObjectState(TObject *obj, s32 state)
{
	obj->state = obj->logic[state];
	
	return lgcInitObjectState(obj, obj->state);
}

BOOL lgcUpdateObject(TObject *obj)
{

	return TRUE;
}

struct
{
	u8					type;
	const TEvent		(*logic)[STATE_EVENTS_MAX];
}
lgcLogicTables[] = 
{
	{ OBJTYPE_CAVEMAN,						lgcCaveman },
	{ OBJTYPE_CAVEWOMAN,					lgcCaveman },
	{ OBJTYPE_CAVEOLDMAN,					lgcCaveman },
	{ OBJTYPE_STONE,						lgcStone },
	{ OBJTYPE_TREE,							lgcTree },
	{ OBJTYPE_FRUIT,						lgcFruit },
	{ OBJTYPE_BIRD,							lgcBird },
	{ OBJTYPE_DINO,							lgcDino },
	{ OBJTYPE_WIND,							lgcWind },
	{ OBJTYPE_TRIOPTERUS,					lgcTriopterus },

	{ OBJTYPE_CUTSCENE_CAVEMAN_CARRY,		lgcCutsceneCavemanCarry },
	{ OBJTYPE_CUTSCENE_LOGO,				lgcCutsceneLogo },
	{ OBJTYPE_GUI_PAYMENT,					lgcGuiPayment },

	{ OBJTYPE_NULL }
};



BOOL lgcInitObject(TObject *obj, s8 state)
{
	s32			i = 0;

	// Find the logic table
	while( (lgcLogicTables[i].type != obj->type) &&
		   (lgcLogicTables[i].type != OBJTYPE_NULL) ) i++;

	// Not found
	if(lgcLogicTables[i].type == OBJTYPE_NULL)
	{
		obj->logic = NULL;
		obj->state = NULL;

		return FALSE;
	}

	obj->logic = lgcLogicTables[i].logic;
	obj->state = lgcLogicTables[i].logic[state];

	if(state == 0)
	{
		lgcInitObjectState(obj, obj->state);
	}

	return TRUE;
}

