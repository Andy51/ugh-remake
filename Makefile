
ifndef CFG
CFG = Debug
endif

OUTDIR = $(CFG)

WINFLAGS = -mno-cygwin -g
CFLAGS = -I../SDK -DEP2 $(WINFLAGS)
LDFLAGS = -mno-cygwin -g -L"../EmuElf/$(OUTDIR)/" -lEmuElf -lgdi32 -lmsimg32
SOURCES = app.c main.c resources.c gfx.c bmp.c map.c mechanix.c maths.c gui.c logic.c states.c menu.c
BITMAPDIR = bitmaps
BITMAPFILES = $(wildcard $(BITMAPDIR)/*.bmp)
IMAGEDIR = FS/b/res
IMAGEFILES = $(BITMAPFILES:$(BITMAPDIR)/%.bmp=$(IMAGEDIR)/%.img)
OBJFILES = $(SOURCES:%.c=$(OUTDIR)/%.o)
LOGICDIR = logic
LOGICFILES = $(wildcard $(LOGICDIR)/*.sm.c)

OUTPUT = $(OUTDIR)/Ugh.exe




CC = gcc-3
LD = gcc-3
MKDIR = mkdir


all: $(OUTPUT)

$(OUTPUT): $(OBJFILES) $(OUTDIR) $(IMAGEFILES)
	$(LD) $(OBJFILES) $(LDFLAGS) -o "$(OUTPUT)"

$(OUTDIR)/logic.o: logic.c $(LOGICFILES)
	$(CC) $(CFLAGS) -c -o $@ $<

$(OUTDIR)/%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<

$(IMAGEDIR)/%.img: $(BITMAPDIR)/%.bmp
	./bmp2img $<
	mv $(addsuffix .img, $(basename $<)) $@
	
$(OUTDIR):
	$(MKDIR) "$(OUTDIR)"
	
cleancode:
	rm $(OUTDIR)/*.o
	
clean:
	rm $(OUTDIR)/*.o
	rm $(IMAGEDIR)/*.img
	