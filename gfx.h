#ifndef GFX__H
#define GFX__H

#include <loader.h>
#include <ATI.h>
#include <DAL.h>
#include "common.h"
#include "dbg.h"

#define SCREEN_W			220
#define SCREEN_H			176


extern AHIDEVCONTEXT_T		gCtx;

extern AHISURFACE_T			gDrawSurf;
extern AHISURFACE_T			gDispSurf;

extern AHIRECT_T			gScreenRect;
extern u32					gCacheSize;


#define GFX_MEMORY_SMALLINTMEM	((((SCREEN_W+7)/8)*8)*SCREEN_H*2)

#define GFX_CFG_NONE						0
#define GFX_CFG_HAS_LANDSCAPE_SURFS			1
#define GFX_CFG_SURFS_REALLOC				2
#define GFX_CFG_FREE_CACHE					4

typedef enum
{
	/*
		Unknown memory configuration fails the gfxInit
	*/
	GFX_MEMORY_CFG_UNKNOWN = 0,

	/* 
	    On old type (E398, L7, V360), there is no extmem, all surfaces are
	    allocated in intmem and DAL cache can be deallocated safely.
	 
	    For this type we free DAL cache, all the surfaces, and reallocate them
	    in intmem.
	*/
	GFX_MEMORY_CFG_OLD_TYPE,

	/* 
	    On new type (L7e, Z3), there is a big chunk of extmem, disp surf is
	    preallocated in landscape size in intmem, draw surf and DAL cache -
	    in the extmem. DAL holds already reused draw/disp surfs with portrait
	    size from landscape sized ones as current draw/disp.
	    A lot of intmem freespace as a result. DAL cache should not be freed
	    (using DAL_FreeCache at least) as it contain necessary information for
		the skins engine.
	 
	    For this type we use the DAL's already preallocated for landscape mode
	    disp surf and reallocate it into intmem and allocate additional draw
	    surf into intmem.
	*/
	GFX_MEMORY_CFG_NEW_TYPE

} TGfxMemoryCfg;



BOOL gfxInit();
BOOL gfxTerminate();
void gfxFlushDoublebuffer();

BOOL gfxDrawSprite(TSprite *spr, TVector2D pos);
BOOL gfxOutputVideomemInfo();

u32 gfxDetectMemoryCfg();

#endif

