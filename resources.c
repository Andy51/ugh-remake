
#include "resources.h"

FILE_HANDLE_T			gLevelsData = FILE_HANDLE_INVALID;
u8						*gCurrentLevelMap;
AHIBITMAP_T				gTileArray;
AHIPOINT_T				*gTiles;

AHIBITMAP_T				gSpriteBitmaps[MAX_SPRITES];
u32						gSpriteBitmapsCount;


WCHAR					gHomeDir[64];
WCHAR					sPathBuf[64];

PHONE_PLATFORM_T		gPhonePlatform;

TSpritesFileHeader		*sSpritesHeader;
TSpriteProto			*gSpritePrototypes;
u32						gSpriteProtoCount;

TFont					gFonts[FONTS_COUNT];			// Should be freed

TLevel					*gLevels;
TLevel					*gCurrentLevel;
TPlatform				*gPlatforms;
TPlatform				*gLevelPlatforms;
TRouteStop				*gRoutes;
TGameObject				*gPassengers;
char 					*gStrings;




BOOL resInit()
{

	return TRUE;
}

BOOL resTerminate()
{
	u32					i;

	if(gCurrentLevelMap != NULL)
		suFreeMem(gCurrentLevelMap);

	if(gTiles != NULL)
		suFreeMem(gTiles);

	if(gTileArray.image != NULL)
		suFreeMem(gTileArray.image);

	if(gSpritePrototypes != NULL)
	{
		resCleanSpriteProtos();

		suFreeMem(gSpritePrototypes);
	}

	if(sSpritesHeader != NULL)
		suFreeMem(sSpritesHeader);
	
	for(i = 0; i < FONTS_COUNT; i++)
	{
		// We do not store directly the allocated data pointer
		if(gFonts[i].map != NULL)
			suFreeMem(gFonts[i].map - sizeof(TFontHeader));
	}

	if(gLevelsData != FILE_HANDLE_INVALID)
		DL_FsCloseFile(gLevelsData);

	return TRUE;
}


void resInitHomeDir(WCHAR *elfPath)
{
	WCHAR			*last;

	u_strcpy(gHomeDir, elfPath);

	last = u_strrchr(gHomeDir, L'/');

	last++;

	u_strcpy(last, RESOURCES_DIR);
}

WCHAR *resGetFullPathExt(char *filename, const char *extension)
{
	u32				len;

	u_strcpy(sPathBuf, gHomeDir);

	len = u_strlen(sPathBuf);

	u_atou(filename, &sPathBuf[len]);


	// Append extension

	len = u_strlen(sPathBuf);

	sPathBuf[len] = L'.';

	u_atou(extension, &sPathBuf[len+1]);

	return sPathBuf;
}

WCHAR *resGetFullPath(char *filename)
{
	u32				len;

	u_strcpy(sPathBuf, gHomeDir);

	len = u_strlen(sPathBuf);

	u_atou(filename, &sPathBuf[len]);

	return sPathBuf;
}

void* resLoadFile(WCHAR *path)
{
	FILE_HANDLE_T			f;
	void					*ptr;
	u32						result;
	u32						sz;


	f = DL_FsOpenFile( path, FILE_READ_MODE, 0);
	
	if(f == FILE_HANDLE_INVALID)
		return NULL;

	sz = DL_FsGetFileSize(f);

	if(sz == 0)
		return NULL;

	ptr = (void*)suAllocMem( sz, &result );

	if(ptr == NULL)
		return NULL;

	DL_FsReadFile( ptr, sz, 1, f, &result );

	DL_FsCloseFile(f);

	return ptr;
}

BOOL resLoadImage(WCHAR *path, AHIBITMAP_T *bitmap)
{
	FILE_HANDLE_T			f;
	TImageHeader			header;
	void					*data;
	u32						result;
	u32						sz;
	#ifdef WIN32
		u16					*ptr;
		u32					i;
	#endif


	f = DL_FsOpenFile( path, FILE_READ_MODE, 0);
	
	if(f == FILE_HANDLE_INVALID)
		return FALSE;


	DL_FsReadFile( &header, sizeof(TImageHeader), 1, f, &result );

	CONV16(header.width);
	CONV16(header.height);

	sz = header.width * header.height * 2;

	if(sz == 0)
		return FALSE;

	data = (void*)suAllocMem( sz, &result );

	if(data == NULL)
		return FALSE;

	DL_FsReadFile( data, sz, 1, f, &result );

	DL_FsCloseFile(f);

	bitmap->format = AHIFMT_16BPP_565;
	bitmap->width = header.width;
	bitmap->height = header.height;
	bitmap->stride = header.width * 2;
	bitmap->image = data;

	dbgf("Image loaded: %dx%d, %d B", bitmap->width, bitmap->height, sz);

	#ifdef WIN32
		ptr = (u16*)data;

		for(i = 0; i < sz/2; i++, ptr++)
			CONV16(*ptr);

	#endif

	return TRUE;
}


BOOL resLoadLevelData(u32 levelIdx)
{
	u32					read;
	DL_FsFSeekFile(gLevelsData, levelIdx * LEVEL_SIZE, SEEK_WHENCE_SET);
	DL_FsReadFile(gCurrentLevelMap, LEVEL_SIZE, 1, gLevelsData, &read);


	gCurrentLevel = &gLevels[levelIdx];
	gLevelPlatforms = &gPlatforms[gCurrentLevel->platformsTblOff];

	return TRUE;
}

#ifdef WIN32

void convertLevelsHeader(TLevelsHeader *dst)
{
	CONV16(dst->magic);
	CONV16(dst->version);
	CONV32(dst->levelsCount);
	CONV32(dst->platformsOff);
	CONV32(dst->platformsCount);
	CONV32(dst->passengersOff);
	CONV32(dst->passengersCount);
	CONV32(dst->routesOff);
	CONV32(dst->routesCount);
	CONV32(dst->stringsOff);
	CONV32(dst->stringsSize);
}

void convertLevels(TLevel *dst, int count)
{
	int					i;

	for(i=0; i<count; i++)
	{
		CONV16(dst[i].firstPlayerX);
		CONV16(dst[i].firstPlayerY);
		CONV16(dst[i].nameTblOff);
		CONV16(dst[i].objectsTblOff);
		CONV16(dst[i].passengersTblOff);
		CONV16(dst[i].platformsTblOff);
		CONV16(dst[i].waterLevel);
		CONV16(dst[i].waterSpeed);
		CONV16(dst[i].routesToComplete);
	}
}

void convertPlatforms(TPlatform *dst, int count)
{
	int					i;

	for(i=0; i<count; i++)
	{
		CONV16(dst[i].left);
		CONV16(dst[i].cave);
		CONV16(dst[i].level);
		CONV16(dst[i].number);
		CONV16(dst[i].object);
		CONV16(dst[i].right);
		CONV16(dst[i].stop);
	}
}

void convertRoutes(TRouteStop *dst, int count)
{
	int					i;

	for(i=0; i<count; i++)
	{
		CONV16(dst[i].delay);
		CONV16(dst[i].platform);
	}
}

void convertPassengers(TGameObject *dst, int count)
{
	int					i;

	for(i=0; i<count; i++)
	{
		CONV16(dst[i].ID);
		if(dst[i].ID != OBJTYPE_DINO)
		{
			CONV16(dst[i].platform);
			CONV16(dst[i].routeOffset);
		}
	}
}
#endif

BOOL resLoadMap(char *filename)
{
	s32					result;
	TLevelsHeader		*lvlHdr;
	u8					*dataStart;

	if(gLevelsData != FILE_HANDLE_INVALID)
		DL_FsCloseFile(gLevelsData);

	gCurrentLevelMap = (u8*)suAllocMem(LEVEL_SIZE, &result);

	gLevelsData = DL_FsOpenFile(resGetFullPathExt(filename, "dat"), FILE_READ_MODE, 0);

	if(gLevelsData == FILE_HANDLE_INVALID)
		return FALSE;

	lvlHdr = (TLevelsHeader*)resLoadFile(resGetFullPathExt(filename, "lvl"));

	dataStart = (u8*)lvlHdr;

	#ifdef WIN32
		convertLevelsHeader(lvlHdr);
	#endif

	gLevels = (TLevel*)(lvlHdr+1);
	gPlatforms = (TPlatform*)(lvlHdr->platformsOff + dataStart);
	gRoutes = (TRouteStop*)(lvlHdr->routesOff + dataStart);
	gPassengers = (TGameObject*)(lvlHdr->passengersOff + dataStart);
	gStrings = (char*)(lvlHdr->stringsOff + dataStart);

	#ifdef WIN32
		convertLevels(gLevels, lvlHdr->levelsCount);
		convertPlatforms(gPlatforms, lvlHdr->platformsCount);
		convertRoutes(gRoutes, lvlHdr->routesCount);
		convertPassengers(gPassengers, lvlHdr->passengersCount);
	#endif

	return TRUE;
}


BOOL resLoadTiles(char *filename)
{
	u32					status;
	u32					i;

	/*
		Load the all-tiles bitmap
	*/

	status = resLoadImage( resGetFullPath(filename), &gTileArray );

	if(status != TRUE)
		return FALSE;

	/*
		Cut the tiles
	*/

	gTiles = (AHIPOINT_T*)suAllocMem(sizeof(AHIPOINT_T) * TILES_COUNT, &status);

	if(gTiles == NULL)
		return FALSE;

	for(i=0; i<TILES_COUNT; i++)
	{

		gTiles[i].x = TILE_W * i;
		gTiles[i].y = 0;
		/*gTiles[i].width = TILE_W;
		gTiles[i].height = TILE_H;
		gTiles[i].image = ((u8*)gTileArray.image) + TILE_W * i;
		gTiles[i].stride = gTileArray.stride;
		gTiles[i].format = AHIFMT_16BPP_565;*/
	}

	return TRUE;
}


static u32 getMaxSpriteWidth(u8 *frames, u32 count)
{
	u32					i;
	u32					max, w;

	max = 0;

	for(i=0; i<count; i++)
	{
		w = frames[i+1] - frames[i];

		if(w > max)
			max = w;
	}

	return max;
}


// Loads sprite descriptions only
BOOL resLoadSprites(char *filename)
{
	TSpriteProto			*proto;
	TSpriteInfo				*info;
	u8						*data;
	u8						*frames;
	char					*strTbl;
	s32						err;
	s32						i;

	data = (u8*)resLoadFile( resGetFullPath(filename) );

	sSpritesHeader = (TSpritesFileHeader*)data;

	gSpriteProtoCount = E32(sSpritesHeader->spritesCount);

	gSpritePrototypes = (TSpriteProto*)suAllocMem(gSpriteProtoCount*sizeof(TSpriteProto), &err);

	strTbl = (char*)(data + E32(sSpritesHeader->strTblOffset));
	frames = (u8*)(data + E32(sSpritesHeader->framesTblOffset));
	info = (TSpriteInfo*)(data + sizeof(TSpritesFileHeader));

	for(i=0; i<gSpriteProtoCount; i++)
	{
		proto = &gSpritePrototypes[i];
		proto->framesCount = info[i].framesCount;
		proto->isLoaded = FALSE;
		proto->frames = &frames[E32(info[i].frames)];
		proto->filename = &strTbl[E32(info[i].filename)];
		proto->patchSize.x = getMaxSpriteWidth(proto->frames, proto->framesCount);
		proto->frameInterval = E16(info[i].frameInterval);
	}

	return TRUE;
}

BOOL resFreeSpriteProto(TSpriteProto	*proto)
{
	if(proto->isLoaded)
	{
		suFreeMem(proto->bitmap.image);
		proto->isLoaded = FALSE;
	}

	return TRUE;
}

BOOL resCleanSpriteProtos()
{
	TSpriteProto	*proto;
	s32				i;

	proto = gSpritePrototypes;

	for(i = 0; i < gSpriteProtoCount; i++, proto++)
	{
		resFreeSpriteProto(proto);
	}

	return TRUE;
}


TSprite* resCreateSprite(u32 ID, BOOL patched)
{
	TSprite				*spr;
	TSpriteProto		*proto;
	AHIPOINT_T			patchSize;
	u32					err, status = TRUE;
	u32					i;

	// Find the proto for the sprite

	if(ID >= gSpriteProtoCount)
		return NULL;
	
	proto = &gSpritePrototypes[ID];


	// Create a new sprite

	spr = (TSprite*)suAllocMem(sizeof(TSprite), &err);

	spr->ID = ID;

	spr->proto = proto;

	spr->curFrame = 0;

	spr->auxData = 0;
	spr->drawCallback = NULL;

	// Load resources, if needed

	if(proto->isLoaded == FALSE)
	{
		status = resLoadImage( resGetFullPath(proto->filename), &proto->bitmap );

		proto->patchSize.y = proto->bitmap.height;
		proto->isLoaded = TRUE;
	}

	if(status != TRUE)
	{
		suFreeMem(spr);
		return NULL;
	}

	
	patchSize.x = proto->patchSize.x*2;
	patchSize.y = proto->patchSize.y;
	
	if(patched)
	{
		status = AhiSurfAlloc(gCtx, &spr->patch, &patchSize, AHIFMT_16BPP_565, 0);
		spr->flags = SPRITE_FLAG_NORMAL;
	}
	else
	{
		spr->patch = NULL;
		spr->flags = SPRITE_FLAG_NOPATCH;
	}
	
	spr->direction = 0;

	spr->prevPos.x = spr->prevPos.y = 255;

	//spr->frameInterval = proto->frameInterval;
	spr->frameTimer = spr->proto->frameInterval;

	spr->prevSprite = NULL;

	gfxUpdateSpriteFrame(spr);

	gfxOutputVideomemInfo();

	return spr;
}


TSprite* resCreateEmptySprite(s32 w, s32 h)
{
	TSprite				*spr;
	AHIPOINT_T			patchSize;
	u32					err, status = RESULT_OK;
	u32					i;

	// Create a new sprite with frames array attached

	spr = (TSprite*)suAllocMem(sizeof(TSprite) + 2*sizeof(u8), &err);

	spr->ID = SPRITE_NULL;
	spr->flags = SPRITE_FLAG_NEWPROTO | SPRITE_FLAG_NOBITMAP;

	spr->curFrame = 0;

	spr->auxData = 0;
	spr->drawCallback = NULL;

	patchSize.x = w;
	patchSize.y = h;
	
	status = AhiSurfAlloc(gCtx, &spr->patch, &patchSize, AHIFMT_16BPP_565, 0);

	spr->proto = (TSpriteProto*)suAllocMem(sizeof(TSpriteProto), NULL);

	spr->proto->patchSize.x = w;
	spr->proto->patchSize.y = h;
	spr->proto->framesCount = 0;
	spr->proto->frames = NULL;
	spr->proto->isLoaded = FALSE;
	
	spr->direction = 0;

	spr->prevPos.x = spr->prevPos.y = 255;

	spr->frameTimer = 0;

	spr->prevSprite = NULL;

	gfxOutputVideomemInfo();

	return spr;
}

TSprite* resChangeSprite(TSprite *prevSprite, u32 newID)
{
	TSprite				*spr;

	if((prevSprite != NULL) && (newID == prevSprite->ID))
	{
		return prevSprite;
	}

	spr = resCreateSprite(newID, TRUE);

	/*
		If we are changing the sprite second time in a frame, prevSprite will not be used.
		Otherwise we will have sprite doubling bug.
	*/

	if((prevSprite != NULL) && (prevSprite->prevSprite != NULL))
	{
		spr->prevSprite = prevSprite->prevSprite;
		resFreeSprite(prevSprite);
	}
	else
	{
		spr->prevSprite = prevSprite;
	}

	if(spr->prevSprite != NULL)
	{
		spr->direction = spr->prevSprite->direction;
	}

	gfxOutputVideomemInfo();
	
	return spr;
}

BOOL resFreeSprite(TSprite	*spr)
{
	if(spr->patch != NULL)
		AhiSurfFree(gCtx, spr->patch);

	if(spr->flags & SPRITE_FLAG_NEWPROTO)
	{
		if(spr->proto->isLoaded)
			suFreeMem(spr->proto->bitmap.image);

		suFreeMem(spr->proto);
	}

	//suFreeMem(spr->bitmap->image);
	suFreeMem(spr);

	return TRUE;
}

s32 resGetSpriteWidth(TSprite *spr)
{
	if(spr->proto->framesCount != 0)
		return (spr->proto->frames[spr->curFrame+1] - spr->proto->frames[spr->curFrame]);
	else
		return spr->proto->patchSize.x;
}

s32 resGetSpriteHeight(TSprite *spr)
{
	return spr->proto->patchSize.y;
}

BOOL resLoadFont(char *filename, u8 fontID)
{
	TFontHeader			*hdr;
	u8					*data;

	if(fontID >= FONTS_COUNT)
		return FALSE;

	data = (u8*)resLoadFile( resGetFullPath(filename) );

	if(data == NULL)
		return FALSE;

	hdr = (TFontHeader*)data;

	gFonts[fontID].w = hdr->w;
	gFonts[fontID].h = hdr->h;
	gFonts[fontID].stride = hdr->stride;
	gFonts[fontID].map = (u8*)++hdr;
	gFonts[fontID].bits = gFonts[fontID].map + 256;

	return TRUE;
}
