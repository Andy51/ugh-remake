
#include "resources.h"
#include "mechanix.h"
#include "menu.h"
#include "gui.h"

#define STATE_PARAM_LVLCOMPLETE		0
#define STATE_PARAM_TRYAGAIN		1
#define STATE_PARAM_LOSE			2
#define STATE_PARAM_WIN				3

typedef enum
{
	STATE_NULL = 0,
	STATE_INIT,
	STATE_MENU,
	STATE_LOADING,
	STATE_GAME,
	STATE_RESULT,
	STATE_CONFIRM,

	STATE_MAX
} TStateEnum;

typedef BOOL (*TInitParamFn)(u32);
typedef void (*TStateKeysHandler)(u32, u32, u32);
typedef STATE_RESULT_T (*TStateHandler)(void);
typedef TInitFn				TTerminateFn;

typedef struct
{
	TStateHandler		update;
	TStateKeysHandler	keyHandler;
	TInitParamFn		initializer;
	TTerminateFn		terminator;

} TStateEntry;

BOOL stsInit();
BOOL stsTerminate();
BOOL stsUpdate();
void stsHandleKeys(u32 keysPressed, u32 keysReleased, u32 keysHold);
BOOL stsChangeState(TStateEnum newState, u32 data);
