
#include "menu.h"

static u32			sKeysPressed;
static s32			sMenuItemIndex, sMenuResultIndex;
static BOOL			sMenuSaveAvailable;

static TGameObject	sCutsceneParamsCaveman, sCutsceneParamsLogo, sCutsceneParamsCollider;

extern u32			gHighscore;
extern s32			gHighLevel;

typedef struct
{
	const char		*text;
	BOOL			enabled;

} TMenuItem;

static TMenuItem sMenuItems[] = 
{
	{ "New Game", 	TRUE  },
	{ "Continue", 	FALSE },
	{ "Exit",		TRUE  },
};

#define MENU_NUM_ITEMS (sizeof(sMenuItems) / sizeof(TMenuItem))

#define MENU_BACKGROUND_COLOR		ATI_565RGB(0x00,0x00,0x00)
#define MENU_HIGHLIGHT_COLOR		ATI_565RGB(0xFF,0xFF,0xBF)
#define MENU_ITEM_COLOR				ATI_565RGB(0xD5,0xAA,0x6A)
#define MENU_DISABLED_COLOR			ATI_565RGB(0x90,0x90,0x90)

#define MENU_ITEMS_START_LINE		12


BOOL menuDrawMenu()
{
	int				i;


	guiDrawScreenBackground(MENU_BACKGROUND_COLOR);

	guiFontSet(1);

	for(i=0; i < MENU_NUM_ITEMS; i++)
	{
		// Highlited item
		if(i == sMenuItemIndex)
		{
			guiFontSetColor(MENU_HIGHLIGHT_COLOR);
		}
		else if(sMenuItems[i].enabled == FALSE)
		{
			guiFontSetColor(MENU_DISABLED_COLOR);
		}
		else
		{
			guiFontSetColor(MENU_ITEM_COLOR);
		}

		guiPrintfCenterScreen(MENU_ITEMS_START_LINE + i*2, sMenuItems[i].text);
	}

	guiFontSet(0);
	guiFontSetColor(ATI_565RGB(0xFF, 0xFF, 0xFF));
	guiPrintfCenterScreen(27, "Highscore: %06d at L%02d", gHighscore, gHighLevel + 1);
	guiPrintfCenterScreen(28, "Andy51 (c) 2011 motofan.ru");	

	return TRUE;
}


BOOL menuInitState(u32 init)
{
	TObject				*obj, *logo;
	FIXED				level;

	lgcInitState();
	
	sCutsceneParamsCaveman.csCavemanCarry.tx = LEVEL_W + 6;
	sCutsceneParamsCaveman.csCavemanCarry.ty = 7;
	sCutsceneParamsCaveman.csCavemanCarry.target =  LEVEL_W / 2 + 8;

	obj = mexCreateObject(OBJTYPE_CUTSCENE_CAVEMAN_CARRY, &sCutsceneParamsCaveman);

	sCutsceneParamsLogo = sCutsceneParamsCaveman;
//	sCutsceneParamsLogo.csCavemanCarry.ty = 3;

	logo = mexCreateObject(OBJTYPE_CUTSCENE_LOGO, &sCutsceneParamsLogo);

	/* Finetune the logo position relative to the caveman */
	logo->pos.y = obj->pos.y - (((resGetSpriteHeight(obj->sprite) + resGetSpriteHeight(logo->sprite)) / 2) << FP_BASE);

	sCutsceneParamsCollider.collider.tw = 16;
	sCutsceneParamsCollider.collider.th = 1;
	sCutsceneParamsCollider.collider.tx = LEVEL_W / 2;
	sCutsceneParamsCollider.collider.ty = 7;

	obj = mexCreateObject(OBJTYPE_COLLIDER, &sCutsceneParamsCollider);

	sMenuItemIndex = 0;
	sMenuResultIndex = -1;


	// Check for savegame existence
	if(mexCheckSaveGame() == TRUE)
		sMenuItems[1].enabled = TRUE;
	else
		sMenuItems[1].enabled = FALSE;

	
	return TRUE;
}

BOOL menuTerminateState()
{
	mexClearObjects();
	resCleanSpriteProtos();

	return TRUE;
}

u32 menuUpdate()
{

	menuDrawMenu();

	menuUpdateObjects();

	return sMenuResultIndex;
}


#ifdef LANDSCAPE_RIGHT

#define MENU_MAP_LEFT		(MULTIKEY_UP		| MULTIKEY_2)
#define MENU_MAP_RIGHT		(MULTIKEY_DOWN		| MULTIKEY_8)
#define MENU_MAP_UP			(MULTIKEY_RIGHT 	| MULTIKEY_6)
#define MENU_MAP_DOWN		(MULTIKEY_LEFT		| MULTIKEY_4)

#define MENU_MAP_EXIT		(MULTIKEY_STAR)
#define MENU_MAP_OK			(MULTIKEY_JOY_OK	| MULTIKEY_5)

#else

#define MENU_MAP_LEFT		(MULTIKEY_DOWN		| MULTIKEY_8)
#define MENU_MAP_RIGHT		(MULTIKEY_UP		| MULTIKEY_2)
#define MENU_MAP_UP			(MULTIKEY_LEFT		| MULTIKEY_4)
#define MENU_MAP_DOWN		(MULTIKEY_RIGHT 	| MULTIKEY_6)

#define MENU_MAP_EXIT		(MULTIKEY_STAR)
#define MENU_MAP_OK			(MULTIKEY_JOY_OK	| MULTIKEY_5)

#endif

void menuHandleKeys(u32 keysPressed, u32 keysReleased, u32 keysHold)
{
	if(keysPressed & MENU_MAP_DOWN)
	{
		do
		{
			sMenuItemIndex++;

			if(sMenuItemIndex >= MENU_NUM_ITEMS)
				sMenuItemIndex = 0;

		} while(sMenuItems[sMenuItemIndex].enabled == FALSE);
	}

	if(keysPressed & MENU_MAP_OK)
	{
		sMenuResultIndex = sMenuItemIndex;
	}

	if(keysPressed & MENU_MAP_UP)
	{
		do
		{
			sMenuItemIndex--;

			if(sMenuItemIndex < 0)
				sMenuItemIndex = MENU_NUM_ITEMS - 1;

		} while(sMenuItems[sMenuItemIndex].enabled == FALSE);
	}

	// TODO: Remove this crap! 
/*	if(keysReleased & MENU_MAP_EXIT)
	{
		dbgf("MENU: * detected, state: 0x%X 0x%X 0x%X", keysPressed, keysReleased, keysHold);
		gGlobalState = GLOBAL_STATE_EXIT;
	}*/
}



BOOL menuUpdateObject(TObject *obj)
{
	TSprite				*sprite;

	sprite = obj->sprite;

	// Animation
	if(sprite->frameTimer != ANIMATION_TIMER_STOP)
	{
		sprite->frameTimer -= gTick;
		if(sprite->frameTimer <= 0)
		{
			sprite->curFrame++;
			if(sprite->curFrame >= sprite->proto->framesCount)
			{
				sprite->curFrame = 0;
				lgcRegisterEvent(EVENT_ANIMATION, (void*)obj);
			}

			sprite->frameTimer += sprite->proto->frameInterval;

			gfxUpdateSpriteFrame(sprite);
		}
	}

	// Physical movement
	if(obj->flags & OBJFLAG_PHYSIC)
	{
		mexPhysicalMovement(obj);
	}

	// Increasing water
/*	if( ((obj->flags & OBJFLAG_SWIM) == 0) && 
		((obj->flags & OBJFLAG_PHYSIC) == 0) && 
		(gWaterLevel < obj->pos.y) && obj->type != OBJTYPE_GUI_HEALTHBAR )
	{
		lgcRegisterEvent(EVENT_SWIM, (void*)obj);
	}
*/
	// Moving to target
	if(obj->flags & (OBJFLAG_TARGET | OBJFLAG_TARGET_OBJECT))
	{
		FIXED			target;

		obj->pos.x += fpmul(obj->vel.x, gTick);
		obj->pos.y += fpmul(obj->vel.y, gTick);

		if(obj->flags & OBJFLAG_TARGET)
			target = obj->target.pos;
		else // OBJFLAG_TARGET_OBJECT
			target = lgcUtilGetObject(obj->target.obj)->pos.x;

		if(obj->vel.x < 0)
		{
			if(obj->pos.x <= target)
			{
				obj->vel.x = 0;
				lgcRegisterEvent(EVENT_ARRIVAL, (void*)obj);
			}
		}
		else
		{
			if(obj->pos.x >= target)
			{
				obj->vel.x = 0;
				lgcRegisterEvent(EVENT_ARRIVAL, (void*)obj);
			}
		}
	}

	return TRUE;
}


/*
	Stripped down version of mexUpdateObjects
*/
static BOOL menuUpdateObjects()
{
	TObject				*obj;

	mexCalcObjectCollisions();

	lgcRegisterEvent(EVENT_TIMER, (void*)gTick);

	obj = gObjectsList;

	while( obj != NULL )
	{
		if((obj->flags & OBJFLAG_HIDDEN) == 0)
		{
			menuUpdateObject(obj);
		}
		obj = obj->next;
	}
	
	obj = gObjectsList;

	while( obj != NULL )
	{
		if((obj->flags & OBJFLAG_HIDDEN) == 0)
		{
			gfxDrawSpritePatch(obj->sprite, obj->pos);	
		}

		if((obj->flags & OBJFLAG_HIDING) != 0)
		{
			gfxDrawSpritePatch(obj->sprite, obj->pos);

			obj->flags &= ~OBJFLAG_HIDING;
			obj->flags |= OBJFLAG_HIDDEN;
		}

		obj = obj->next;
	}

	obj = gObjectsList;

	while( obj != NULL )
	{
		if((obj->flags & OBJFLAG_HIDDEN) == 0)
		{
			gfxUpdateSpritePatch(obj->sprite, obj->pos);	
		}

		obj = obj->next;
	}

	obj = gObjectsList;

	while( obj != NULL )
	{
		if((obj->flags & OBJFLAG_HIDDEN) == 0)
		{
			gfxDrawSprite(obj->sprite, obj->pos);
		}

		obj = obj->next;
	}

	// If there was any objects marked for deletion deleted - remove them now
	mexCleanDeletedObjects();

	return TRUE;
}
