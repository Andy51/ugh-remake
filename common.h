#ifndef COMMON__H
#define COMMON__H

#include <typedefs.h>
#include <ATI.h>

//#define DEBUG_SINGLE_FRAME
#define LANDSCAPE_RIGHT


typedef unsigned char		u8;
typedef signed char			s8;
typedef unsigned short		u16;
typedef signed short		s16;
typedef unsigned long		u32;
typedef signed long			s32;



#define	FP_BASE				10
#define	FP_ONE				(1<<FP_BASE)

typedef s32					FIXED;

#define TOFIXED(_fl_)		((FIXED)((_fl_)*FP_ONE))



#define	GLOBAL_STATE_EXIT		0
#define GLOBAL_STATE_INIT		1
#define GLOBAL_STATE_MAIN		2
#define GLOBAL_STATE_SUSPEND	3

extern u32					gGlobalState;

typedef BOOL (*TInitFn)(void);

extern u32					gFPS;
extern FIXED				gTick;

typedef struct
{
	u32			filename;
	u32			frames;
	/*char		*filename;
	u8			*frames;*/
	u16			frameInterval;
	u8			framesCount;

} TSpriteInfo;

typedef struct
{
	u32			spritesCount;
	u32			framesTblOffset;
	u32			framesTblSize;
	u32			strTblOffset;
	u32			strTblSize;

} TSpritesFileHeader;

#define SPRITE_FLAG_NORMAL			0
#define SPRITE_FLAG_NOBITMAP		1
#define SPRITE_FLAG_NOPATCH			2
#define SPRITE_FLAG_NEWPROTO		4

typedef struct
{
	u8					framesCount;
	BOOL				isLoaded;
	u8					flags;

	AHIBITMAP_T			bitmap;
	u8					*frames;
	char				*filename;

	AHIPOINT_T			patchSize;
	u32					frameInterval;

} TSpriteProto;

struct TSprite_tag;
typedef BOOL (*TSpriteCallback)(struct TSprite_tag*, s32, s32);

typedef struct TSprite_tag
{
	u8					ID;
	u8					flags;
//	u8					framesCount;
	u8					curFrame;
	u8					direction;

//	AHIBITMAP_T			*bitmap;
//	u8					*frames;

	AHISURFACE_T		patch;
//	AHIPOINT_T			patchSize;
	AHIPOINT_T			prevPos;
	
	s32					frameTimer;
//	u32					frameInterval;

	s32					auxData;

	TSpriteCallback		drawCallback;

	TSpriteProto		*proto;

	struct TSprite_tag	*prevSprite;

} TSprite;


typedef enum
{
	PHONE_PLATFORM_LTE = 0,
	PHONE_PLATFORM_LTE2

} PHONE_PLATFORM_T;

extern PHONE_PLATFORM_T		gPhonePlatform;


typedef struct
{
	FIXED	x, y;

} TVector2D;

typedef enum
{
	EVRESULT_CANCEL = -2,
	EVRESULT_PROCEED = -1,
	EVRESULT_TOSTATE = 0
	
} EVRESULT_T;

typedef enum
{
	STATE_RESULT_NONE = 0,
	STATE_RESULT_PROCEED,
	STATE_RESULT_RETRY,
	STATE_RESULT_FAIL,
	STATE_RESULT_SUSPEND

} STATE_RESULT_T;


struct TObject_tag;
typedef EVRESULT_T (*TEventHandler)(struct TObject_tag *, void * );

typedef struct
{
	s16				evID;
	s16				evParam;

	TEventHandler	handler;

	s16				toState;			

} TEvent;

typedef struct TRouteStop_tag
{
	s16					platform;
	s16					delay;

} TRouteStop;

#define ANIMATION_TIMER_STOP 			0xFFFF


enum
{
	OBJTYPE_NULL = 0,
	OBJTYPE_STATIC,
	OBJTYPE_CHOPPER,
	OBJTYPE_CAVEMAN,
	OBJTYPE_CAVEWOMAN,
	OBJTYPE_CAVEOLDMAN,
	OBJTYPE_STONE,
	OBJTYPE_TREE,
	OBJTYPE_BIRD,
	OBJTYPE_TRIOPTERUS,
	OBJTYPE_DINO,
	OBJTYPE_SPEECH_BUBBLE,
	OBJTYPE_FRUIT,
	OBJTYPE_GUI_HEALTHBAR,
	OBJTYPE_GUI_DESTINATION,
	OBJTYPE_GUI_SCORE,
	OBJTYPE_GUI_PAYMENT,
	OBJTYPE_WIND,

	OBJTYPE_CUTSCENE_CAVEMAN_CARRY,
	OBJTYPE_CUTSCENE_LOGO,

	OBJTYPE_COLLIDER
};


#define OBJFLAG_HIDING					((u16)1)
#define OBJFLAG_HIDDEN					((u16)2)
#define OBJFLAG_TARGET					((u16)4)
#define OBJFLAG_TARGET_OBJECT			((u16)8)
#define OBJFLAG_PHYSIC					((u16)16)
#define OBJFLAG_DIVE					((u16)32)
#define OBJFLAG_SWIM					((u16)64)
#define OBJFLAG_COLLIDES_LANDSCAPE		((u16)128)
#define OBJFLAG_DELETED					((u16)256)

#define OBJFLAG_MASK_TARGETS			(OBJFLAG_TARGET | OBJFLAG_TARGET_OBJECT)

#define STATE_EVENTS_MAX	4


typedef struct
{
	s16				ID;

	union
	{
		struct
		{
			u16				platform;			// For Stone
												//
			union
			{
				s16				routeOffset;		// For Cavemen, TUghRouteStop
				s16				foodListOffset;		// For the Tree
			};
		};

		struct
		{
				u16		platform;
				u16		speed;
		} triopterus;

		struct
		{
				s8		tx;
				s8		ty;
				s8		direction;
				s8		reach;
		} dino;

		struct
		{
				u16		interval;
				u16		speed;
		} bird;

		struct
		{
				s8		tx;
				s8		ty;
				u8		tw;
				u8		th;
		} collider;

		struct
		{
				s8		tx;
				s8		ty;
				s8		target;

		} csCavemanCarry;
	};

} TGameObject;

typedef struct TObject_tag
{
	u8					type;

	s8					curPlatform;
    TRouteStop			*route;

	u16					flags;

	TVector2D			pos;
	TVector2D			vel;
	TVector2D			accel;
	TVector2D			appliedForce;

	FIXED				mass;
	FIXED				archimedeForce;

	u32					timestamp;

	union
	{
		FIXED				pos;
		s16					obj;
	} target;

	TGameObject			*aux;

	TSprite				*sprite;

	const TEvent		(*logic)[STATE_EVENTS_MAX];
	const TEvent		*state;
	s16					stateData[STATE_EVENTS_MAX];

	struct TObject_tag	*passenger;

	struct TObject_tag	*next;

} TObject;


#ifdef DEBUG_SINGLE_FRAME
extern BOOL			gSingleFrame;
#endif

#ifdef WIN32
#define CONV16(_x_)			((_x_) = E16(_x_))
#define CONV32(_x_)			((_x_) = E32(_x_))
#else
#define CONV16(_x_)
#define CONV32(_x_)
#endif

#endif

