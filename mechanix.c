
#include "mechanix.h"


//#define DEBUG_LANDSCAPE_COLLISIONS

TObject							*gObjectsList;
u32								gObjectsCount;


TObject							*gPlayer;
TObject							*sPlayerPropeller;
TObject							*gSpeechBubble;
TObject							*gPaymentDisplay;

static TVector2D				sPlayerTraction;

TGameState						gGameState;

u32								sPassengersCount;

extern TLevel					*gCurrentLevel;
extern u32						gCurrentLevelNumber;

#ifdef DEBUG_SINGLE_FRAME
BOOL							gSingleFrame;
static BOOL						sFrameRequest;
#endif

static BOOL						sRuntimeObjectsCreation;

static FILE_HANDLE_T			sSaveGameFile;
static s32						sSaveGameObjectsCount;
static BOOL						sSaveGameLoaded;

u32								gTimestamp;
u32								gHighscore;
s32								gHighLevel;
static BOOL						sSuspendRequested;


BOOL mexInit()
{
	mexLoadHighscore(&gHighscore, &gHighLevel);

	return mexReset();
}

BOOL mexTerminate()
{

	return TRUE;
}


BOOL mexReset()
{
	// Completely new game, saved values defaults
	lgcReset();
	
	sSaveGameLoaded = FALSE;
	sSaveGameFile = FILE_HANDLE_INVALID;
	gGameState.livesCount = GAMESTATE_LIVES_COUNT;
	gGameState.score = 0;

	gTimestamp = 0;

	return TRUE;
}

// After everything is loaded and savegame parsed
BOOL mexInitState(u32 level)
{
	lgcInitState();

	sSuspendRequested = FALSE;
/*	mapRender();
	gfxFlushDoublebuffer();*/
	mapRender();

	sPlayerTraction.x = 0;
	sPlayerTraction.y = 0;

	return TRUE;
}

BOOL mexTermState()
{

	if(gGlobalState == GLOBAL_STATE_SUSPEND)
	{
		// If we are terminating for suspend, save the state
		mexSaveGame();
	}

	mexClearObjects();

	sRuntimeObjectsCreation = FALSE;

	return TRUE;
}

BOOL mexAddPlayerHealth(FIXED add)
{
	gGameState.playerHealth += add;

	if(gGameState.playerHealth < 0)
		gGameState.playerHealth = 0;

	if(gGameState.playerHealth > TOFIXED(1.0))
		gGameState.playerHealth = TOFIXED(1.0);

	return TRUE;
}

__inline void mexStopAnimation(TSprite *spr)
{
	spr->frameTimer = ANIMATION_TIMER_STOP;
}

__inline void mexStartAnimation(TSprite *spr)
{
	spr->frameTimer = spr->proto->frameInterval;
}

BOOL mexCallbackSpeechBubble(TSprite *spr, s32 x, s32 y)
{
	guiFontSet(1);
	guiFontSetColor(ATI_565RGB(0x00, 0x00, 0x00));

	if(spr->auxData <= SPEECH_PLATFORM_MAX)
		guiPrintfCenter(x, y, "%d", spr->auxData);
	else
		switch(spr->auxData)
		{
			case SPEECH_QUESTION:
				guiPrintfCenter(x, y, "?!");
			break;

			default:
			break;
		}

	return TRUE;
}

BOOL mexSetRoute(TObject *obj, s16 route)
{
	obj->route = &gRoutes[route];

	return TRUE;
}


/* 
    Create a simple object that does nothing and knows noting
*/
BOOL mexCreateStatic(TObject *obj, u8 spriteID)
{

	obj->pos.x = 0;
	obj->pos.y = 0;

	obj->vel.x = obj->vel.y = 0;

	obj->accel.x = obj->accel.y = 0;
	obj->appliedForce.x = obj->appliedForce.y = 0;

	obj->mass = DEFAULT_MASS;
	obj->archimedeForce = ARCHIMEDE_FORCE_DEFAULT;

	obj->curPlatform = PLATFORM_NONE;

	obj->route = NULL;

	if(spriteID != SPRITE_NULL)
		obj->sprite = resCreateSprite(spriteID, TRUE);
	else
		obj->sprite = NULL;

	obj->flags = OBJFLAG_HIDDEN;

	return TRUE;
}


/* 
    Create a simple object drawn directly from a bitmap
*/
BOOL mexCreateBitmapStatic(TObject *obj, u8 spriteID)
{

	obj->pos.x = 0;
	obj->pos.y = 0;

	obj->vel.x = obj->vel.y = 0;

	obj->accel.x = obj->accel.y = 0;
	obj->appliedForce.x = obj->appliedForce.y = 0;

	obj->mass = DEFAULT_MASS;
	obj->archimedeForce = ARCHIMEDE_FORCE_DEFAULT;

	obj->curPlatform = PLATFORM_NONE;

	obj->route = NULL;

	if(spriteID != SPRITE_NULL)
		obj->sprite = resCreateSprite(spriteID, FALSE);
	else
		obj->sprite = NULL;

	obj->flags = OBJFLAG_HIDDEN;

	return TRUE;
}

BOOL mexCreateCaveman(TObject *obj)
{
	return mexCreateStatic(obj, SPRITE_NULL); // Sprite will be created/changed on state machine request
}

BOOL mexCreateCaveoldman(TObject *obj)
{
	BOOL			result;

	result = mexCreateStatic(obj, SPRITE_NULL); // Sprite will be created/changed on state machine request

	if( result == TRUE )
	{
		obj->mass = TOFIXED(2.0); // Caveoldmen cant swim
	}

	return result;

}

BOOL mexCreateCutsceneCavemanCarry(TObject *obj, TGameObject *params)
{
	mexCreateStatic(obj, SPRITE_CS0_CAVEMAN_CARRY); // Sprite will be created/changed on state machine request

	obj->pos.x = TOFIXED(((s32)params->csCavemanCarry.tx) * TILE_W);
	obj->pos.y = lgcUtilPlaceOver(obj, ((s16)params->csCavemanCarry.ty) * TILE_H);

	lgcUtilSetTargetPosition(obj, ((s32)params->csCavemanCarry.target) * TILE_W, TOFIXED(8));
	
	lgcUtilShowObject(obj, TRUE);

	obj->aux = params;

	return TRUE;
}


BOOL mexCreateCutsceneLogo(TObject *obj, TGameObject *params)
{
	mexCreateBitmapStatic(obj, SPRITE_LOGO); // Sprite will be created/changed on state machine request

	lgcUtilPlaceObject(obj, ((s32)params->csCavemanCarry.tx) * TILE_W, ((s32)params->csCavemanCarry.ty) * TILE_H);

	lgcUtilSetTargetPosition(obj, ((s32)params->csCavemanCarry.target) * TILE_W, TOFIXED(8));
	
	lgcUtilShowObject(obj, TRUE);

	obj->aux = params;

	return TRUE;
}


BOOL mexCreateBird(TObject *obj, TGameObject *passenger)
{
	if( mexCreateStatic(obj, SPRITE_NULL) == FALSE ) // Sprite will be created/changed on state machine request
		return FALSE;

	obj->pos.x = TOFIXED(-20);
	obj->aux = passenger;

	obj->mass = STONE_MASS;

	// Debug
	//obj->aux->bird.speed = 100; //passenger->bird.speed;
	//obj->aux->bird.interval = 5000; //passenger->bird.delay;

	return TRUE;
}

BOOL mexCreateDino(TObject *obj, TGameObject *passenger)
{
	TObject 		*wind;

	if( mexCreateStatic(obj, SPRITE_DINO) == FALSE )
		return FALSE;

	obj->aux = passenger;

	// Coordinates are for lower left corner of the sprite
	obj->pos.x = TOFIXED( obj->aux->dino.tx * TILE_W + resGetSpriteWidth(obj->sprite) / 2 );
	obj->pos.y = lgcUtilPlaceOver(obj, obj->aux->dino.ty * TILE_H);
	obj->flags = 0;

	// debug
	//obj->aux->dino.reach = 1;

	wind = mexCreateObject(OBJTYPE_WIND, passenger);

	wind->pos.x = TOFIXED( obj->aux->dino.tx * TILE_W - obj->aux->dino.reach * TILE_W / 2);
	wind->pos.y = TOFIXED( obj->aux->dino.ty * TILE_H - TILE_H / 2);

	obj->passenger = wind;

	return TRUE;
}

BOOL mexCreateSpeechBubble(TObject *obj)
{
	BOOL				result;

	result = mexCreateStatic(obj, SPRITE_SPEECH_BUBBLE);
	
	if(result)
		obj->sprite->drawCallback = mexCallbackSpeechBubble;

	return result;
}

BOOL mexCreateHealthBarObject(TObject *obj)
{
	BOOL				result;

	result = mexCreateStatic(obj, SPRITE_NULL);

	obj->sprite = resCreateEmptySprite(GUI_GAMESTATE_TIMER_WIDTH, GUI_GAMESTATE_TIMER_HEIGHT);
	
	obj->sprite->drawCallback = guiCallbackHealthBar;

	obj->flags &= ~OBJFLAG_HIDDEN;

	obj->pos.x = TOFIXED(GUI_GAMESTATE_TIMER_X);
	obj->pos.y = TOFIXED(GUI_GAMESTATE_TIMER_Y);

	return result;
}

BOOL mexCreateScoreDisplayObject(TObject *obj)
{
	BOOL				result;

	result = mexCreateStatic(obj, SPRITE_NULL);

	obj->sprite = resCreateEmptySprite(GUI_GAMESTATE_SCORE_WIDTH, GUI_GAMESTATE_SCORE_HEIGHT);
	
	obj->sprite->drawCallback = guiCallbackScore;

	obj->flags &= ~OBJFLAG_HIDDEN;

	obj->pos.x = TOFIXED(GUI_GAMESTATE_SCORE_X);
	obj->pos.y = TOFIXED(GUI_GAMESTATE_SCORE_Y);

	return result;
}

BOOL mexCreatePaymentDisplayObject(TObject *obj)
{
	BOOL				result;

	result = mexCreateStatic(obj, SPRITE_NULL);

	obj->sprite = resCreateEmptySprite(GUI_GAMESTATE_PAYMENT_WIDTH, GUI_GAMESTATE_PAYMENT_HEIGHT);
	
	obj->sprite->drawCallback = guiCallbackPayment;

	return result;
}

BOOL mexCreateCollider(TObject *obj, TGameObject *params)
{
	BOOL				result;

	result = mexCreateStatic(obj, SPRITE_NULL);

	obj->sprite = resCreateEmptySprite(params->collider.tw * TILE_W, params->collider.th * TILE_H);

	obj->pos.x = TOFIXED(params->collider.tx * TILE_W + TILE_W / 2);
	obj->pos.y = TOFIXED(params->collider.ty * TILE_H + TILE_H / 2);

	obj->aux = params;

	obj->flags &= ~OBJFLAG_HIDDEN;

	return result;
}

BOOL mexCreateWind(TObject *obj, TGameObject *passenger)
{
	BOOL				result;

	result = mexCreateStatic(obj, SPRITE_NULL);

	obj->sprite = resCreateEmptySprite(passenger->dino.reach * TILE_W, TILE_H);

	// Inhaling first
	obj->sprite->direction = 1;
	obj->flags &= ~OBJFLAG_HIDDEN;

	return result;
}

BOOL mexCreateStone(TObject *obj, TGameObject *passenger)
{
	BOOL				result;

	result = mexCreateStatic(obj, SPRITE_STONE);

	if(result == TRUE)
	{
		obj->flags &= ~OBJFLAG_HIDDEN;
		obj->flags |= OBJFLAG_COLLIDES_LANDSCAPE;
	
		obj->curPlatform = passenger->platform;

		obj->mass = STONE_MASS;

		obj->pos.x = TOFIXED(gLevelPlatforms[obj->curPlatform].object);
		obj->pos.y = lgcUtilPlaceOver(obj, gLevelPlatforms[obj->curPlatform].level);
	}

	return result;
}



BOOL mexCreateTree(TObject *obj, TGameObject *passenger)
{
	BOOL				result;

	result = mexCreateStatic(obj, SPRITE_TREE);

	if(result == TRUE)
	{
		obj->flags &= ~OBJFLAG_HIDDEN;
	
		obj->curPlatform = passenger->platform;

		obj->pos.x = TOFIXED(gLevelPlatforms[obj->curPlatform].object);
		obj->pos.y = lgcUtilPlaceOver(obj, gLevelPlatforms[obj->curPlatform].level);
	}

	return result;
}

BOOL mexCreateTriopterus(TObject *obj, TGameObject *passenger)
{
	BOOL				result;
	s32					target;

	result = mexCreateStatic(obj, SPRITE_TRIOPTERUS_WALK);

	if(result == TRUE)
	{
		obj->flags &= ~OBJFLAG_HIDDEN;
	
		obj->aux = passenger;

		// debug
//		passenger->triopterus.speed = 30;

		obj->curPlatform = passenger->triopterus.platform;

		obj->pos.x = TOFIXED( gLevelPlatforms[obj->curPlatform].object );
		obj->pos.y = lgcUtilPlaceOver(obj, gLevelPlatforms[obj->curPlatform].level);

		// null target for now, to let the logic init it properly
		lgcUtilSetTargetPosition(obj, obj->pos.x >> FP_BASE , TOFIXED(passenger->triopterus.speed));
	}

	return result;
}

/* 
    Creates a fruit with random sprite
*/
BOOL mexCreateFruit(TObject *obj)
{
	BOOL				result;
	s32					rnd;

	rnd = random(SPRITE_FRUIT_MAX - SPRITE_FRUIT_MIN + 1);

	result = mexCreateStatic(obj, SPRITE_FRUIT_MIN + rnd);
	
	if(result)
	{
		obj->flags |= OBJFLAG_PHYSIC | OBJFLAG_COLLIDES_LANDSCAPE;
		obj->flags &= ~OBJFLAG_HIDDEN;
	}

	return result;
}

TObject* mexCreateObject(u8 type, void* data)
{
	TObject				*obj;
	TGameObject			*passenger = (TGameObject*)data;
	u32					err;
	u32					i;

	obj = (TObject*)suAllocMem(sizeof(TObject), &err);

	memset((void*)obj, 0, sizeof(TObject));
	

	// Insert the object into global objects registry

	if(sRuntimeObjectsCreation == FALSE)
	{
		obj->next = gObjectsList;

		gObjectsList = obj;
	}
	else
	{
		// Runtime-created objects should be added to the tail
		// of the list to preserve other objects' numbering

		TObject			*tail;

		tail = gObjectsList;

		while(tail->next != NULL)
			tail = tail->next;

		tail->next = obj;
	}

	obj->type = type;

	gObjectsCount++;


	// Initialize the object by its type

	switch(type)
	{
		case OBJTYPE_SPEECH_BUBBLE:
			mexCreateSpeechBubble(obj);
		break;

		case OBJTYPE_CAVEMAN:
		case OBJTYPE_CAVEWOMAN:
		case OBJTYPE_CAVEOLDMAN:

			if( type == OBJTYPE_CAVEOLDMAN)
				mexCreateCaveoldman(obj);
			else
				mexCreateCaveman(obj);

            mexSetRoute(obj, passenger->routeOffset);

			sPassengersCount++;

		break;

		case OBJTYPE_STONE:
			mexCreateStone(obj, passenger);
		break;

		case OBJTYPE_TREE:
			mexCreateTree(obj, passenger);
		break;


		case OBJTYPE_FRUIT:
			mexCreateFruit(obj);
		break;

		case OBJTYPE_GUI_HEALTHBAR:
			mexCreateHealthBarObject(obj);
		break;

		case OBJTYPE_GUI_SCORE:
			mexCreateScoreDisplayObject(obj);
		break;

		case OBJTYPE_GUI_PAYMENT:
			mexCreatePaymentDisplayObject(obj);
		break;

		case OBJTYPE_BIRD:
			mexCreateBird(obj, passenger);
		break;

		case OBJTYPE_DINO:
			mexCreateDino(obj, passenger);
		break;

		case OBJTYPE_WIND:
			mexCreateWind(obj, passenger);
		break;

		case OBJTYPE_TRIOPTERUS:
			mexCreateTriopterus(obj, passenger);
		break;

		case OBJTYPE_CUTSCENE_CAVEMAN_CARRY:
			mexCreateCutsceneCavemanCarry(obj, passenger);
		break;

		case OBJTYPE_CUTSCENE_LOGO:
			mexCreateCutsceneLogo(obj, passenger);
		break;

		case OBJTYPE_COLLIDER:
			mexCreateCollider(obj, passenger);
		break;

		case OBJTYPE_STATIC:
		default:
			mexCreateStatic(obj, (u8)(u32)data);
		break;
	}

	obj->aux = data;

	lgcInitObject(obj, 0);

	return obj;
}


BOOL mexFreeObject(TObject *obj)
{
	if(obj->sprite != NULL)
		resFreeSprite(obj->sprite);

	suFreeMem(obj);

	return TRUE;
}

/* 
    Removes the object from the list and frees it, not for use in iterators!
*/
BOOL mexRemoveObject(TObject *obj)
{
	TObject			*prev;
	TObject			*cur;

	prev = NULL;
	cur = gObjectsList;

	while( cur != NULL )
	{
		if(obj == cur)
		{
			if(prev != NULL)
				prev->next = obj->next;
			else
				gObjectsList = obj->next;

			mexFreeObject(obj);

			return TRUE;
		}

		prev = cur;
		cur = cur->next;
	}

	return FALSE;
}

/* 
    Queues the object's removal. Can be used in iterators.
*/
BOOL mexDeleteObject(TObject *obj)
{
	lgcUtilShowObject(obj, FALSE);

	obj->flags |= OBJFLAG_DELETED;

	dbgf("Object deleted, ID = %d", obj->type);

	return TRUE;
}


/* 
    Removes the marked for deletion objects, for runtime
*/
BOOL mexCleanDeletedObjects()
{
	TObject			*prev;
	TObject			*next;
	TObject			*obj;

	prev = NULL;
	obj = gObjectsList;

	while( obj != NULL )
	{
		// Get the next now, since obj could be freed
		next = obj->next;

		if(obj->flags & OBJFLAG_DELETED)
		{
			/*if(prev != NULL)
				prev->next = next;
			else
				gObjectsList = next;

			mexFreeObject(obj);*/

			// Making the object a dummy, in order to not to break the objects' index

			obj->type = OBJTYPE_NULL;
			obj->flags = OBJFLAG_HIDDEN;

			if(obj->sprite != NULL)
			{
				resFreeSprite(obj->sprite);
				obj->sprite = NULL;
			}

			obj->state = NULL;
		}

		prev = obj;
		obj = next;
	}

	return FALSE;
}

/* For termination */
BOOL mexClearObjects()
{
	TObject				*obj, *next;
	u32					i;

	gObjectsCount = 0;

	next = gObjectsList;
	
	if(next == NULL)
		return TRUE;

	do
	{
		obj = next;

		next = obj->next;

		mexFreeObject(obj);

	}while( next != NULL );

	gObjectsList = NULL;

	return TRUE;
}



BOOL mexCreateGuiObjects()
{
	TObject				*obj;

	obj = mexCreateObject(OBJTYPE_GUI_HEALTHBAR, NULL);

	obj = mexCreateObject(OBJTYPE_GUI_SCORE, NULL);

	gPaymentDisplay = mexCreateObject(OBJTYPE_GUI_PAYMENT, NULL);

	return TRUE;
}

BOOL mexCreatePlayer(TSavedObject	*sobj)
{
	TObject				*obj;

	obj = mexCreateObject(OBJTYPE_CHOPPER, NULL);

	if(sobj != NULL)
	{
		// Init from a savegame
		obj->pos = sobj->pos;
		
		obj->vel = sobj->vel;
	
		obj->accel = sobj->accel;
	
		obj->curPlatform = sobj->curPlatform;
	
		obj->flags = sobj->flags;

		// We just copy indexes for now
		obj->passenger = (TObject*)(u32)sobj->passenger;
	}
	else
	{
		// Default values

		obj->pos.x = TOFIXED(gCurrentLevel->firstPlayerX);
		obj->pos.y = TOFIXED(gCurrentLevel->firstPlayerY);
		
		obj->vel.x = obj->vel.y = 0;
	
		obj->accel.x = obj->accel.y = 0;
	
		obj->curPlatform = PLATFORM_NONE;
	
		obj->flags = OBJFLAG_COLLIDES_LANDSCAPE | OBJFLAG_PHYSIC;
	}

	obj->mass = PLAYER_MASS;
	obj->archimedeForce = ARCHIMEDE_FORCE_CHOPPER;

	obj->sprite = resCreateSprite(SPRITE_CHOPPER, TRUE);

	gPlayer = obj;


	obj = mexCreateObject(OBJTYPE_CHOPPER, NULL);

	obj->pos.x = gPlayer->pos.x;
	obj->pos.y = gPlayer->pos.y - (gPlayer->sprite->proto->patchSize.y / 2) - 1;

	obj->vel.x = obj->vel.y = 0;
	obj->accel.x = obj->accel.y = 0;

	obj->mass = DEFAULT_MASS;

	obj->curPlatform = PLATFORM_NONE;

	obj->sprite = resCreateSprite(SPRITE_PROPELLER, TRUE);

	obj->flags = 0;

	sPlayerPropeller = obj;

	if(sobj != NULL)
	{
		obj->passenger = (TObject*) -1;
	}


	return TRUE;
}

BOOL mexPrepareLevel(u32 levelIdx)
{
	TGameObject			*passList;
	TObject				*obj;
	BOOL				result;


	/* 
	    Creation order is the order which will be used to draw sprites.
	    Last created will be the first drawn (more backwards towards Z-order)
	*/

	sRuntimeObjectsCreation = FALSE;

	/* Foremost objects */

	// Create a speech bubble
	gSpeechBubble = mexCreateObject(OBJTYPE_SPEECH_BUBBLE, NULL);

	mexCreateGuiObjects();


	
	/* Background layer objects */
	
	if( mexLoadStateObjects() == FALSE )
	{

		gGameState.completeRoutes = 0;
		gGameState.passengerDeaths = 0;
		gGameState.playerHealth = TOFIXED(1.0);
		gGameState.playerCrash = FALSE;
		gGameState.playerCrashTimer = GAMESTATE_CRASH_TIMER;

		/* Middle layer objects */

		// Create a player's Chopper
		result = mexCreatePlayer(NULL);

		// Passengers

		passList = &gPassengers[gCurrentLevel->passengersTblOff];
	
		sPassengersCount = 0;

		while(passList->ID != -1)
		{
			mexCreateObject(passList->ID, (void*)passList);
			passList++;
		}
	}

	sRuntimeObjectsCreation = TRUE;

	return result;
}

#ifdef LANDSCAPE_RIGHT

#define MEX_MAP_LEFT		(MULTIKEY_UP		| MULTIKEY_2)
#define MEX_MAP_RIGHT		(MULTIKEY_DOWN		| MULTIKEY_8)
#define MEX_MAP_UP			(MULTIKEY_RIGHT 	| MULTIKEY_6)
#define MEX_MAP_DOWN		(MULTIKEY_LEFT		| MULTIKEY_4)

#define MEX_MAP_LEFT_UP		(MULTIKEY_3)
#define MEX_MAP_RIGHT_UP	(MULTIKEY_9)
#define MEX_MAP_LEFT_DOWN	(MULTIKEY_1)
#define MEX_MAP_RIGHT_DOWN	(MULTIKEY_7)

#define MEX_MAP_EJECT		(MULTIKEY_JOY_OK	| MULTIKEY_5)
#define MEX_MAP_MENU		(MULTIKEY_SOFT_LEFT	| MULTIKEY_SOFT_RIGHT)

#else

#define MEX_MAP_LEFT		(MULTIKEY_DOWN		| MULTIKEY_8)
#define MEX_MAP_RIGHT		(MULTIKEY_UP		| MULTIKEY_2)
#define MEX_MAP_UP			(MULTIKEY_LEFT		| MULTIKEY_4)
#define MEX_MAP_DOWN		(MULTIKEY_RIGHT 	| MULTIKEY_6)

#define MEX_MAP_LEFT_UP		(MULTIKEY_7)
#define MEX_MAP_RIGHT_UP	(MULTIKEY_1)
#define MEX_MAP_LEFT_DOWN	(MULTIKEY_9)
#define MEX_MAP_RIGHT_DOWN	(MULTIKEY_3)

#define MEX_MAP_EJECT		(MULTIKEY_JOY_OK	| MULTIKEY_5)
#define MEX_MAP_MENU		(MULTIKEY_SOFT_LEFT	| MULTIKEY_SOFT_RIGHT)

#endif

void mexHandleKeys(u32 keysPressed, u32 keysReleased, u32 keysHold)
{
	sPlayerTraction.x = sPlayerTraction.y = 0;

	if(keysHold & MEX_MAP_LEFT)
		sPlayerTraction.x = -TRACTION_X;

	if(keysHold & MEX_MAP_RIGHT)
		sPlayerTraction.x = TRACTION_X;

	if(keysHold & MEX_MAP_UP)
		sPlayerTraction.y = -TRACTION_Y;

	if(keysHold & MEX_MAP_DOWN)
		sPlayerTraction.y = TRACTION_Y;

	/* 
	  Diagonal keys
	*/
	 
	if(keysHold & MEX_MAP_LEFT_UP)
	{
		sPlayerTraction.y = -TRACTION_Y;
		sPlayerTraction.x = -TRACTION_X;
	}

	if(keysHold & MEX_MAP_RIGHT_UP)
	{
		sPlayerTraction.y = -TRACTION_Y;
		sPlayerTraction.x = TRACTION_X;
	}

	if(keysHold & MEX_MAP_LEFT_DOWN)
	{
		sPlayerTraction.y = TRACTION_Y;
		sPlayerTraction.x = -TRACTION_X;
	}

	if(keysHold & MEX_MAP_RIGHT_DOWN)
	{
		sPlayerTraction.y = TRACTION_Y;
		sPlayerTraction.x = TRACTION_X;
	}

	/* 
	  Other keys
	*/

	if(keysPressed & MEX_MAP_EJECT)
		lgcRegisterEvent(EVENT_EJECT, (void*)NULL);

	#ifdef DEBUG_SINGLE_FRAME
	if(gSingleFrame && (keysPressed & MULTIKEY_0))
		sFrameRequest = TRUE;
	#endif

	if(keysReleased & MEX_MAP_MENU)
	{
		sSuspendRequested = TRUE;
	}

/*	if(keysReleased & MULTIKEY_POUND)
	{
		mexSaveGame();
	}
 
	// TODO: Remove this crap! 
	if(keysReleased & MULTIKEY_STAR)
	{
		mexTermState();
		gGlobalState = GLOBAL_STATE_EXIT;
	}
*/

}


void applyBounds(s32 *val, s32 min, s32 max)
{
	if( *val < min )
		*val = min;

	if( *val > max )
		*val = max;
}

/* 
	Check for Interval intersection 
    x1, x2 - centers of intervals
    w1, w2 - widths of intervals
*/
BOOL checkIntersect(s32 x1, s32 w1, s32 x2, s32 w2)
{
	s32				sumw = (w1 / 2) + (w2 / 2);

	if( abs(x2-x1) <= sumw )
		return TRUE;
	
	return FALSE;
}


/* 
    Check for collisions of obj (chopper) with landscape
*/
#define COLLISION_NONE			0
#define COLLISION_VERTICAL		1
#define COLLISION_HORIZONTAL	2
#define COLLISION_CORNER		4

BOOL mexCalcLandscapeCollisions(TObject *whom)
{
	TSprite				*spr;
	s32					top, left, right, bottom;
	s32					w, h, tx, ty, x, y, i;
	u8					tile;
	u32					code = COLLISION_NONE;
	TPlatform			*plt;
	FIXED				newVel, damage;

	s32					prevX, prevY;

	#ifdef DEBUG_LANDSCAPE_COLLISIONS
		static				u32 counter = 0;
		static				TObject* stat = NULL;
	#endif

	spr = whom->sprite;

	damage = 0;

	w = resGetSpriteWidth(spr);
	h = resGetSpriteHeight(spr);
	
	// Select colliding corner

	left = (whom->pos.x >> FP_BASE) - w/2;
	top = (whom->pos.y >> FP_BASE) - h/2;
	right = left + w;
	bottom = top + h;

	if(whom->vel.x > 0)
		x = right;
	else
		x = left;

	if(whom->vel.y > 0)
		y = bottom;
	else
		y = top;

	/* 
	    Landscape collisions
	*/


	tx = x / TILE_W;
	ty = y / TILE_H;


	// Vertical line
	i = top / TILE_H;
	if((tx >= 0) && (tx < LEVEL_W))
	do
	{
		if( (i != ty) && (i >= 0) && (i < LEVEL_H) )
		{
			tile = gCurrentLevelMap[tx + i * LEVEL_W];
			
			if(tile >= SOLID_TILE)
			{
				code |= COLLISION_VERTICAL;
				break;
			}
		}

		i++;
	} while( ((i * TILE_H) < bottom) && (i < LEVEL_H)  );


	// Horizontal line
	i = left / TILE_W;
	if((ty >= 0) && (ty < LEVEL_H))
	do
	{
		if( (left < 0) || (i != tx) )
		{
			tile = gCurrentLevelMap[i + ty * LEVEL_W];
			
			if(tile >= SOLID_TILE)
			{
				code |= COLLISION_HORIZONTAL;
				break;
			}
		}

		i++;
	} while( ((i * TILE_W) < right) && (i < LEVEL_W) );

	// Corner
	if((tx >= 0) && (ty >= 0) && (tx < LEVEL_W) && (ty < LEVEL_H))
	{
		tile = gCurrentLevelMap[tx + ty * LEVEL_W];
		if(tile >= SOLID_TILE)
			code |= COLLISION_CORNER;
	}


	#ifdef DEBUG_LANDSCAPE_COLLISIONS
		if(code != COLLISION_NONE)
		{
			dbgf("%d CODE = %d y: %d", counter, code, whom->pos.y>>FP_BASE);
	
			stat = whom;
		}
	#endif

	
	if(code & COLLISION_HORIZONTAL)
	{
		// Landing check
		plt = gLevelPlatforms;
		i = 0;

		while(plt->left != -1)
		{
			if( (ty == (plt->level / TILE_H)) &&
				((whom->pos.x >> FP_BASE) >= plt->left) &&
				((whom->pos.x >> FP_BASE) <= plt->right) &&
				(whom->vel.y > 0) &&
				(whom->vel.y < LANDING_SPEED) )
			{
				whom->vel.x = whom->vel.y = 0;
				whom->flags &= ~OBJFLAG_PHYSIC;


				whom->curPlatform = i;

				lgcRegisterEvent(EVENT_LANDING, (void*)whom);

				dbgf("LANDING %d", whom->type);

				break;
			}

			plt++;
			i++;
		}

		newVel = fpmul(whom->vel.y, BOUNCE_KOEF);
		damage += abs(whom->vel.y - newVel);
		whom->vel.y = newVel;
	}
	else if(code & COLLISION_VERTICAL)
	{
		newVel = fpmul(whom->vel.x, BOUNCE_KOEF);
		damage += abs(whom->vel.x - newVel);
		whom->vel.x = newVel;
	}


	if(code == COLLISION_CORNER)
	{
		
		if(whom->vel.x < 0)
			prevX = (whom->sprite->prevPos.x) / TILE_W;
		else
			prevX = (whom->sprite->prevPos.x + w) / TILE_W;

		if(whom->vel.y < 0)
			prevY = (whom->sprite->prevPos.y) / TILE_H;
		else
			prevY = (whom->sprite->prevPos.y + h) / TILE_H;


		if(prevX != tx)
		{
			newVel = fpmul(whom->vel.x, BOUNCE_KOEF);
			damage += abs(whom->vel.x - newVel);
			whom->vel.x = newVel;
			code |= COLLISION_VERTICAL;
		}


		if(prevY != ty)
		{
			newVel = fpmul(whom->vel.y, BOUNCE_KOEF);
			damage += abs(whom->vel.y - newVel);
			whom->vel.y = newVel;
			code |= COLLISION_HORIZONTAL;
		}


		/*if( abs(obj->vel.x) > abs(obj->vel.y) )
		{
			obj->vel.x = fpmul(obj->vel.x, BOUNCE_KOEF);
			code |= 1;
		}
		else
		{
			obj->vel.y = fpmul(obj->vel.y, BOUNCE_KOEF);
			code |= 2;
		}*/

		//dbgf("%d. Corner collision detected, dx %d dy %d", counter++, tx-prevX, ty-prevY);
	}



	if(code & COLLISION_HORIZONTAL)
	{
		if(whom->vel.y <= 0)
		{
			ty = bottom / TILE_H;

			//whom->pos.y = (ty * TILE_H - h/2 -1) << FP_BASE;
			whom->pos.y = (ty * TILE_H - h/2 - 1) << FP_BASE;
		}
		else
		{
			ty = top / TILE_H;

			whom->pos.y = ((ty+1) * TILE_H + (h+1)/2) << FP_BASE;

		}
	}

	if(code & COLLISION_VERTICAL)
	{
		if(whom->vel.x < 0)
		{
			tx = right / TILE_W;

			whom->pos.x = (tx * TILE_W - w/2 - 1) << FP_BASE;
		}
		else
		{
			tx = left / TILE_W;

			whom->pos.x = ((tx+1) * TILE_W + (w+1)/2 + 1) << FP_BASE;

		}
	}

	if( (whom->type == OBJTYPE_CHOPPER) && (damage > DAMAGE_CUTOFF) && (gGameState.playerHealth > 0))
	{
		mexAddPlayerHealth( -fpmul(damage-DAMAGE_CUTOFF, DAMAGE_KOEF) );
		//dbg("UGH!");
	}

	#ifdef DEBUG_LANDSCAPE_COLLISIONS
		if(stat == whom && counter < 10)
		{
			dbgf("vy %d ny %d\n", whom->vel.y>>FP_BASE, whom->pos.y>>FP_BASE);
			counter++;
		}
	#endif

	return (BOOL)(code != COLLISION_NONE);
}

BOOL mexCalcObjectCollisions()
{
	s32					x,y,w,h;
	TObject				*whom, *obj;
	TCollisionEvData	collision;
	BOOL				colliderPhysic;

	/* 
	    Objects collisions
	    Loops organized so that every collision check is performed only once
	*/

	whom = gObjectsList;

	while(whom != NULL)
	{

		if((whom->flags & OBJFLAG_HIDDEN) == 0)
		{
			x = whom->pos.x >> FP_BASE;
			y = whom->pos.y >> FP_BASE;
	
			w = resGetSpriteWidth(whom->sprite);
			h = resGetSpriteHeight(whom->sprite);

			colliderPhysic = ((whom->flags & OBJFLAG_PHYSIC) != 0);
	
			collision.obj1 = whom;
	
			obj = whom->next;
	
			while(obj != NULL)
			{
				if( ((obj->flags & OBJFLAG_HIDDEN) == 0) &&
					( obj->type != whom->type ) &&
					( colliderPhysic || 
					  (obj->flags & OBJFLAG_PHYSIC) ||
					  (obj->type == OBJTYPE_CHOPPER)) &&
					checkIntersect(x, w, obj->pos.x >> FP_BASE, resGetSpriteWidth(obj->sprite)) &&
					checkIntersect(y, h, obj->pos.y >> FP_BASE, resGetSpriteHeight(obj->sprite)) )
				{
					collision.obj2 = obj;
					lgcRegisterEvent(EVENT_COLLISION, (void*)&collision);

//					dbgf("COLLISION %d-%d\n", whom->type, obj->type);
				}

				obj = obj->next;
			}
		}

		whom = whom->next;
	}

	return TRUE;
}

BOOL mexUpdatePlayer()
{
	TObject				*obj;

	/*
		Propeller
	*/

	if(gGameState.playerHealth > 0)
	{
		obj = sPlayerPropeller;
	
		obj->pos.x = gPlayer->pos.x;
		obj->pos.y = gPlayer->pos.y - ((resGetSpriteHeight(gPlayer->sprite) / 2) << FP_BASE) - FP_ONE;
	}
	else if( (gGameState.playerHealth == 0) && (gGameState.playerCrash == FALSE) )
	{
		lgcUtilRandomFlyout(sPlayerPropeller, NULL);

		obj = mexCreateObject(OBJTYPE_STATIC, (void*)SPRITE_CAVEMAN_FALL);
		lgcUtilShowObject(obj, TRUE);
		lgcUtilRandomFlyout(obj, &gPlayer->pos);

		dbg("flyoff");

		gGameState.playerCrash = TRUE;

		mexStopAnimation(gPlayer->sprite);
		mexStartAnimation(sPlayerPropeller->sprite);
	}

	/* 
	    Flying animation
	*/

	if(gGameState.playerCrash == FALSE)
	{
		if((sPlayerTraction.x == 0 && sPlayerTraction.y == 0) || ((gPlayer->flags & OBJFLAG_PHYSIC)==0))
		{
			mexStopAnimation(gPlayer->sprite);
			mexStopAnimation(sPlayerPropeller->sprite);
		}
		else if(gPlayer->sprite->frameTimer == ANIMATION_TIMER_STOP)
		{
			mexStartAnimation(gPlayer->sprite);
			mexStartAnimation(sPlayerPropeller->sprite);
		}
	}

	return TRUE;
}

BOOL mexPhysicalMovement(TObject *obj)
{
	FIXED				one_div_mass;
	FIXED				tickPow2;
	FIXED				yForce, xForce, archimedeForce;
	FIXED				h;
	TVector2D			friction;

	tickPow2 = gTick*gTick;

	// TODO: cache this?
	one_div_mass = fpdiv(FP_ONE, obj->mass);

	/* 
	    Traction or other force
	*/

	xForce = obj->appliedForce.x;
	//xForce = 0;
	//yForce = obj->appliedForce.y;
	//yForce = 0;

	/* 
		Friction force
	*/

	if((obj->vel.x != 0) && ((obj->flags & OBJFLAG_MASK_TARGETS) == 0))
	{
		friction.x = fpmul(obj->vel.x, FRICTION_KOEF);

		xForce += friction.x;
	}
	else
		friction.x = 0;

	friction.y = fpmul(obj->vel.y, FRICTION_KOEF);


	/* 
	    Archimede force
	*/

	archimedeForce = 0;
	yForce = obj->appliedForce.y;

	if(gWaterLevel >= 0)
	{
	
		h = resGetSpriteHeight(obj->sprite) << FP_BASE;
	
		archimedeForce = obj->pos.y + h/2 - gWaterLevel;
	
		if(archimedeForce > h)
		{
			archimedeForce = h;
	
			// Our engine is ineffective underwater
			yForce = 0;
		}
	}

	// Reset the applied force for the next frame
	obj->appliedForce.x = 0;
	obj->appliedForce.y = 0;
	
	if(archimedeForce > 0)
	{
		// Applied water friction
		archimedeForce = fpmul(fpdiv(archimedeForce, h), obj->archimedeForce) + friction.y;

		yForce += archimedeForce;
	}
	else
	{
		archimedeForce = 0;
	}


	/* 
		Water entering damping
	*/

	if( (archimedeForce < 0) && (obj->vel.y > 0) && ((obj->flags & OBJFLAG_DIVE)==0) )
	{
		//archimedeForce += WATER_DEMP_FORCE;
		obj->vel.y /= 2;
		//obj->vel.y = 0;
		obj->flags |= OBJFLAG_DIVE;
	}

	if((archimedeForce == 0) && ((obj->flags & OBJFLAG_DIVE)!=0))
	{
		obj->flags &= ~(OBJFLAG_DIVE | OBJFLAG_SWIM);

		if(obj->type == OBJTYPE_CHOPPER)
			lgcRegisterEvent(EVENT_TAKEOFF, (void*)-1);
	}


	/* 
	    Newton law
	*/

	obj->accel.y = GRAVITY_ACCEL; // a = mg/m = g

	if(yForce != 0)
	{
//		yForce += fpmul(GRAVITY_ACCEL, obj->mass);
		
		obj->accel.y += fpmul(yForce, one_div_mass);

		if( ((obj->flags & OBJFLAG_SWIM) == 0) && (abs(obj->accel.y) < SWIM_ACCELERATION) && (abs(obj->vel.y) < SWIM_VELOCITY) )
		{
			cprint("SWIM\n");

			obj->flags |= OBJFLAG_SWIM;

			lgcRegisterEvent(EVENT_SWIM, (void*)obj);
		}
	}

	if(xForce != 0)
		obj->accel.x = fpmul(xForce, one_div_mass);
	else
		obj->accel.x = 0;


	// Euler integrator

	obj->pos.x += fpmul(obj->vel.x, gTick) + fpmul2(obj->accel.x, tickPow2) / 2;
	obj->pos.y += fpmul(obj->vel.y, gTick) + fpmul2(obj->accel.y, tickPow2) / 2;

	obj->vel.x += fpmul(obj->accel.x, gTick);
	obj->vel.y += fpmul(obj->accel.y, gTick);

	// Velocity boundaries check

	//applyBounds(&obj->vel.x, -MAX_VELOCITY, MAX_VELOCITY);
	//applyBounds(&obj->vel.y, -MAX_VELOCITY, MAX_VELOCITY);

	// Screen boundaries check

	applyBounds(&obj->pos.x, /*3*FP_ONE*/0, TOFIXED(SCREEN_W));
	applyBounds(&obj->pos.y, 0, DEADLY_DEPTH*2);

/*	if((obj->pos.x == 0) || (obj->pos.x == (SCREEN_W<<FP_BASE)))
		obj->vel.x = 0;
*/
	if((obj->pos.y == 0) || (obj->pos.y == TOFIXED(SCREEN_H)))
		obj->vel.y = 0;


	if( ( (obj->pos.y + h/2) < 0 ) ||
		( (obj->pos.y - h/2) > TOFIXED(SCREEN_H) ) )
	{
		lgcRegisterEvent(EVENT_OFFSCREEN, (void*)obj);
	}

	// If we drown far - we are dead :)
	if(obj->pos.y > DEADLY_DEPTH)
	{
		mexDeleteObject(obj);

		if( (obj->type == OBJTYPE_CAVEMAN) ||
			(obj->type == OBJTYPE_CAVEWOMAN) ||
			(obj->type == OBJTYPE_CAVEOLDMAN) )
		{
			gGameState.passengerDeaths++;
		}
	}

	if(obj->flags & OBJFLAG_COLLIDES_LANDSCAPE)
	{
		mexCalcLandscapeCollisions(obj);
	}

	return TRUE;
}

BOOL mexUpdateObject(TObject *obj)
{
	TSprite				*sprite;

	sprite = obj->sprite;

	// Animation
	if(sprite->frameTimer != ANIMATION_TIMER_STOP)
	{
		sprite->frameTimer -= gTick;
		if(sprite->frameTimer <= 0)
		{
			sprite->curFrame++;
			if(sprite->curFrame >= sprite->proto->framesCount)
			{
				sprite->curFrame = 0;
				lgcRegisterEvent(EVENT_ANIMATION, (void*)obj);
			}

			sprite->frameTimer += sprite->proto->frameInterval;

			gfxUpdateSpriteFrame(sprite);
		}
	}

	// Physical movement
	if(obj->flags & OBJFLAG_PHYSIC)
	{
		mexPhysicalMovement(obj);
	}

	// Increasing water
	if( ((obj->flags & OBJFLAG_SWIM) == 0) && 
		((obj->flags & OBJFLAG_PHYSIC) == 0) && 
		(gWaterLevel < obj->pos.y) && obj->type != OBJTYPE_GUI_HEALTHBAR )
	{
		lgcRegisterEvent(EVENT_SWIM, (void*)obj);
	}

	// Moving to target
	if(obj->flags & (OBJFLAG_TARGET | OBJFLAG_TARGET_OBJECT))
	{
		FIXED			target;

		obj->pos.x += fpmul(obj->vel.x, gTick);
		obj->pos.y += fpmul(obj->vel.y, gTick);

		if(obj->flags & OBJFLAG_TARGET)
			target = obj->target.pos;
		else // OBJFLAG_TARGET_OBJECT
			target = lgcUtilGetObject(obj->target.obj)->pos.x;

		if(obj->vel.x < 0)
		{
			if(obj->pos.x <= target)
			{
				obj->vel.x = 0;
				lgcRegisterEvent(EVENT_ARRIVAL, (void*)obj);
			}
		}
		else
		{
			if(obj->pos.x >= target)
			{
				obj->vel.x = 0;
				lgcRegisterEvent(EVENT_ARRIVAL, (void*)obj);
			}
		}
	}

	return TRUE;
}

BOOL mexUpdateObjects()
{
	TObject				*obj;

	lgcRegisterEvent(EVENT_TIMER, (void*)gTick);

	if(gGameState.playerHealth > 0)
	{
		gPlayer->appliedForce.x += sPlayerTraction.x;
		gPlayer->appliedForce.y += sPlayerTraction.y;
	}
	else
	{
		gPlayer->appliedForce.x = 0;
		gPlayer->appliedForce.y = 0;	
	}

	// A little hack, since changing sprites in mexUpdatePlayer is unsafe
	if(gGameState.playerCrash == TRUE && gPlayer->sprite->ID != SPRITE_CHOPPER_EMPTY)
	{
		gPlayer->sprite = resChangeSprite(gPlayer->sprite, SPRITE_CHOPPER_EMPTY);	
	}

	mexCalcObjectCollisions();

	obj = gObjectsList;

	while( obj != NULL )
	{
		if((obj->flags & OBJFLAG_HIDDEN) == 0)
		{
			mexUpdateObject(obj);
		}
		obj = obj->next;
	}
	
	obj = gObjectsList;

	while( obj != NULL )
	{
		if((obj->flags & OBJFLAG_HIDDEN) == 0)
		{
			gfxDrawSpritePatch(obj->sprite, obj->pos);	
		}

		if((obj->flags & OBJFLAG_HIDING) != 0)
		{
			gfxDrawSpritePatch(obj->sprite, obj->pos);

			obj->flags &= ~OBJFLAG_HIDING;
			obj->flags |= OBJFLAG_HIDDEN;
		}

		obj = obj->next;
	}

	// Placement here is important!
	mexUpdatePlayer();

	// Placement here is important! We should patch the original and shift before the patch is updated
	mapRenderWater();

	obj = gObjectsList;

	while( obj != NULL )
	{
		if((obj->flags & OBJFLAG_HIDDEN) == 0)
		{
			gfxUpdateSpritePatch(obj->sprite, obj->pos);	
		}

		obj = obj->next;
	}

	obj = gObjectsList;

	while( obj != NULL )
	{
		if((obj->flags & OBJFLAG_HIDDEN) == 0)
		{
			gfxDrawSprite(obj->sprite, obj->pos);
		}

		obj = obj->next;
	}

	// If there was any objects marked for deletion deleted - remove them now
	mexCleanDeletedObjects();

	return TRUE;
}



STATE_RESULT_T mexUpdate()
{
	gTimestamp += gTick;

	/*
		Game objects
	*/

#ifdef DEBUG_SINGLE_FRAME
	if((gSingleFrame == FALSE) || (gSingleFrame && sFrameRequest))
	{
		mexUpdateObjects();
		sFrameRequest = FALSE;
	}
#else
	mexUpdateObjects();
#endif
	
	/*
		Player
	*/
	if((sPlayerTraction.y < 0) && (gPlayer->flags & OBJFLAG_PHYSIC) == 0)
	{
		gPlayer->flags |= OBJFLAG_PHYSIC;

		gPlayer->appliedForce.x = 0;

		lgcRegisterEvent(EVENT_TAKEOFF, (void*)(u32)gPlayer->curPlatform);

		gPlayer->curPlatform = PLATFORM_NONE;
	}

	/*
		Game conditions
	*/

	if(gGameState.playerCrash)
	{
		gGameState.playerCrashTimer -= gTick;
	}

	// Level win condition
	if( gGameState.completeRoutes == gCurrentLevel->routesToComplete )
	{
		return STATE_RESULT_PROCEED;
	}

	// Level fail conditions
	if( ( (sPassengersCount - gGameState.passengerDeaths) < gCurrentLevel->routesToComplete ) ||
		(gGameState.playerCrashTimer <= 0) )
	{
		gGameState.livesCount--;

		if(gGameState.livesCount == 0)
		{
			return STATE_RESULT_FAIL;
		}

		return STATE_RESULT_RETRY;
	}

	if(sSuspendRequested == TRUE)
	{
		// We will need to reload the savegame
		sSaveGameLoaded = FALSE;
		return STATE_RESULT_SUSPEND;
	}

	return STATE_RESULT_NONE;
}


u16 mexUtilFindRoute(TRouteStop *route)
{
	if(route != NULL)
		return (u16)(route - gRoutes);
	else
		return 0;
}

s16 mexUtilFindPassenger(TGameObject *gobj)
{
	if(gobj != NULL)
		return (u16)(gobj - gPassengers);
	else
		return -1;
}

s16 mexUtilFindState(const TEvent *state, const TEvent	(*logic)[])
{
	if(logic != NULL)
		return (u16)(state - (const TEvent*)logic) / STATE_EVENTS_MAX;
	else
		return 0;
}





#ifdef WIN32
#define CONV16(_x_)			((_x_) = E16(_x_))
#define CONV32(_x_)			((_x_) = E32(_x_))



void convertSavedGameInfo(TSavedGameInfo *dst)
{
	CONV16(dst->level);
	CONV32(dst->gameState.completeRoutes);
	CONV32(dst->gameState.passengerDeaths);
	CONV32(dst->gameState.playerHealth);
	//CONV8(dst->gameState.playerCrash);
	CONV32(dst->gameState.playerCrashTimer);
	CONV32(dst->gameState.livesCount);
	CONV32(dst->gameState.score);
	CONV32(dst->waterLevel);
	CONV32(dst->objCount);
	CONV32(dst->timestamp);
}

void convertSavedObject(TSavedObject *dst)
{
	s32				i;

	CONV16(dst->flags);
	CONV16(dst->route);
	CONV32(dst->pos.x);
	CONV32(dst->pos.y);
	CONV32(dst->vel.x);
	CONV32(dst->vel.y);
	CONV32(dst->accel.x);
	CONV32(dst->accel.y);
	CONV32(dst->appliedForce.x);
	CONV32(dst->appliedForce.y);

	CONV32(dst->mass);
	CONV32(dst->archimedeForce);
	CONV32(dst->timestamp);

	CONV32(dst->target.pos);
	CONV16(dst->aux);
	CONV16(dst->sprite);
	CONV16(dst->state);

	for(i=0; i<STATE_EVENTS_MAX; i++)
	{
		CONV16(dst->stateData[i]);
	}

	CONV32(dst->passenger);
}

#endif


static u32		sLoadLegacy;

BOOL mexSaveObject(TSavedObject *sobj)
{
	u32				written;

	if(sSaveGameFile == FILE_HANDLE_INVALID)
		return FALSE;

	DL_FsWriteFile((void*)sobj, sizeof(TSavedObject), 1, sSaveGameFile, &written);

	return TRUE;
}

#define COPY_LEGACY_STRUCT(_field_, _struct_ ) \
	sobj->_field_ = _struct_._field_;

BOOL mexLoadObject(TSavedObject *sobj)
{
	TSavedObjectV7	sobjv7;
	u32				read;

	if(sSaveGameFile == FILE_HANDLE_INVALID)
		return FALSE;

	/* Legacy support for format 7 */
	if(sLoadLegacy == 7)
	{
		DL_FsReadFile((void*)&sobjv7, 1, sizeof(TSavedObjectV7), sSaveGameFile, &read);

		COPY_LEGACY_STRUCT(type, sobjv7);
		COPY_LEGACY_STRUCT(curPlatform, sobjv7);
		sobj->route = E16((u16)sobjv7.route);
		COPY_LEGACY_STRUCT(flags, sobjv7);
		COPY_LEGACY_STRUCT(pos, sobjv7);
		COPY_LEGACY_STRUCT(vel, sobjv7);
		COPY_LEGACY_STRUCT(accel, sobjv7);
		COPY_LEGACY_STRUCT(appliedForce, sobjv7);
		COPY_LEGACY_STRUCT(mass, sobjv7);
		COPY_LEGACY_STRUCT(archimedeForce, sobjv7);
		COPY_LEGACY_STRUCT(timestamp, sobjv7);
		COPY_LEGACY_STRUCT(target.pos, sobjv7);
		COPY_LEGACY_STRUCT(aux, sobjv7);
		COPY_LEGACY_STRUCT(sprite, sobjv7);
		COPY_LEGACY_STRUCT(state, sobjv7);
		memcpy(sobj->stateData, sobjv7.stateData, sizeof(sobjv7.stateData));
		COPY_LEGACY_STRUCT(passenger, sobjv7);
		
	}
	else
	{
		DL_FsReadFile((void*)sobj, 1, sizeof(TSavedObject), sSaveGameFile, &read);
	}

	if(read != sizeof(TSavedObject))
		return FALSE;

	return TRUE;
}

BOOL mexSaveAllObjects( s32 *count )
{
	TObject			*last;
	TObject			*obj;
	TSavedObject	sobj;

	*count = 0;

	last = NULL;

	// Walking the list backwards
	while( last != gObjectsList )
	{
		obj = gObjectsList;

		while(obj->next != last)
			obj = obj->next;

		last = obj;

		if( obj->type == OBJTYPE_GUI_DESTINATION ||
			obj->type == OBJTYPE_GUI_HEALTHBAR ||
			obj->type == OBJTYPE_GUI_SCORE ||
			obj->type == OBJTYPE_GUI_PAYMENT ||
			obj->type == OBJTYPE_SPEECH_BUBBLE ||
			obj->type == OBJTYPE_WIND ||
			obj == sPlayerPropeller
			)
		{
			obj = obj->next;
			continue;
		}

		sobj.type = obj->type;
		sobj.curPlatform = obj->curPlatform;
		sobj.route = mexUtilFindRoute(obj->route);
		sobj.flags = obj->flags;
		sobj.pos = obj->pos;
		sobj.vel = obj->vel;

		sobj.accel = obj->accel;				// Need this?
		sobj.appliedForce = obj->appliedForce; // ?

		sobj.mass = obj->mass;					// ?
		sobj.archimedeForce = obj->archimedeForce; // ?

		sobj.timestamp = obj->timestamp;

		sobj.aux = mexUtilFindPassenger(obj->aux);

		if(obj->sprite != NULL)
		{
			sobj.sprite = obj->sprite->ID; // Just this for now
		}
		else
			sobj.sprite = 0;


		sobj.state = mexUtilFindState(obj->state, obj->logic);

		memcpy(sobj.stateData, obj->stateData, sizeof(obj->stateData));

		sobj.passenger = lgcUtilFindObjectIndex(obj->passenger);

		if(sobj.flags & OBJFLAG_TARGET_OBJECT)
			sobj.target.obj = obj->target.obj;
		else
			sobj.target.pos = obj->target.pos;
			
		
		#ifdef WIN32
			convertSavedObject(&sobj);
		#endif

		mexSaveObject(&sobj);

		obj = obj->next;

		*count++;
	}

	return FALSE;
}


BOOL mexLoadAllObjects( s32 *count )
{

	TObject			*obj;

	TSavedObject	sobj;

	BOOL			result;
	void			*data;


	*count = 0;

	while( mexLoadObject(&sobj) != FALSE )
	{
		#ifdef WIN32
			convertSavedObject(&sobj);
		#endif

		if(sobj.aux != -1)
			data = (void*)&gPassengers[sobj.aux];
		else
			data = NULL;
		
		if(sobj.type == OBJTYPE_CHOPPER)
		{
			mexCreatePlayer(&sobj);
			continue;
		}

		obj = mexCreateObject(sobj.type, data);

		obj->curPlatform = sobj.curPlatform;
		obj->route = &gRoutes[sobj.route];
		obj->flags = sobj.flags;
		obj->pos = sobj.pos;
		obj->vel = sobj.vel;

		// ?
		obj->accel = sobj.accel;
		obj->appliedForce = sobj.appliedForce;
		obj->mass = sobj.mass;
		obj->archimedeForce = sobj.archimedeForce;

		obj->timestamp = sobj.timestamp;

		if(sobj.flags & OBJFLAG_TARGET_OBJECT)
			obj->target.obj = sobj.target.obj;
		else
			obj->target.pos = sobj.target.pos;

		obj->sprite = resChangeSprite(obj->sprite, sobj.sprite);
		
		lgcInitObject(obj, sobj.state);
		memcpy(obj->stateData, sobj.stateData, sizeof(obj->stateData));

		// We just copy indexes for now
		obj->passenger = (TObject*)(s32)sobj.passenger;

		*count++;
	}

	// Restore passenger links
	obj = gObjectsList;

	while(obj != NULL)
	{
		obj->passenger = lgcUtilGetObject((s16)(u32)obj->passenger);
		obj = obj->next;
	}


	return FALSE;
}


BOOL mexClearSaveGame()
{
	DL_FsDeleteFile(resGetFullPath(SAVEGAME_FILENAME), 0);

	return TRUE;
}

BOOL mexCheckSaveGame()
{
	return DL_FsFFileExist(resGetFullPath(SAVEGAME_FILENAME));
}

#define CURRENT_SAVES_VERSION			8

BOOL mexSaveGame()
{
	TSavedGameInfo		gameInfo;
	s32					objCount;
	u32					written;

	sSaveGameFile = DL_FsOpenFile(resGetFullPath(SAVEGAME_FILENAME), FILE_WRITE_MODE, 0);

	gameInfo.version = CURRENT_SAVES_VERSION;
	gameInfo.level = (u16)gCurrentLevelNumber;
	gameInfo.objCount = gObjectsCount;
	gameInfo.waterLevel = gWaterLevel;
	gameInfo.gameState = gGameState;
	gameInfo.timestamp = gTimestamp;

	memcpy(gameInfo.platformOccupied, gPlatformOccupied, sizeof(gPlatformOccupied));

	#ifdef WIN32
		convertSavedGameInfo(&gameInfo);
	#endif

	DL_FsWriteFile((void*)&gameInfo, sizeof(TSavedGameInfo), 1, sSaveGameFile, &written);

	mexSaveAllObjects(&objCount);

	DL_FsCloseFile(sSaveGameFile);

	sSaveGameFile = FILE_HANDLE_INVALID;

	return TRUE;
}

BOOL mexLoadGame( u32 *level )
{
	TSavedGameInfo		gameInfo;
	u32					read;

	// To prevent game loading on level retry
	if(sSaveGameLoaded == TRUE)
		return FALSE;

	sSaveGameFile = DL_FsOpenFile(resGetFullPath(SAVEGAME_FILENAME), FILE_READ_MODE, 0);

	if(sSaveGameFile == FILE_HANDLE_INVALID)
		return FALSE;

	DL_FsReadFile((void*)&gameInfo, sizeof(TSavedGameInfo), 1, sSaveGameFile, &read);

	// We wont need to retry reading a savegame anyway
	sSaveGameLoaded = TRUE;

	#ifdef WIN32
		convertSavedGameInfo(&gameInfo);
	#endif

	// If the savegame is old, do not load it
	if(gameInfo.level < gCurrentLevelNumber)
	{
		DL_FsCloseFile(sSaveGameFile);
		sSaveGameFile = FILE_HANDLE_INVALID;

		return FALSE;
	}

	// If the savegame format is wrong
	if(gameInfo.version != CURRENT_SAVES_VERSION)
	{
		if(gameInfo.version == 7)
		{
			sLoadLegacy = 7;
		}
		else
		{
			dbgf(" *ERROR: wrong savegame version: %d, expected: %d!", gameInfo.version, CURRENT_SAVES_VERSION);
			DL_FsCloseFile(sSaveGameFile);
			sSaveGameFile = FILE_HANDLE_INVALID;

			sLoadLegacy = 0;
	
			return FALSE;
		}
	}


	gGameState = gameInfo.gameState;
	gWaterLevel = gameInfo.waterLevel;
	gCurrentLevelNumber = gameInfo.level;
	gTimestamp = gameInfo.timestamp;

	memcpy(gPlatformOccupied, gameInfo.platformOccupied, sizeof(gPlatformOccupied));

	*level = gameInfo.level;
	
	// Do NOT close the file! It will be closed in mexLoadStateObjects

	return TRUE;
}

BOOL mexLoadStateObjects()
{
	s32					objCount;

	// Savegame is opened in mexLoadState, if any
	if(sSaveGameFile == FILE_HANDLE_INVALID)
		return FALSE;

	mexLoadAllObjects(&objCount);
	
	DL_FsCloseFile(sSaveGameFile);

	sSaveGameFile = FILE_HANDLE_INVALID;

	return TRUE;
}

BOOL mexLoadHighscore(u32 *score, s32 *level)
{
	FILE_HANDLE_T		f;
	THighscoreInfo		info;
	u32					read;
	u32					size;
	
	f = DL_FsOpenFile(resGetFullPath(HIGHSCORE_FILENAME), FILE_READ_MODE, 0);

	if(f == FILE_HANDLE_INVALID)
	{
		*score = 0;
		*level = -1;
		return FALSE;
	}

	size = DL_FsGetFileSize(f);

	/* Old format */
	if( size == sizeof(u32) )
	{
		DL_FsReadFile((void*)&info.score, sizeof(u32), 1, f, &read);
		info.level = -1;
		info.ver = E16(CURRENT_HIGHSCORE_VER);
	}
	else /* New format */
	{
		DL_FsReadFile((void*)&info, sizeof(THighscoreInfo), 1, f, &read);
	}

	#ifdef WIN32
		CONV32(info.score);
		CONV16(info.level);
		CONV16(info.ver);
	#endif

	if(info.ver != CURRENT_HIGHSCORE_VER)
	{
		*score = 0;
		*level = -1;
		return FALSE;
	}

	DL_FsCloseFile(f);

	*score = info.score;
	*level = (s32)info.level;	
	
	return TRUE;
}

BOOL mexSaveHighscore(u32 score, s32 level)
{
	FILE_HANDLE_T		f;
	THighscoreInfo		info;
	u32					written;

	f = DL_FsOpenFile(resGetFullPath(HIGHSCORE_FILENAME), FILE_WRITE_MODE, 0);

	if(f == FILE_HANDLE_INVALID)
		return FALSE;

	info.ver = CURRENT_HIGHSCORE_VER;
	info.level = level;
	info.score = score;

	#ifdef WIN32
		CONV32(info.score);
		CONV16(info.level);
		CONV16(info.ver);
	#endif

	DL_FsWriteFile((void*)&info, sizeof(THighscoreInfo), 1, f, &written);

	DL_FsCloseFile(f);

	return TRUE;
}
